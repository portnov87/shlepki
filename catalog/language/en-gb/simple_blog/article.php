<?php
// Heading 
$_['heading_title']					= 'Блог';

// Buttons
$_['button_continue_reading']		= 'Подробнее';
$_['button_submit']					= 'Отправить';

$_['text_date_format']				= 'd.m.y';
$_['text_date_format_long']			= 'F jS, Y  g:i A';

// Entry
$_['entry_name']					= 'Ваше Имя:';
$_['entry_captcha']					= 'Enter the code in the box below:';
$_['entry_review']					= 'Ваш Комментарий:';

// Text
$_['text_no_found']					= 'No Blogs Created, Get Back Soon With Blogs!';
$_['text_related_product']			= 'Related Products';
$_['text_related_comment']			= 'Комментарии';
$_['text_related_article']			= 'Related Article';
$_['text_author_information']		= 'Автор';
$_['text_posted_by']				= 'Опубликовано';
$_['text_updated']					= 'Обновлено';
$_['text_comment_on_article']		= ' Комментария';
$_['text_view_comment']				= ' Посмотреть комментарии';
$_['text_write_comment']			= 'Оставить комментарий';
$_['text_cancel_reply']			    = 'Отменить';
$_['text_note']						= 'Note: HTML is not translated!';
$_['text_comments']					= ' Комментария';
$_['text_comment']					= ' Comment';
$_['text_no_blog']   				= 'There are no comments for this blog.';
$_['text_on']           			= ' on ';
$_['text_success']      			= 'Thank you for your comment!';
$_['text_success_approval']			= 'Thank you for your comment. It has been submitted to the webmaster for approval!';
$_['text_wait']						= 'Wait';
$_['text_reply_comment']			= 'Ответить';
$_['text_said']						= 'said:';
$_['text_authors']					= 'Authors';

$_['text_category_error']			= 'Blog Category Not Found!';
$_['text_author_error']				= 'Blog Author Not Found!';
$_['text_article_error']			= 'Blog Not Found!';

// Error
$_['error_name']        			= 'Warning: Author Name must be between 3 and 25 characters!';
$_['error_text']        			= 'Warning: Comment Text must be between 3 and 1000 characters!';
$_['error_captcha']     			= 'Warning: Verification fails!';


?>