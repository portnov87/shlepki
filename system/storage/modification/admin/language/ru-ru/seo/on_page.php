<?php
// Heading
$_['heading_title']          = 'on-page';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_filter']            = 'Фильтр';
$_['text_list']              = 'Список';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';

// Column
$_['column_seo_url']         = 'seo-url';
$_['column_store']           = 'Магазин';
$_['column_action']          = 'Действие';

// Entry
$_['entry_seo_url']          = 'seo-url';
$_['entry_store']            = 'Магазин';
$_['entry_heading']          = 'H1 заголовок';
$_['entry_seo_text']         = 'SEO текст';
$_['entry_meta_title']       = 'Мета-тег Title';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_head']             = 'Код в раздел &#60;head&#62;&#60;/head&#62;';

// Help
$_['help_status']            = 'Показывать/Скрыть переключатели языков (витрина магазина).';

// Error
$_['error_permission']       = 'У Вас нет прав для изменения on page параметров!';
$_['error_exists']           = 'Внимание: Сначала добавьте язык!';
$_['error_seo_url']          = 'seo-url обязателное поле!';
$_['error_code']             = 'Код языка должен быть от 2 символов!';
$_['error_locale']           = 'Необходимо указать кодировки языка!';
$_['error_default']          = 'Язык не может быть удален, поскольку он используется по умолчанию!';
$_['error_admin']            = 'Язык не может быть исключен, поскольку он является языком Администратора';
$_['error_store']            = 'Языки не может быть удален, так как назначен %s магазинам!';
$_['error_order']            = 'Языки не может быть удален, так как назначен %s заказам!';
