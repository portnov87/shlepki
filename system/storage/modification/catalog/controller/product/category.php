<?php

class ControllerProductCategory extends Controller
{
    public function index()
    {
        $this->load->language('product/category');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');
        $this->load->model('account/wishlist');


        $this->load->model('tool/image');

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.date_added';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
        }


        // OCFilter start
        if (isset($this->request->get['filter_ocfilter'])) {
            $filter_ocfilter = $this->request->get['filter_ocfilter'];
        } else {
            $filter_ocfilter = '';
        }
        // OCFilter end

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

//            foreach ($parts as $path_id) {
//                if (!$path) {
//                    $path = (int)$path_id;
//                } else {
//                    $path .= '_' . (int)$path_id;
//                }

                $category_info = $this->model_catalog_category->getCategory($category_id);






                if ($category_info) {




                    if (isset($this->request->get['page']))
                    {

                        $urlPath = 'path=' . $this->request->get['path'];

                        $this->document->addLink($this->url->link('product/category', $urlPath), 'canonical');


                        if ($this->request->get['page']==1)
                        {


                            $_url = $this->url->link('product/category', 'path=' . $category_id);
                            header('Location: ' . $_url);
                            exit();
                        }
                    }


                    if ($category_info['parent_id'] > 0) {
                        $category_info_parent = $this->model_catalog_category->getCategory($category_info['parent_id']);
                        if ($category_info_parent) {
                            $categories[] = $category_info_parent['name'];
                            //$category_info_parent['name']
                            $data['breadcrumbs'][] = array(
                                'text' => $category_info_parent['name'],
                                'href' => $this->url->link('product/category', 'path=' . $category_info_parent['category_id'])
                            );
                        }
                        $catId=$category_info_parent['category_id'].'_'.$category_id;
                    }else{
                        $catId=$category_id;
                    }

                    $categories[] = $category_info['name'];
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $catId . $url)
                    );
                }


//                if ($category_info) {
//                    $data['breadcrumbs'][] = array(
//                        'text' => $category_info['name'],
//                        'href' => $this->url->link('product/category', 'path=' . $path . $url)
//                    );
//                }
            //}
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_catalog_category->getCategory($category_id);

        $pageTitleAdd='';
        if (!empty($this->request->get['page']))
        {
            $pageTitleAdd=' - Страница '.$this->request->get['page'];
        }

//        echo '<pre>'.$category_id;
//        print_r($parts);
//        echo '</pre>';
//
//        echo '<pre>';
//        print_r($category_info);
//        echo '</pre>';

        if ($category_info) {
            if ($category_info['meta_title']=='') {
                $categoryName='';
                if ($category_info['parent_id']>0) {
                    $categoryName= $this->getSex($category_info['parent_id']);
                    $categoryName .= ' '.mb_strtolower($category_info['name']);
                }else
                    $categoryName =$category_info['name'];//mb_strtolower($category_info['name']);

                if ((int)$this->config->get('config_language_id')==2)
                    $metaTitle = $categoryName." - Купить Оптом в Украине ".$pageTitleAdd." | Shlepki.com";
                else
                    $metaTitle = $categoryName." - Купити Оптом в Україні ".$pageTitleAdd." | Shlepki.com";

            }
            else $metaTitle=$category_info['meta_title'];


            $this->document->setTitle($metaTitle);

            if ($category_info['meta_description']=='') {
                if ((int)$this->config->get('config_language_id')==2)
                    $metaDescription = "Интернет-магазин Shlepki - купить оптом " . mb_strtolower($category_info['name']) . " по выгодной цене. Украинские и зарубежные производители, высокое качество.";
                else
                    $metaDescription = "Інтернет-магазин Shlepki - купити оптом " . mb_strtolower($category_info['name']) . " За вигідною ціною. Українські та зарубіжні виробники, висока якість.";
            }
            else $metaDescription=$category_info['meta_description'];



            //if ($metaDescription)
            if (!empty($this->request->get['page']) &&($this->request->get['page']>1)) {
                $metaDescription='';
                //echo '$metaDescription.'.$metaDescription;

            }


            $this->document->setDescription($metaDescription);

            $this->document->setKeywords($category_info['meta_keyword']);


            $headingTitle='';
            if ($category_info['parent_id']>0)
                $headingTitle= $this->getSex($category_info['parent_id']).' ';

            $headingTitle .= $category_info['name'];


            $data['heading_title'] = $headingTitle;

            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));

            // Set the last category breadcrumb
//            $data['breadcrumbs'][] = array(
//                'text' => $category_info['name'],
//                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
//            );

            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'));

                $data['thumb_width'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width');
                $data['thumb_height'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height');

            } else {
                $data['thumb'] = '';
            }

            if (empty($this->request->get['page']) || ($this->request->get['page']==1))
                $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
            else
                $data['description'] = '';


            if (empty($this->request->get['page']) || ($this->request->get['page']==1))
                $data['short_description'] = html_entity_decode($category_info['short_description'], ENT_QUOTES, 'UTF-8');
            else
                $data['short_description'] = '';



            //$data['meta_h1'] = html_entity_decode($category_info['short_description'], ENT_QUOTES, 'UTF-8');


            $data['compare'] = $this->url->link('product/compare');

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['categories'] = array();

            $results = $this->model_catalog_category->getCategories($category_id);

            foreach ($results as $result) {
                $filter_data = array(
                    'filter_category_id' => $result['category_id'],
                    'filter_sub_category' => true
                );


                if ($result['count_products'] > 0) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'));

                    $data['categories'][] = array(
                        'name' => $result['name'] . ' (' . $result['count_products'] . ')',// . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'thumb' => $image,
                        'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                    );
                }
            }


            $data['label_sale'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_label_sale');
            $data['label_discount'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_label_discount');
            $data['label_new'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_label_new');
            if ($this->config->get('theme_' . $this->config->get('config_theme') . '_label_new')) {
                $product_new = $this->model_catalog_product->getLatestProducts($this->config->get('theme_' . $this->config->get('config_theme') . '_label_new_limit'));
            }

            $data['products'] = array();

            $filter_data = array(
                'filter_category_id' => $category_id,
                'filter_sub_category' => true,
                'filter_filter' => $filter,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit
            );


            // OCFilter start
            $filter_data['filter_ocfilter'] = $filter_ocfilter;
            // OCFilter end



            //$results = $this->model_catalog_product->getProducts($filter_data);

            if ($category_id==258) {
                $results = $this->model_catalog_product->getProductSpecials($filter_data);
                $product_total = $this->model_catalog_product->getTotalProductSpecials($filter_data);
            }
            else {
                $product_total = $this->model_catalog_product->getTotalProducts($filter_data);
                $results = $this->model_catalog_product->getProducts($filter_data);
            }

            $wishlist = [];
            if ($this->customer->isLogged()) {
                foreach ($this->model_account_wishlist->getWishlist() as $w) {
                    $wishlist[] = $w['product_id'];
                }
            } else {
                $wishlist = isset($this->session->data['wishlist']) ? $this->session->data['wishlist'] : array();
            }

            foreach ($results as $result) {

                if ($this->config->get('theme_' . $this->config->get('config_theme') . '_label_new')) {
                    $label_new = 0;
                    foreach ($product_new as $product_new_id => $product) {
                        if ($product_new[$product_new_id]['product_id'] == $result['product_id']) {
                            $label_new = 1;
                            break;
                        }
                    }
                }

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
                }


                $additional_image = $this->model_catalog_product->getProductImages($result['product_id']);
                if ($additional_image) {
                    $additional_image = $this->model_tool_image->resize($additional_image[0]['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $additional_image = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                    $label_discount = '-' . (int)(100 - ($result['special'] * 100 / $result['price'])) . '%';

                } else {
                    $special = false;

                    $label_discount = false;

                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }


                $options = array();
                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data = array();
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price_option = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $price_option = false;
                            }
                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price_option,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }
                    $options[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );
                }

                $in_box = $this->model_catalog_product->getParInBox($result['product_id']);
                $exchange=false;

                $exchange_price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
                $price_item = $this->currency->format($this->tax->calculate($result['price_item'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);
                //$exchange_price / $in_box

                $price = $this->currency->format($this->tax->calculate($exchange_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                //$price_number'] = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, false, false);

                if ((float)$result['special']) {
                    $exchange_special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
                } else {
                    $exchange_special = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($exchange_special, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                } else {
                    $special = false;
                }

                if ($exchange_special)
                {
                    $price_item = $this->currency->format($this->tax->calculate($result['special_item'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);

                }else{
                    $price_item = $this->currency->format($this->tax->calculate($result['price_item'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);

                }




                //$data['in_box']=$in_box;
                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'model'=>$result['model'],

                    'additional_thumb' => $additional_image,
                    'wishlist' => in_array($result['product_id'], $wishlist),


                    'img-width' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'),
                    'img-height' => $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'),

                    'name' => $result['name'],
                    //'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price' => $price,
                    'price_item'=>$price_item,
                    'special' => $special,
                    'par_in_box'=>$in_box,
                    'label_discount' => $label_discount,
                    'label_new' => $this->config->get('theme_' . $this->config->get('config_theme') . '_label_new') ? $label_new : 0,
                    'tax' => $tax,
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating' => $result['rating'],
                    //'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    'options' => $options
                );
            }

            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_asc'),
                'value' => 'pd.name-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_desc'),
                'value' => 'pd.name-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
            );

            if ($this->config->get('config_review_status')) {
                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_desc'),
                    'value' => 'rating-DESC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
                );

                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_asc'),
                    'value' => 'rating-ASC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
                );
            }

            $data['sorts'][] = array(
                'text' => $this->language->get('text_model_asc'),
                'value' => 'p.model-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_model_desc'),
                'value' => 'p.model-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
            );

            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'), 24, 48, 72, 96));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
                );
            }

            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['last_page'] = (int)ceil($product_total / $limit) === (int)$page;

            $data['next_page'] = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page=' . ($page + 1));
            $data['content_adventure'] = $this->load->controller('common/content_adventure');

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            $this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');
            if ($page == 1) {

                $urlCanonical=$this->url->link('product/category', 'path=' . $this->request->get['path']);
                $this->document->addLink($urlCanonical, 'canonical');
            } else {
                //$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id']), 'canonical');
                /*if ($category_info['parent_id']>0)
                    $urlCanonical=$this->url->link('product/category', 'path=' . $category_info['parent_id'].'_'.$category_info['category_id'] . '&page=' . $page);
                else
                    $urlCanonical=$this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . $page);
*/

                $urlCanonical=$this->url->link('product/category', 'path=' . $this->request->get['path']);// . '&page={page}');
                $urlCanonical=str_replace(array('?page=' . $page,'&page=' . $page),'',$urlCanonical);
                $this->document->addLink($urlCanonical, 'canonical');
            }

            if ($page > 1) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . (($page - 2) ? '&page=' . ($page - 1) : '')), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page=' . ($page + 1)), 'next');
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            // OCFilter Start
            if ($this->ocfilter->getParams()) {
                if (isset($product_total) && !$product_total) {
                    $this->response->redirect($this->url->link('product/category', 'path=' . $this->request->get['path']));
                }


                $this->document->setTitle($this->ocfilter->getPageMetaTitle($this->document->getTitle()));
                $this->document->setDescription($this->ocfilter->getPageMetaDescription($this->document->getDescription()));
                $this->document->setKeywords($this->ocfilter->getPageMetaKeywords($this->document->getKeywords()));

                $data['heading_title'] = $this->ocfilter->getPageHeadingTitle($data['heading_title']);

                if (empty($this->request->get['page']) || ($this->request->get['page']==1))
                    $data['description'] = $this->ocfilter->getPageDescription();
                else
                    $data['description'] = '';

                if (!trim(strip_tags(html_entity_decode($data['description'], ENT_QUOTES, 'UTF-8')))) {
                    $data['thumb'] = '';
                }

                $breadcrumb = $this->ocfilter->getPageBreadCrumb();

                if ($breadcrumb) {
                    $data['breadcrumbs'][] = $breadcrumb;
                }

                //$this->document->deleteLink('canonical');
            }
            // OCFilter End
            elseif ($category_info) {
                if ($category_info['meta_h1'] != '')
                    $data['heading_title'] = $category_info['meta_h1'];
            }

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('product/category', $data));
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/category', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function getSex($category_id)
    {
        $name='';
        switch ($category_id)
        {
            case 186:
                if ((int)$this->config->get('config_language_id')==2)
                    $name='Детские';
                else
                    $name='Дитячі';
                break;
            case 184:
                if ((int)$this->config->get('config_language_id')==2)
                    $name='Женские';
                else
                    $name='Жіночі';
                break;
            case 189:
                if ((int)$this->config->get('config_language_id')==2)
                    $name='Мужские';
                else
                    $name='Чоловічі';
                break;

        }
        return $name;

    }
}
