<?php
class ControllerCommonMobilenav extends Controller {
	public function index() {
		$this->load->language('common/menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

        $cached_categories = $this->cache->get('menu_categories_mobile.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id'));
        if ($cached_categories) {

            $data['categories']=$cached_categories;//$cached_categories;
        } else {


            $categories = $this->model_catalog_category->getCategories(0);


            foreach ($categories as $category) {
                if ($category['top']) {
                    if (($category['name']!=$this->language->get('menu_brand'))&&($category['name']!=$this->language->get('menu_new'))
                        &&($category['name']!='')
                        &&($category['name']!=$this->language->get('menu_special'))) {

                        // Level 2
                        $children_data = array();

                        $children = $this->model_catalog_category->getCategories($category['category_id']);

                        foreach ($children as $child) {
                            if ($child['count_products']>0) {
                                $filter_data = array(
                                    'filter_category_id' => $child['category_id'],
                                    'filter_sub_category' => true
                                );

                                $children_data[] = array(
                                    'name' => $child['name'].' ('.$child['count_products'].')',// . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                                );
                            }
                        }

                        // Level 1
                        $data['categories'][] = array(
                            'name' => $category['name'],
                            'children' => $children_data,
                            'column' => $category['column'] ? $category['column'] : 1,
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                        );
                    }
                }
            }

            $this->cache->set('menu_categories_mobile.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id'), $data['categories']);
        }

		return $this->load->view('common/mobilenav', $data);
	}
}
