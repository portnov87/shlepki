<?php
// header
$_['heading_title']  = 'Відновлення паролю';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Enter the new password you wish to use.';
$_['text_success']   = 'Ваш пароль успішно відновлено';

// Entry
$_['entry_password'] = 'Пароль';
$_['entry_confirm']  = 'Підтвердження паролю';

// Error
$_['error_password'] = 'Пароль повинен містити від 4 до 20 символів!';
$_['error_confirm']  = 'Підтвердження паролю не співпадає!';
$_['error_code']     = 'Password reset code is invalid or was used previously!';
