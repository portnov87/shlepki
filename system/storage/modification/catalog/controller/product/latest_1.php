<?php
class ControllerProductLatest extends Controller {
  public function index() {
    $this->load->language('product/latest');

    $this->load->model('catalog/product');

    $this->load->model('tool/image');

    $this->load->model('catalog/filter');

    $this->load->model('account/wishlist');

    if (isset($this->request->get['path'])) {
      $category_id = str_replace('_', ',', $this->request->get['path']);
    } else {
      $category_id = '';
    }

    if (isset($this->request->get['filter'])) {
      $filter = $this->request->get['filter'];
    } else {
      $filter = '';
    }

    if (isset($this->request->get['price'])) {
      $price = $this->request->get['price'];
    } else {
      $price = false;
    }

    if (isset($this->request->get['size'])) {
      $size = $this->request->get['size'];
    } else {
      $size = false;
    }

    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'p.date_added';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'desc';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    if (isset($this->request->get['limit'])) {
      $limit = (int)$this->request->get['limit'];
    } else {
      $limit = $this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit');
    }

    if (isset($this->request->get['manufacturer_id'])) {
      $filter_manufacturer = $this->request->get['manufacturer_id'];
    } else {
      $filter_manufacturer = '';
    }

    $this->document->setTitle(str_replace(['{{page_name}}', '{{telephones}}'], [$this->language->get('heading_title'), $this->config->get('config_telephone')], $this->config->get('config_meta_title')[$this->config->get('config_language_id')]));
    $this->document->setdescription(str_replace(['{{page_name}}', '{{telephones}}'], [$this->language->get('heading_title'), $this->config->get('config_telephone')], $this->config->get('config_meta_description')[$this->config->get('config_language_id')]));

    $data['show_bags_note'] = ((int)$category_id === (int)$this->config->get('bags_category_id')) ? true : false;

    $data['heading'] = $this->load->view('helpers/heading');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    if (isset($this->request->get['limit'])) {
      $url .= '&limit=' . $this->request->get['limit'];
    }

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('product/latest', $url)
    );

    $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));

    $data['compare'] = $this->url->link('product/compare');

    $data['products'] = array();

    $filter_data = array(
      'filter_category_id'      => $category_id,
      'filter_filter'           => $filter,
      'manufacturer_id'         => $filter_manufacturer,
      'price'                   => $price,
      'size'                    => $size,
      'sort'                    => $sort,
      'order'                   => $order,
      'start'                   => ($page - 1) * $limit,
      'limit'                   => $limit
    );
      if (isset($this->request->get['_size_']))
      {
          $filter_data['_size_'] = $this->request->get['_size_'];
      }
      if (isset($this->request->get['_stock_']))
      {
          $filter_data['_stock_'] = $this->request->get['_stock_'];
      }

      if (isset($this->request->get['selection_size']))
      {
          $filter_data['selection_size'] = $this->request->get['selection_size'];
      }

      $ajax = false;
      if (isset($this->request->get['ajax']))
          $ajax = true;

    $product_total = $this->model_catalog_product->getTotalProductLatests($filter_data);


      if (isset($this->request->get['count_products'])) {
          $data['products_total'] = $product_total;
          //echo '$product_total'.$product_total;die();
          $this->response->setOutput(json_encode($data));
          //die();
      } else {


          if ($product_total) {
              $results = $this->model_catalog_product->getProductLatests($filter_data);

              $wishlist = [];
              if ($this->customer->isLogged()) {
                  foreach ($this->model_account_wishlist->getWishlist() as $w) {
                      $wishlist[] = $w['product_id'];
                  }
              } else {
                  $wishlist = isset($this->session->data['wishlist']) ? $this->session->data['wishlist'] : array();
              }

              $position = 1;

              $categories = [];

              foreach ($results as $result) {
                  if ($result['image']) {
                      $image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'), $result['watermark']);
                  } else {
                      $image = $this->model_tool_image->resize('no_image.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'));
                  }

                  $filters = $this->model_catalog_product->getProductFilters($result['product_id']);
                  $data['product_filters'] = array();

                  $in_box = 1;
                  $size = false;
                  $dimension = false;
                  $number_of_pockets = 0;
                  $color = null;
                  $color_code = null;
                  $in_box_text='';
                  foreach ($filters as $filter_id) {
                      $filter_info = $this->model_catalog_filter->getFilter($filter_id);

                      if ($filter_info) {
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('type_filter_id')) {
                              $categories[] = $filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('in_box_filter_id')) {
                              $in_box = (int)$filter_info['name'];
                              $in_box_text = "{$filter_info['group']} ({$filter_info['name']})";
                          }

                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('size_filter_id')) {
                              $size = $filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('number_of_pockets_filter_id')) {
                              $number_of_pockets = $filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('height_filter_id')) {
                              $dimension[0] = (float)$filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('length_filter_id')) {
                              $dimension[1] = (float)$filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('depth_filter_id')) {
                              $dimension[2] = (float)$filter_info['name'];
                          }
                          if ((int)$filter_info['filter_group_id'] === (int)$this->config->get('color_filter_id')) {
                              $color = $filter_info['name'];
                              $color_code = $filter_info['filter_color'];
                          }
                          $data['product_filters'][] = array(
                              'filter_id' => $filter_info['filter_id'],
                              'name' => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                          );
                      }
                  }

                  if ($dimension && !empty($dimension)) {
                      ksort($dimension);
                      $dimension = implode('x', $dimension);
                  }

                  $exchange = (int)(CURRENCY_CODE === $result['currency_code']);
                  $exchange_price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);

                  if ((float)$result['special']) {
                      $exchange_special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
                  } else {
                      $exchange_special = false;
                  }

                  if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                      $price = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                  } else {
                      $price = false;
                  }

                  if ($exchange_special) {
                      $special = $this->currency->format($this->tax->calculate($exchange_special * $in_box, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                      $special_percent = '-' . round((($exchange_price - $exchange_special) / $exchange_price) * 100) . '%';
                  } else {
                      $special = false;
                      $special_percent = false;
                  }

                  if ($this->config->get('config_tax')) {
                      $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                  } else {
                      $tax = false;
                  }

                  if ($this->config->get('config_review_status')) {
                      $rating = (int)$result['rating'];
                  } else {
                      $rating = false;
                  }

                  if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                      $price_item = $this->currency->format($this->tax->calculate($exchange_price, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                  } else {
                      $price_item = false;
                  }

                  if ($exchange_special) {
                      $special_item = $this->currency->format($this->tax->calculate($exchange_special, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
                  } else {
                      $special_item = false;
                  }

                  $ga_ecommerce = [
                      'list' => 'new',
                      'link' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                      'id' => $result['product_id'],
                      'name' => $result['name'],
                      'category' => implode('/', $categories),
                      'brand' => $result['manufacturer'],
                      'position' => $position
                  ];

                  $position++;
                  if ($result['pre_order_days'] > 0)
                      $text_pre_order = '<span>под заказ ' . $this->model_catalog_product->plural_form($result['pre_order_days'], ['день', 'дня', 'дней']) . '</span>';
                  else $text_pre_order = '';
                  $data['products'][] = array(
                      'product_id' => $result['product_id'],
                      'thumb' => $image,
                      'pre_order' => ($result['pre_order_days']>0?1:$result['pre_order']),//$result['pre_order'],
                      'pre_order_days' => $result['pre_order_days'],
                      'pre_order_days_text' => $text_pre_order,
                      'name' => $result['name'],
                      'model' => $result['model'],
                      'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                      'stock_status' => $result['stock_status'],

                      'price' => $price,
                      'price_item' => $price_item,
                      'special' => $special,
                      'special_percent' => $special_percent,
                      'special_item' => $special_item,
                      'selection_size' => $result['selection_size'],
                      'in_box' => $in_box,
                      'in_box_number'=>$in_box,
                      'in_box_text'=>$in_box_text,
                      'size' => $size,
                      'number_of_pockets' => $number_of_pockets,

                      'dimension' => $dimension,
                      'color' => $color,
                      'color_code' => $color_code,
                      'tax' => $tax,
                      'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                      'rating' => $result['rating'],
                      'is_new' => $result['is_new'],
                      'ga_ecommerce' => $ga_ecommerce,
                      'wishlist' => in_array($result['product_id'], $wishlist),
                      'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                  );
              }
          }
      }

    $url = '';

    if (isset($this->request->get['path'])) {
      $url .= '&path=' . $this->request->get['path'];
    }

    if (isset($this->request->get['filter'])) {
      $url .= '&filter=' . $this->request->get['filter'];
    }

    if (isset($this->request->get['price'])) {
      $url .= '&price=' . $this->request->get['price'];
    }

    if (isset($this->request->get['size'])) {
      $url .= '&size=' . $this->request->get['size'];
    }


      if (isset($this->request->get['_size_'])) {
          $url .= '&_size_=' . $this->request->get['_size_'];
      }
      if (isset($this->request->get['_stock_'])) {
          $url .= '&_stock_=' . $this->request->get['_stock_'];
      }



      if (isset($this->request->get['selection_size'])) {
          $url .= '&selection_size=' . $this->request->get['selection_size'];
      }

    if (isset($this->request->get['limit'])) {
      $url .= '&limit=' . $this->request->get['limit'];
    }

    if (isset($this->request->get['manufacturer_id'])) {
      $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
    }

    $data['sorts'] = array();

    $data['sorts'][] = array(
      'text'  => $this->language->get('text_date_added_asc'),
      'value' => 'p.date_added-desc',
      'href'  => $this->url->link('product/latest', 'sort=p.date_added&order=desc' . $url)
    );

    $data['sorts'][] = array(
      'text'  => $this->language->get('text_popular_desc'),
      'value' => 'popular-desc',
      'href'  => $this->url->link('product/latest', 'sort=popular&order=desc' . $url)
    );

    $data['sorts'][] = array(
      'text'  => $this->language->get('text_special_asc'),
      'value' => 'special-desc',
      'href'  => $this->url->link('product/latest', 'sort=special&order=desc' . $url)
    );

    $data['sorts'][] = array(
      'text'  => $this->language->get('text_price_asc'),
      'value' => 'price-ASC',
      'href'  => $this->url->link('product/latest', 'sort=price&order=asc' . $url)
    );

    $data['sorts'][] = array(
      'text'  => $this->language->get('text_price_desc'),
      'value' => 'price-desc',
      'href'  => $this->url->link('product/latest', 'sort=price&order=desc' . $url)
    );

    if ($this->config->get('config_review_status')) {
      $data['sorts'][] = array(
        'text'  => $this->language->get('text_rating_desc'),
        'value' => 'rating-desc',
        'href'  => $this->url->link('product/latest', 'sort=rating&order=desc' . $url)
      );

      $data['sorts'][] = array(
        'text'  => $this->language->get('text_rating_asc'),
        'value' => 'rating-ASC',
        'href'  => $this->url->link('product/latest', 'sort=rating&order=asc' . $url)
      );
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $data['limits'] = array();

    $limits = array_unique(array($this->config->get('theme_' . $this->config->get('config_theme') . '_product_limit'), 59, 89));

    sort($limits);

    foreach($limits as $value) {
      $data['limits'][] = array(
        'text'  => $value + 1,
        'value' => $value,
        'href'  => $this->url->link('product/latest', $url . '&limit=' . $value)
      );
    }

    $url = '';

    if (isset($this->request->get['path'])) {
      $url .= '&path=' . $this->request->get['path'];
    }

    if (isset($this->request->get['filter'])) {
      $url .= '&filter=' . $this->request->get['filter'];
    }

    if (isset($this->request->get['price'])) {
      $url .= '&price=' . $this->request->get['price'];
    }

    if (isset($this->request->get['size'])) {
      $url .= '&size=' . $this->request->get['size'];
    }
      if (isset($this->request->get['_size_'])) {
          $url .= '&_size_=' . $this->request->get['_size_'];
      }
      if (isset($this->request->get['_stock_'])) {
          $url .= '&_stock_=' . $this->request->get['_stock_'];
      }

      if (isset($this->request->get['selection_size'])) {
          $url .= '&selection_size=' . $this->request->get['selection_size'];
      }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['limit'])) {
      $url .= '&limit=' . $this->request->get['limit'];
    }

    if (isset($this->request->get['manufacturer_id'])) {
      $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
    }

    $pagination = new Pagination();
    $pagination->total = $product_total;
    $pagination->page = $page;
    $pagination->limit = $limit;
    $pagination->url = $this->url->link('product/latest', $url . '&page={page}');

    $data['last_page'] = (int)ceil($product_total / $limit) === (int)$page;

    $data['pagination'] = $pagination->render();

    $data['next_page'] = $this->url->link('product/latest', $url.'&page='.($page + 1), true);

    $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

    if ($limit && ceil($product_total / $limit) > $page) {
        $this->document->addLink($this->url->link('product/latest', 'page='. ($page + 1), true), 'next');
    }

    if ($limit && (ceil($product_total / $limit) >= $page) && ($page !== 1)) {
        $this->document->addLink($this->url->link('product/latest', 'page='. ($page - 1), true), 'prev');
    }

    $data['sort'] = $sort;
    $data['order'] = $order;
    $data['limit'] = $limit;

    $data['sort_box'] = $product_total ? $this->load->view('helpers/sort', array('sort' => $sort.'-'.$order, 'sorts' => $data['sorts'])) : null;

    $data['filters_buttons'] = $this->load->view('helpers/filters_buttons');

    $data['sort_box_mobile'] = $this->load->view('helpers/sort_mobile', array('sort' => $sort.'-'.$order, 'sorts' => $data['sorts']));

    $data['breadcrumbs'] = $this->load->view('helpers/breadcrumbs', ['breadcrumbs' => $data['breadcrumbs'], 'mobile_filter' => true, 'product_total' => $product_total]);

    $data['continue'] = $this->url->link('common/home');

    $data['column_left'] = $this->load->controller('common/column_left');

      if (!$ajax) {
          $data['column_right'] = $this->load->controller('common/column_right');
          $data['content_top'] = $this->load->controller('common/content_top');
          $data['content_bottom'] = $this->load->controller('common/content_bottom');
          $data['footer'] = $this->load->controller('common/footer');
          $data['header'] = $this->load->controller('common/header');
          $this->response->setOutput($this->load->view('product/latest', $data));
      }else {


          $states=[];
          $filters_exists = 0;
          if (isset($this->request->get['filters_group'])) {
              if (count($this->request->get['filters_group'])>0) {



                  foreach ($this->request->get['filters_group'] as $key=>$filter)
                  {
                      $key_array=explode('_',$key);
                      if (isset($key_array[1])) {
                          $filter_group_info = $this->model_catalog_filter->getFilterGroup((int)$key_array[1]);
                          if ($filter_group_info) {
                              foreach ($filter as $_key => $_fil) {

                                  $filter_info = $this->model_catalog_filter->getFilter((int)$_fil);
                                  if (!isset($states[$key_array[1]]))
                                  {
                                      $states[$key_array[1]]=['key'=>$filter_group_info['name'],'values'=>[]];//$filter_info['name']];


                                  }
                                  if ($filter_info) {

                                      $url = '';

                                      if (!in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
                                          $url .= '&path=' . $this->request->get['path'];
                                      }

                                      if (in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
                                          $url .= '&path=' . $this->request->get['path'];
                                      }

                                      if (isset($this->request->get['search'])) {
                                          $url .= '&search=' . $this->request->get['search'];
                                      }

                                      if (isset($this->request->get['manufacturer_id'])) {
                                          $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
                                      }

                                      if (isset($this->request->get['sort'])) {
                                          $url .= '&sort=' . $this->request->get['sort'];
                                      }

                                      if (isset($this->request->get['order'])) {
                                          $url .= '&order=' . $this->request->get['order'];
                                      }

                                      if (isset($this->request->get['limit'])) {
                                          $url .= '&limit=' . $this->request->get['limit'];
                                      }
                                      if (isset($this->request->get['filter'])) {
                                          $filter_delete=$filter_info['filter_id'];
                                          $_f=$this->request->get['filter'];
                                          $_f_array=explode(',',$_f);
                                          foreach ($_f_array as $_k=>$_v) {
                                              if ($_v==$filter_delete)
                                                  unset($_f_array[$_k]);
                                          }
                                          if (count($_f_array)>0)
                                              $url .= '&filter=' . implode(',',$_f_array);
                                      }




                                      $reset = str_replace('&amp;', '&', $this->url->link($route, $url));
                                      $name='<a href="'.$reset.'" style="font-size:14px;color: #545454;text-decoration:none;">'.mb_strtolower($filter_info['name']).' x</a>';
                                      $states[$key_array[1]]['values'][]=$name;
                                      //$states[$key_array[1]] =['key'=>$filter_group_info['name'],'values'=>$filter_info['name']];
                                  }
                              }
                          }

                          if (isset($states[$key_array[1]]))
                          {
                              $states[$key_array[1]]['values_string']=implode(', ',$states[$key_array[1]]['values']);
                          }
                      }
                  }

                  $filters_exists = 1;
              }
          }
          if (isset($this->request->get['route'])) {
              $route = $this->request->get['route'];
          } else {
              $route = '';
          }

          $url = '';

          if (!in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
              $url .= '&path=' . $this->request->get['path'];
          }

          if (in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
              $url .= '&path=' . $this->request->get['path'];
          }

          if (isset($this->request->get['search'])) {
              $url .= '&search=' . $this->request->get['search'];
          }

          if (isset($this->request->get['manufacturer_id'])) {
              $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
          }

          if (isset($this->request->get['sort'])) {
              $url .= '&sort=' . $this->request->get['sort'];
          }

          if (isset($this->request->get['order'])) {
              $url .= '&order=' . $this->request->get['order'];
          }

          if (isset($this->request->get['limit'])) {
              $url .= '&limit=' . $this->request->get['limit'];
          }

          $reset = str_replace('&amp;', '&', $this->url->link($route, $url));
          if (isset($this->request->get['_size_']))
          {


              $states['size']=['key'=>'Размер'];



              foreach ($this->request->get['_size_'] as $_size) {
                  $url = '';

                  if (!in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
                      $url .= '&path=' . $this->request->get['path'];
                  }

                  if (in_array($route, ['product/latest', 'product/special']) && isset($this->request->get['path']) && !isset($this->request->get['manufacturer'])) {
                      $url .= '&path=' . $this->request->get['path'];
                  }

                  if (isset($this->request->get['search'])) {
                      $url .= '&search=' . $this->request->get['search'];
                  }

                  if (isset($this->request->get['manufacturer_id'])) {
                      $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
                  }

                  if (isset($this->request->get['sort'])) {
                      $url .= '&sort=' . $this->request->get['sort'];
                  }

                  if (isset($this->request->get['order'])) {
                      $url .= '&order=' . $this->request->get['order'];
                  }

                  if (isset($this->request->get['limit'])) {
                      $url .= '&limit=' . $this->request->get['limit'];
                  }
                  if (isset($this->request->get['filter'])) {
                      $filter_delete = $filter_info['filter_id'];
                      $_f = $this->request->get['filter'];
                      $_f_array = explode(',', $_f);
                      /*foreach ($_f_array as $_k => $_v) {
                          if ($_v == $filter_delete)
                              unset($_f_array[$_k]);
                      }*/
                      if (count($_f_array)>0)
                          $url .= '&filter=' . implode(',', $_f_array);
                  }



                  //echo $url.'<br/>';
                  $reset = str_replace('&amp;', '&', $this->url->link($route, $url));
                  foreach ($this->request->get['_size_'] as $_size2) {
                      if ($_size2!=$_size) {
                          if ($_size2!='')
                            $reset .= '/size_' . $_size2;
                      }
                  }
                  $name = '<a href="' . $reset . '" style="">' . mb_strtolower($_size) . ' <span>X</span></a>';

                  $states['size']['values'][] = $name;

              }
//die();
              $states['size']=['key'=>'Размер',
                  'values_string'=>implode(' ',$states['size']['values']),
                  'values'=>
                      $states['size']['values']

              ];
              $filters_exists=1;
              //$states[$key_array[1]]['values_string']=implode(' ',$states[$key_array[1]]['values']);
          }
          $data['states'] = $this->load->view('product/states', array('states' => $states,'reset'=>$reset,'filters_exists'=>$filters_exists));





          $data['total'] = $product_total;
          $products = $data['products'];
          $data['products_html'] = $this->load->view('product/products', array('next_page' => $data['next_page'], 'pagination' => $data['pagination'], 'last_page' => $data['last_page'], 'products' => $products));

          unset($data['products']);

          $this->response->setOutput(json_encode($data));



      }
  }
}
