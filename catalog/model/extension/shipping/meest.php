<?php

class ModelExtensionShippingMeest extends Model {
  public function getQuote($address) {
    $this->load->language('extension/shipping/meest');

    if ($this->config->get('shipping_meest_status')) {
      $quote_data = [];

      $quote_data['meest'] = [
        'code'          => 'meest.meest',
        'title'         => $this->language->get('text_description'),
      ];

      $method_data = [
        'code'          => 'meest',
        'title'         => $this->language->get('text_title'),
        'quote'         => $quote_data,
        'sort_order'    => $this->config->get('shipping_meest_sort_order'),
        'error'         => false
      ];

      return $method_data;
    }
  }

  public function getRegions() {
    return $this->db->query("SELECT * FROM " . DB_PREFIX . "meest_region ORDER BY DescriptionRU ASC")->rows;
  }

  public function getCitiesByRegion($region_uuid) {
    return  $this->db->query("SELECT * FROM " . DB_PREFIX . "meest_city WHERE Regionuuid = '" . $region_uuid . "' ORDER BY DescriptionRU ASC")->rows;
  }

  public function getBranchByCity($city_uuid) {
    return  $this->db->query("SELECT * FROM " . DB_PREFIX . "meest_branch WHERE CityUUID = '" . $city_uuid . "' ORDER BY DescriptionRU ASC")->rows;
  }

  public function getBranch($branch_uuid) {
    return  $this->db->query("SELECT * FROM " . DB_PREFIX . "meest_branch WHERE UUID = '" . $branch_uuid . "'")->row;
  }
}

?>
