<?php
// Text
$_['text_title']       = 'Новая Почта';
$_['text_description'] = 'Новая Почта';
$_['text_district'] = 'Регион:';
$_['text_select_district'] = 'Выберите область...';
$_['text_city'] = 'Город:';
$_['text_delivery_type'] = 'Тип доставки:';
$_['text_delivery_department'] = 'Доставка на отделение';
$_['text_delivery_address'] = 'Доставка на адрес';
$_['text_office'] = 'Отделение:';
$_['text_address'] = 'Адрес:';
$_['text_order_comment'] = 'Комментарий к заказу:';
?>