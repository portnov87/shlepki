<?php
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

class ControllerImportForsage extends Controller
{
    private $error = array();


    protected function rus2translit($string)
    {
        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    protected function transliterate($str)
    {
        // переводим в транслит
        $str = $this->rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        //$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        //$str = trim($str, "-");
        return $str;
    }


//    protected function transliterate($textcyr = null, $textlat = null)
//    {
//        $cyr = array(
//            'ж', 'ч', 'щ', 'ш', 'ю', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я',
//            'Ж', 'Ч', 'Щ', 'Ш', 'Ю', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
//        $lat = array(
//            'zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q',
//            'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q');
//        if ($textcyr) return str_replace($cyr, $lat, $textcyr);
//        else if ($textlat) return str_replace($lat, $cyr, $textlat);
//        else return null;
//    }

    protected function getImage($url, $name, $prefix = '')
    {
        //$filename = $this->model_extension_ocfilter->translit(str_replace('&amp;', 'and', mb_strtolower($name)))."_".basename($url);//$this->model_extension_ocfilter->translit($prefix .
        $filename = $this->transliterate($prefix . basename($url));
        $img_path = 'catalog/storage/forsage/';// . $this->model_extension_ocfilter->translit(str_replace('&amp;', 'and', mb_strtolower($name))) . '/';
        $img_dir = DIR_IMAGE . $img_path;
        if (!is_dir($img_dir)) {
            mkdir($img_dir, 0777, true);
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $out = curl_exec($ch);
        if (trim($out) == '')
            return false;

        $image_sv = $img_dir . $filename;
        $img_sc = file_put_contents($image_sv, $out);
        curl_close($ch);

        return $img_path . $filename;
    }


    protected function removeCurrencyFromPrice($price)
    {
        return filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }

//import api


    public function getStoresApi()
    {

    }

    public function importapisupplier()
    {
        $this->load->model('catalog/manufacturer');

        $this->load->model('forsage/api');

        $forsage_api = $this->model_forsage_api;
        $new_suppliers = $forsage_api->getSuppliers();

        if ($new_suppliers->suppliers) {
            foreach ($new_suppliers->suppliers as $supplier) {
                $manufacturer_info = $this->model_catalog_manufacturer->getManufacturerByName(htmlspecialchars($supplier->company));

                if (!empty($manufacturer_info)) {
                    $manufacturer_id = $manufacturer_info['manufacturer_id'];
                    $return['manufacturer_id'] = $manufacturer_id;
                    // update manufacturer
                    $address = $supplier->address;
                    $_address = explode(' ', $address);
                    $number = $_address[count($_address) - 1];
                    unset($_address[count($_address) - 1]);
                    $street = implode(' ', $_address);
                    $type = '';


                    $edit_brand = [
                        'name' => (string)$supplier->company,
                        'phone' => (string)$supplier->phone,
                        'address' => (string)$address,
                        'email' => (string)$supplier->email,
                        'forsage_id' => $supplier->id
                    ];


                    $manufacturer = $this->model_catalog_manufacturer->editManufacturerFromForsage($manufacturer_id, $edit_brand);

                }
            }

        }

    }

    public function importapiprocessall()
    {
        $this->load->model('catalog/product');

        $this->load->model('catalog/product');

        $this->load->model('extension/ocfilter');

        $this->load->model('catalog/option');
        $this->load->model('localisation/language');
        $this->load->model('catalog/manufacturer');
        $this->load->model('setting/setting');
        $this->load->model('catalog/filter');
        $this->load->model('localisation/currency');
        $this->load->model('catalog/filter');
        $this->load->model('catalog/category');
        $this->load->model('localisation/stock_status');
        $this->load->model('forsage/api');
        $this->load->model('localisation/language');

        error_reporting(E_ALL & E_STRICT);
        ini_set('display_errors', 'on');
        ob_implicit_flush(1);

        echo str_pad('', 1024);
        @ob_flush();
        flush();


        $stores = [];

        $stores[] = [
            'store_id' => '0',
            'forsage_path' => 'shlepki-com',
            'key' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzMDAsImlzcyI6Imh0dHBzOi8vZm9yc2FnZS1zdHVkaW8uY29tL2dlbmVyYXRlVG9rZW4vMTMwMCIsImlhdCI6MTU3OTA5OTE1MCwiZXhwIjo0NjQ2Mjk5MTUwLCJuYmYiOjE1NzkwOTkxNTAsImp0aSI6Ik5lUXl3VlVxZnNCakFFT1QifQ.nS5CI6FyyHHgZgQu27TjspA_tOz03Ay7aRQe0zqwejk',
        ];
        $store = $stores[0];
        $period = '-100 days';

        $forsage_api = $this->model_forsage_api;


        $languages = $this->model_localisation_language->getLanguages();

        $input = json_decode(file_get_contents('/var/www/shlepki.com/htdocs/forsage_1.json'), true);

        $products = [];
        if (isset($input['product_ids'])) {
            $products = $input['product_ids'];
            $change_type = false;
            $_change_type = $input['change_type'];
            foreach ($_change_type as $t) {
                $change_type = $t;
            }
        }

        foreach ($products as $forsage_id) {

            $_data_forsage = $forsage_api->GetProduct($forsage_id);

            $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);

            if ($_data_forsage->product) {
                $data_forsage = $_data_forsage->product;
                if ($_data_forsage->product->category->name != 'Одежда и аксессуары') {
                    $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);
                    echo  'process forsage = '.$forsage_id."\r\n";
                }
            } else {

                $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);
                if ($product_data) {

                    $_data = [];
                    $_data['status'] = 0;
                    $this->model_catalog_product->changeStatus($product_data['product_id'], $_data);
                    //$product_id = $this->model_catalog_product->editProduct($product_data['product_id'], $data);
                    echo  'process forsage = '.$forsage_id."\r\n";
                }
                //else{
                    //echo  'error no forsage process forsage = '.$forsage_id."\r\n";
                //}
            }



            @ob_flush();
            flush();
        }

    }

    public function importapiprocess()
    {
        $this->load->model('catalog/product');
//        $this->load->model('extension/ocfilter');
//        $this->load->model('forsage/api');

        $this->load->model('catalog/product');

        $this->load->model('extension/ocfilter');

        $this->load->model('catalog/option');
        $this->load->model('localisation/language');
        $this->load->model('catalog/manufacturer');
        $this->load->model('setting/setting');
        $this->load->model('catalog/filter');
        $this->load->model('localisation/currency');
        $this->load->model('catalog/filter');
        $this->load->model('catalog/category');
        $this->load->model('localisation/stock_status');
        $this->load->model('forsage/api');
        $this->load->model('localisation/language');

        error_reporting(E_ALL & E_STRICT);
        ini_set('display_errors', 'on');
        ob_implicit_flush(1);

        echo str_pad('', 1024);
        @ob_flush();
        flush();


        $stores = [];

        $stores[] = [
            'store_id' => '0',
            'forsage_path' => 'shlepki-com',
            'key' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzMDAsImlzcyI6Imh0dHBzOi8vZm9yc2FnZS1zdHVkaW8uY29tL2dlbmVyYXRlVG9rZW4vMTMwMCIsImlhdCI6MTU3OTA5OTE1MCwiZXhwIjo0NjQ2Mjk5MTUwLCJuYmYiOjE1NzkwOTkxNTAsImp0aSI6Ik5lUXl3VlVxZnNCakFFT1QifQ.nS5CI6FyyHHgZgQu27TjspA_tOz03Ay7aRQe0zqwejk',
        ];
        $store = $stores[0];
        $period = '-100 days';
        //'\'-17 minutes\'';


        $forsage_api = $this->model_forsage_api;

        //$products = $forsage_api->getProducts($start_date, $finish_date, $stock);
        //$suppliers = $forsage_api->getSuppliers();

        $languages = $this->model_localisation_language->getLanguages();
        //$languages=[];
//        foreach ($this->model_catalog_product->getProducts() as $product) {
//            $product_data = $this->model_catalog_product->deleteProduct($product['product_id']);
//        }
//        die();

        //echo DIR_APPLICATION.'forsage.json';
        $input = json_decode(file_get_contents('/var/www/shlepki.com/htdocs/forsage_1.json'), true);
        //https://shlepki.com/index.php?route=push/forsage/index 559461
        //$this->addLogs(file_get_contents('php://input'));
//        echo '<pre>$input';
//        print_r($input);
//        echo '</pre>';
        $products = [];
        if (isset($input['product_ids'])) {
            $products = $input['product_ids'];
            $change_type = false;
            $_change_type = $input['change_type'];
            foreach ($_change_type as $t) {
                $change_type = $t;
            }
        }

        foreach ($products as $forsage_id) {
            //if ($forsage_id>537226) {

            $_data_forsage = $forsage_api->GetProduct($forsage_id);

            $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);

            if ($_data_forsage->product) {
                $data_forsage = $_data_forsage->product;
                if ($_data_forsage->product->category->name != 'Одежда и аксессуары') {
                    //echo $forsage_id . ' ' . $_data_forsage->product->vcode . ' ' . $_data_forsage->product->id . " new\r\n";
                    $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);
//                            @ob_flush();
//                            flush();
                }
            } else {

                $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);
                if ($product_data) {

                    $_data = [];
                    $_data['status'] = 0;
                    changeStatus($product_data['product_id'], $_data);
                    //
                    //$product_id = $this->model_catalog_product->editProduct($product_data['product_id'], $data);
                }
            }
            //}
        }


        die();

        foreach ($suppliers->suppliers as $sup) {


            //echo '$sup id.'.$sup->id."\r\n";
            if ((int)$sup->id < 63) {

                //        if ((int)$sup->id != 91) {
                $products = $forsage_api->GetProductsBySupplier($sup->id, '-1');

                foreach ($products->product_ids as $forsage_id) {

                    $_data_forsage = $forsage_api->GetProduct($forsage_id);

                    $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);


//                    if ($product_data) {
//                        continue;


                    if ($_data_forsage->product) {
                        $data_forsage = $_data_forsage->product;

                        if ((int)$data_forsage->quantity != 0) {


                            if ($_data_forsage->product->category->name != 'Одежда и аксессуары') {
                                echo $forsage_id . ' ' . $_data_forsage->product->vcode . ' ' . $_data_forsage->product->id . " new\r\n";
                                $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);
                                @ob_flush();
                                flush();
                            }
                        }

                    }
                }

                $products = $forsage_api->GetProductsBySupplier($sup->id, '1');
                //$this->getPr($products,$sup,$languages, $store, $stores);
                foreach ($products->product_ids as $forsage_id) {

                    $_data_forsage = $forsage_api->GetProduct($forsage_id);

                    $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);
//                echo '<pre>sdvcsdvcsdvsd  '.$forsage_id;
//                print_r($_data_forsage);
//                echo '</pre>';

//                    if ($product_data) {
//                        continue;


                    if ($_data_forsage->product) {
                        $data_forsage = $_data_forsage->product;

                        if ((int)$data_forsage->quantity != 0) {


                            if ($_data_forsage->product->category->name != 'Одежда и аксессуары') {
                                echo $forsage_id . ' ' . $_data_forsage->product->vcode . ' ' . $_data_forsage->product->id . " new\r\n";
                                $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);
                                @ob_flush();
                                flush();
                            }
                        }

                    }

                }

//                echo $sup->id . ' ' . $sup->company . ' products=' . count($products->product_ids) . "\r\n";


                echo '<pre>$products';
                print_r($products);
                echo '</pre>';

            }

        }


        @ob_flush();
        flush();
        die();

    }


    public function getPr($products, $sup, $languages, $store, $stores)
    {
        $forsage_api = $this->model_forsage_api;
        foreach ($products->product_ids as $forsage_id) {

            $_data_forsage = $forsage_api->GetProduct($forsage_id);

            $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);
//            echo '<pre>sdvcsdvcsdvsd  '.$forsage_id;
//            print_r($_data_forsage);
//            echo '</pre>';

//                    if ($product_data) {
//                        continue;


            if ($_data_forsage->product) {
                $data_forsage = $_data_forsage->product;

                if ((int)$data_forsage->quantity != 0) {

                    $supplier = $data_forsage->supplier;

                    if ($_data_forsage->product->category->name != 'Одежда и аксессуары') {
                        echo $forsage_id . ' ' . $_data_forsage->product->vcode . ' ' . $_data_forsage->product->id . " new\r\n";
                        $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);
                    }
                }

            }
        }
    }


    /**
     * @param $languages
     * @param $preorder_stock_id
     * @param $settings_forsage
     */
    public function processupdate($data_forsage, $languages, $store, $stores)
    {

//
        $this->load->model('extension/ocfilter');
        $this->load->model('catalog/product');
        $forsage_id = $data_forsage->id;
        $vcode = $data_forsage->vcode;
        $name = $data_forsage->name;
        $quantity = $data_forsage->quantity;

        $sex = $type = '';
        $price = 0;
        $cost_price = 0;
        $images = [];
        $special = [];
        $filters_forsage = [];

        $update_product = true;
        //$product_data = [];
        $product_data = $this->model_catalog_product->getProductByForsage($forsage_id);//getProducts(['forsage_id' => $forsage_id]);

        $product_id = false;


        $togrovay_marka = false;
        if (isset($data_forsage->brand))
            $togrovay_marka = $data_forsage->brand->name;


        $product = [
            'filters' => [],
            'categories' => []
        ];
        $supplier = $data_forsage->supplier;
        $supplier_name = $supplier->company;



        $_supplier_name = str_replace(' ', '-', $supplier_name);
        $sku = $data_forsage->vcode;
        $_sku = str_replace(' ', '-', $sku);
        $product['name'] = $data_forsage->name . ' ' . $sku;

        $model = $_supplier_name . '-' . $_sku;


        $product['model'] = $model;
        $product['sku'] = $_sku;


        $characteristics = $data_forsage->characteristics;
        $image = '';
        $images = [];

        if (isset($product_data['forsage_id'])) {
            if ($product_data['forsage_id'] > 0) {
                $images = [];
            } else
                $images = $this->updateImages($characteristics, $model, $store['forsage_path'], $forsage_id);
        } else
            $images = $this->updateImages($characteristics, $model, $store['forsage_path'], $forsage_id);

        foreach ($characteristics as $char) {
            if ($char->type != 'images') {
                switch ($char->name) {
                    case 'Валюта продажи':
                        //гривна
                        $currency_text = $char->value;
                        if (preg_match('/доллар?|\$/', $currency_text)) {
                            $currency_info = $this->model_localisation_currency->getCurrencyByCode('USD');
                            $product['currency_id'] = $currency_info['currency_id'];
                        } else {
                            $currency_info = $this->model_localisation_currency->getCurrencyByCode('UAH');
                            $product['currency_id'] = $currency_info['currency_id'];
                        }
                        break;
                    case 'Пол':
                        $sex = $char->value;
                        break;
                    case 'Тип':
                        $type = $char->value;
                        break;
                    case 'Фото 1':
                    case 'Фото 2':
                    case 'Фото 3':
                    case 'Цена продажи':
                    case 'Старая цена продажи':
                    case 'Цена закупки':
                    case 'Старая цена закупки':
                    case 'Валюта закупки':
                    case 'Дата съемки':

                        break;

                    default:
                        $filters_forsage[$char->name] = $char->value;
                        break;
                }
            }
        }

        $prices = $this->setPrices($characteristics, $forsage_id, $product_id);
        if (!$prices['result']) {


            return $prices;
        }

        //$product['images'] = $images;
        $product['product_special'] = $prices['product_special'];
        $product['cost_price'] = $prices['cost_price'];
        $product['price'] = $prices['price'];

        $child_category_name = $data_forsage->name;
        $category_name = '';
        $child_category = '';
        if (isset($data_forsage->category)) {
            $category = $data_forsage->category;
            $category_name = $category->name;

            if (isset($category->child))
                $child_category = $category->child->name;

        }

        if (strpos($quantity, "-") !== false) // под заказ
        {
            $product['stock_status_id'] = 6;
            $product['status'] = 1;
        } elseif ((int)$quantity == 0) {
            $product['quantity'] = 0;
            $product['stock_status_id'] = 5;
            $product['status'] = 1;
        } else {
            $product['stock_status_id'] = 7;
            $product['status'] = 1;
        }
//        echo '<pre>$supplier';
//        print_r($supplier);
//        echo '</pre>';
        //

        $manufacturer = $this->setManufacturer($supplier, $supplier->id, $product_id);
//        echo '<pre>$manufacturer id';
//        print_r($manufacturer);
//        echo '</pre>';
        $product['manufacturer_id'] = $manufacturer;

        $product['forsage_id'] = $forsage_id;
        $product['categories'] = $this->setCategories($sex, $category_name, $child_category, $filters_forsage, (int)$supplier->id);

//        echo '<pre>$product';
//        print_r($product);
//        echo '</pre>';

        $filters = $this->setFilters($filters_forsage, $forsage_id, $product_id, $category_name, $child_category, $languages, $togrovay_marka, $sex, $product['categories']);
//        echo '<pre>$filters';
//        print_r($filters);
//        echo '</pre>';
        $attributes = $this->setAttributes($filters_forsage, $languages, $togrovay_marka);
//        echo '<pre>$attributes';
//        print_r($attributes);
//        echo '</pre>';
        $product['filters'] = $filters;
        $product['attributes'] = $attributes;


        if (count($images) > 1)
            $product['images'] = $images;
        else $product['images'] = [];
        $add_update = true;


        $_forsage_images = [];

        foreach ($characteristics as $char) {
            switch ($char->type) {
                case 'image':
                    $image = '';
                    $image_path = (string)$char->value;

                    if ($image_path != '') {
                        $_forsage_images[] = $image_path;
                    }
                    break;
            }
        }


        if (count($images) > 0) {
            $product['image'] = $images[0]['image'];
            if ($add_update) {
                $products_data[] = $product;
                $result = $this->processFromForsage($product, $product_data, $languages, $stores);

            }
        } else {
            if (count($_forsage_images) > 0) {

                $products_data[] = $product;
                $product['image'] = $product_data['image'];
//                echo '<pre>$product';
//                print_r($product);
//                echo '</pre>';

                //'image' => $product_data['image'],
                $result = $this->processFromForsage($product, $product_data, $languages, $stores);
            }

        }

        return $result;

    }


    /**
     *
     *///getProducts($period=false,$period_finish=false, $stock=false)
    public function importapi($store, $stores, $period = '-17 minutes', $finish = false, $stock = true)
    {
        $start_date = $period;
        $finish_date = $finish;


        $this->load->model('catalog/product');

        $this->load->model('extension/ocfilter');

        $this->load->model('catalog/option');
        $this->load->model('localisation/language');
        $this->load->model('catalog/manufacturer');
        $this->load->model('setting/setting');
        $this->load->model('catalog/filter');
        $this->load->model('localisation/currency');
        $this->load->model('catalog/filter');
        $this->load->model('catalog/category');
        $this->load->model('localisation/stock_status');
        $this->load->model('forsage/api');


        $forsage_api = $this->model_forsage_api;


        $products_for_exel = [];

        $languages = $this->model_localisation_language->getLanguages();

        $new_brands = [];

        $count_add = $errors = 0;
        $count_edit = 0;
        $data_for_exel = [];

        $new_products = $forsage_api->getProducts($start_date, $finish_date, $stock);


        error_reporting(E_ALL & E_STRICT);
        ini_set('display_errors', 'on');
        ob_implicit_flush(1);

        echo str_pad('', 1024);
        @ob_flush();
        flush();


        if ($new_products->success) {
            foreach ($new_products->products as $prod) {
                $forsage_id = $prod->id;
                $_data_forsage = $forsage_api->GetProduct($forsage_id);

                if ($_data_forsage->success) {
                    $data_forsage = $_data_forsage->product;


                    $return_product = $this->processupdate($data_forsage, $languages, $store, $stores);

                    if ($return_product) {
                        $errors = $errors + count($return_product['errors']);
                        $new_brands = array_merge($new_brands, $return_product['new_brands']);
                        $count_edit = $count_edit + $return_product['count_edit'];
                        $count_add = $count_add + $return_product['count_add'];
                    }
                    @ob_flush();
                    flush();
                }
            }
        }


//        echo 'Ошибки = ' . $errors . "\r\n";
//        echo 'Добавленные товары = ' . $count_add . "\r\n";
//        echo 'Обновленные товары = ' . $count_edit . "\r\n";
//        echo 'Новые бренды = ' . implode(', ', $new_brands) . "\r\n\r\n";

    }


    /**
     * @param $product_data -  новый массив
     * @param $data_exist - существующий товар (если нет то пустой массив)
     * @param $languages - массив языков
     * @param $stores - массиво магазинов
     */
    public function processFromForsage($product_data, $data_exist, $languages, $stores)
    {

        if ($product_data['forsage_id'] > 0) {
            $model = $product_data['model'];
            $name = $product_data['name'];
            $data = [
                'model' => $model,//$product_data['model'],
                'sku' => $product_data['sku'],
                'cost_price' => $product_data['cost_price'],
                'price' => $product_data['price'],
                'currency_id' => $product_data['currency_id'],
                'image' => $product_data['image'],
                'quantity' => null,
                'sku' => $product_data['sku'],
                'upc' => '',
                'jan' => '',
                'ean' => '',
                'isbn' => '',
                'mpn' => '',
                'location' => '',
                'minimum' => 1,
                'subtract' => 0,
                'stock_status_id' => $product_data['stock_status_id'],
                'date_available' => date('Y-m-d'),
                'manufacturer_id' => isset($product_data['manufacturer_id']) ? $product_data['manufacturer_id'] : '',
                'shipping' => 1,
                'points' => '',
                'weight' => '',
                'weight_class_id' => $this->config->get('config_weight_class_id'),
                'length' => '',
                'width' => '',
                'height' => '',
                'length_class_id' => $this->config->get('config_length_class_id'),
                'status' => $product_data['status'],
                'tax_class_id' => 0,
                'sort_order' => 1,
                'forsage_id' => $product_data['forsage_id'],
            ];


            $forsage_id = $product_data['forsage_id'];


            $data['product_description'] = [];
            if (!empty($languages)) {
                foreach ($languages as $lang) {
                    $data['product_description'][$lang['language_id']] = [
                        'name' => htmlentities($product_data['name']),
                        'meta_title' => '',
                        'description' => '',
                        'tag' => '',
                        'meta_description' => '',
                        'meta_keyword' => ''
                    ];
                    break;
                }
            }

            $data['product_store'] = [];

            $data['product_store'][] = 0;

            $data['product_seo_url'] = [];
            $model = str_replace('&', '-', $model);
            if (!empty($languages)) {
                foreach ($data['product_store'] as $store_id) {
                    foreach ($languages as $lang) {

                        $data['product_seo_url'][$store_id][$lang['language_id']] = preg_replace('/(\.|\/)/', '-', mb_strtolower($this->model_extension_ocfilter->translit($model)));
                    }
                }
            }

            if (!empty($product_data['special'])) {
                $data['product_special'] = [
                    0 => ['price' => $product_data['special'], 'customer_group_id' => (int)$this->config->get('config_customer_group_id')]
                ];
            }

            if (isset($product_data['filters']) && !empty($product_data['filters'])) {
                $data['filters'] = $product_data['filters'];
            }

            if (isset($product_data['attributes']) && !empty($product_data['attributes'])) {
                $data['product_attribute'] = $product_data['attributes'];
            }

            if (!empty($product_data['images'])) {
                $data['product_image'] = $product_data['images'];
            }

            if (isset($product_data['categories']) && !empty($product_data['categories'])) {
                $data['product_category'] = $product_data['categories'];
            }


            $price_update = false;
            $new_cost_price = $product_data['cost_price'];
            //$old_price = $data_exist['cost_price'];
//            if (!empty($product_data['special'])) {
//
//                if ((float)$new_cost_price == (float)$old_price) {
//
//
//                    $special_price = $product_data['special'];
//                    if ($product_data['special'] > 0) {
//                        if ((float)$product_data['cost_price'] >= (float)$product_data['special']) {
//                            $price_update = false;
//                        } else {
//                            $price_update = true;
//                        }
//                    } else $price_update = true;
//
//                } else {
//
//                    $data['product_special'] = [];
//                    if ((float)$data['cost_price'] < (float)$data['price']) {
//                        $price_update = true;
//                    }
//
//                }
//
//            } else {
//
//                if ((float)$data['cost_price'] < (float)$data['price']) {
//                    $price_update = true;
//                } else {
//
//                }
//            }
            $price_update = true;
//            echo '<pre>$data_exist';
//            print_r($data_exist);
//            echo '</pre>';

            if ($price_update) {
                if (!empty($data_exist)) {
                    if ((float)$data_exist['price'] > (float)$product_data['price']) {
                        $data['price'] = (float)$data_exist[0]['price'];
                        $data['product_special'] = [
                            0 => [
                                'price' => (float)$product_data['price'],
                                'customer_group_id' => (int)$this->config->get('config_customer_group_id'),
                                'priority' => 0,
                                'date_start' => '',
                                'date_end' => ''
                            ]
                        ];
                    } elseif ((float)$data_exist['price'] < (float)$product_data['price']) {
                        $data['product_special'] = [];

                    }


//                    echo '<pre>$data edit';
//                    print_r($data);
//                    echo '</pre>';
                    $product_id = $this->model_catalog_product->editProduct($data_exist['product_id'], $data);

                    $this->model_catalog_product->saveOcfilters($data_exist['product_id'], $data);
                    $result['edited_products'][] = $data_exist['product_id'];
                } else {

//                    echo '<pre>$data add'.$product_id;
//                    print_r($data);
//                    echo '</pre>';
                    $product_id = $this->model_catalog_product->addProduct($data);
//                    echo '<pre>$data add'.$product_id;
//                    print_r($data);
//                    echo '</pre>';
                    $this->model_catalog_product->saveOcfilters($product_id, $data);


                    if (!empty($product_id)) {

                        //echo 'added_products = ' . $product_id . " " . $data['model'] . "\r\n";

                        $result['added_products'][] = $product_id;
                    } else {

                        $result['error_add_products'][] = $data;
                    }
                }
            } else
                $errors_for_update[] = $data;

            return $result;//['added_products'];//['errors_for_update' => $errors_for_update, 'edited_products' => $result['edited_products'], 'added_products' => $result['added_products']];
        } else return false;

    }

    public function createAttribute($key)
    {
        $this->load->model('catalog/attribute');
        $attribute = $this->model_catalog_attribute->getAttributes(['filter_name' => $key, 'filter_attribute_group_id' => 16]);
        if (count($attribute) > 0) {
            foreach ($attribute as $attr) {

                return $attr['attribute_id'];
            }
        }
        $attribute = $this->model_catalog_attribute->addAttribute([
            'attribute_group_id' => 16,
            'sort_order' => 0,
            'attribute_description' => [
                2 => [
                    'name' => $key
                ]
            ]

        ]);
        return $attribute;
    }

    public function setAttributes($filters_forsage, $languages, $togrovay_marka)
    {

        $result = [];
        foreach ($filters_forsage as $_key => $value) {
            if ($value != '') {

                switch (trim($_key)) {
                    case 'Валюта закупки':
                    case 'Валюта продажи':
                    case 'Дата съемки':
                    case 'Фото 1':
                    case 'Фото 2':
                    case 'Фото 3':
                        continue;
                        break;
                }

                $attributeId = $this->createAttribute($_key);
                //$result[$attributeId] = $value;
                $result[$attributeId] = ['attribute_id' => $attributeId,
                    'product_attribute_description' => [
                        2 => ['text' => $value]
                    ]
                ];

            }
        }


        $attributeId = $this->createAttribute('Бренд');
        $result[$attributeId] = ['attribute_id' => $attributeId,
            'product_attribute_description' => [
                2 => ['text' => $togrovay_marka]
            ]
        ];//$togrovay_marka;

        return $result;
    }

    /**
     * @param $filters_forsage
     * @param $settings_forsage
     * @param $forsage_id
     * @param $product_id
     * @param $category_name
     * @param $child_category
     * @param $languages
     * @param string $torgovay
     * @return array
     */
    public function setFilters($filters_forsage, $forsage_id, $product_id, $category_name, $child_category, $languages, $togrovay_marka, $sex = '', $categories = [])
    {

        $filters = [];

        foreach ($filters_forsage as $_key => $value) {
            $key = $_key;
            if ($value != '') {

                $type = 'checkbox';
                switch ($_key) {
                    case 'Комментарии (доп. описание)':
                        //$this->createAttribute($_key,$value);
                        continue;
                        break;
                    case 'Валюта закупки':
                    case 'Валюта продажи':
                    case 'Дата съемки':
                    case 'Фото 1':
                    case 'Фото 2':
                    case 'Бренд':
                    case 'Фото 3':
                    case 'Видеообзор':
                        continue;
                        break;
                }

                //ocfilter_option_to_category
                $options = $this->model_extension_ocfilter->getOptionByName(['name' => $key, 'categories' => $categories]);
                if (count($options) > 0) {


                    foreach ($options as $option) {
                        $optionId = $option['option_id'];
                        //$filterId = $this->model_extension_ocfilter->getOptionByName(['name' => $key]);
                        $values = $option['values'];
                        $valueOption = false;
                        foreach ($values as $valOption) {
                            if ($valOption['name'] == $value) {
                                $valueOption = $valOption['value_id'];
                            }
                        }

                        if (!$valueOption) {
                            $valueOption = $this->create_filter($value, $languages, $optionId);
                        }

                        if ($valueOption != '')
                            $filters[$optionId] = $valueOption;
                    }
                } else {


                    $optionId = $this->create_filter_option($key, $categories, $type);
                    if ($optionId)
                        $valueOption = $this->create_filter($value, $languages, $optionId);


                    if ($valueOption != '')
                        $filters[$optionId] = $valueOption;

                }
            }


        }

        $brandId = 10085;

        //echo '$brandId = '.$brandId."\r\n\r\n";
        $options = $this->model_extension_ocfilter->getOptionById($brandId);//Name(['name' => 'Бренд', 'categories' => $categories]);
        if (count($options) > 0) {
            foreach ($options as $option) {
                $optionId = $option['option_id'];
                //$filterId = $this->model_extension_ocfilter->getOptionByName(['name' => $togrovay_marka]);
                $values = $option['values'];
                $valueOption = false;

                foreach ($values as $valOption) {
                    if ($valOption['name'] == $togrovay_marka) {
                        $valueOption = $valOption['value_id'];
                    }
                }

                if (!$valueOption) {
                    $valueOption = $this->create_filter($togrovay_marka, $languages, $optionId);
                    $filters[$optionId] = $valueOption;
                } else
                    $filters[$optionId] = $valueOption;


                break;

            }
        }

        return $filters;
    }


    public function createCategory($child_category, $parentId = false)
    {

        $category_id = false;
        $data = [];
        $data['category_description'] = [
            2 => [
                'name' => $child_category,
                'description' => $child_category,
                'meta_title' => $child_category,
                'meta_description' => $child_category,
                'meta_keyword' => $child_category
            ]
        ];
        $data['category_store'] = [0];

        $data['category_seo_url'] = [
            0 => [
                2 => $this->model_extension_ocfilter->translit($child_category)
            ]
        ];

        if (!$parentId)
            $data['parent_id'] = 0;
        else
            $data['parent_id'] = $parentId;
        $data['column'] = 0;
        $data['sort_order'] = 0;
        $data['status'] = 1;

        if ($category_id = $this->model_catalog_category->addCategory($data))
            return [
                0 => ['category_id' => $category_id]
            ];
        else return false;
    }

    public function setCategories($sex, $category_name, $child_category = '', $filters_forsage = [], $supplierId = false)
    {
        $categories = [];

        //echo "\r\n\r\n!!$sex = ".$sex . ' $category_name=' . $category_name . ' $child_category=' . $child_category . "\r\n\r\n\r\n";
        if ($sex != '') {


            if ($category_name == 'Обувь') {
                if ($sex == 'женщины') {
                    $parentName = 'Женская обувь';


                } else if ($sex == 'мужчины') {
                    $parentName = 'Мужская обувь';
                    //$category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => ]);
                } else if ($sex == 'подросток' || $sex == 'дети') {
                    $parentName = 'Детская обувь';
                } else
                    $parentName = false;


                if (!$parentName) {
                    $category_info = $this->createCategory($parentName);
                    //echo '!!new = '.$category_name . ' ' . $child_category . ' ' . $parentName . ' $sex ' . $sex . "\r\n";

                } else {
                    $category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => $parentName, 'parent' => 0]);
                }

                if (!empty($category_info)) {
                    //$categories[] = $category_info[0]['category_id'];

                    if ($child_category != '') {
                        $category_info_child = $this->model_catalog_category->getCategoriesByData(['parent' => $category_info[0]['category_id'], 'filter_name' => $child_category]);
                        if (!empty($category_info_child)) {
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        } else {
                            $category_info_child = $this->createCategory($child_category, $category_info[0]['category_id']);
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        }
                    } else $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
                    //$categories[] = $this->createCategory('Аксессуары');

                }

            } else {
                $category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => 'Аксессуары', 'parent' => 0]);
                if (!empty($category_info)) {
                    //$categories[] = $category_info[0]['category_id'];
                    if ($child_category != '') {
                        //$category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => 'Аксессуары','parent' =>0]);
                        $category_info_child = $this->model_catalog_category->getCategoriesByData(['parent' => $category_info[0]['category_id'], 'filter_name' => $child_category]);
                        if (!empty($category_info_child)) {
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        } else {
                            $category_info_child = $this->createCategory($child_category, $category_info[0]['category_id']);
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        }
                    } else $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
                } else {
                    $category_info = $this->createCategory('Аксессуары');
                    if ($child_category != '') {
                        $category_info_child = $this->model_catalog_category->getCategoriesByData(['parent' => $category_info[0]['category_id'], 'filter_name' => $child_category]);
                        if (!empty($category_info_child)) {
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        } else {
                            $category_info_child = $this->createCategory($child_category, $category_info[0]['category_id']);
                            $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
                        }
                    } else $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
                    //$categories[] = $category_info[0]['category_id'];
                }
            }

        }
//        else {
//            $category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => 'Аксессуары']);
//            if (!empty($category_info)) {
//                $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
//                if ($child_category != '') {
//                    $category_info_child = $this->model_catalog_category->getCategoriesByData(['parent' => $parentName, 'filter_name' => $child_category]);
//                    if (!empty($category_info_child)) {
//                        $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
//                    } else {
//                        $category_info = $this->createCategory($child_category, $category_info[0]['category_id']);
//                        $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
//                    }
//                } else $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
//            } else {
//                $category_info = $this->createCategory('Аксессуары');
//                if ($child_category != '') {
//                    $category_info_child = $this->model_catalog_category->getCategoriesByData(['parent' => $parentName, 'filter_name' => $child_category]);
//                    if (!empty($category_info_child)) {
//                        $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
//                    } else {
//                        $category_info_child = $this->createCategory($child_category, $category_info[0]['category_id']);
//                        $categories[$category_info_child[0]['category_id']] = $category_info_child[0]['category_id'];
//                    }
//                } else $categories[$category_info[0]['category_id']] = $category_info[0]['category_id'];
//                //$categories[] = $category_info[0]['category_id'];
//            }
//        }

        if ($supplierId == 91) {

            if (isset($filters_forsage['Страна'])) {
                if ($filters_forsage['Страна'] == 'Украина') {
                    $categories[256] = 256;
                }

            }
        }


        return $categories;

    }

    public function create_filter_option($key, $categories, $type = 'checkbox')
    {
        $data = [];
        $data['status'] = 1;
        $data['sort_order'] = 1;
        $data['type'] = 'checkbox';
        $data['selectbox'] = 0;
        $data['color'] = '';
        $data['image'] = '';
        $data['ocfilter_option_description'] = [
            2 => ['name' => $key,
                'description' => $key,
                'postfix' => ''
            ]
        ];
        $data['option_store'] = [0];
        $data['category_id'] = [0];


        $filter_info = $this->model_extension_ocfilter->addOption($data);
        return $filter_info;


    }

    /**
     * @param $filter_name
     * @param bool $filter_group_id
     * @param $languages
     * @return array
     */
    public function create_filter($filter_name, $languages, $filter_group_id = false)
    {

        $data_record = [];
        $data_record['option_id'] = $filter_group_id;
        $data_record['color'] = '';
        $data_record['image'] = '';
        $data_record['sort_order'] = '';
        $data_record['language'] = [];
        foreach ($languages as $lang) {
            $data_record['language'][$lang['language_id']] = ['name' => $filter_name];
        }

        $filter_info = $this->model_extension_ocfilter->addOptionValue($data_record);

        if (!empty($filter_info)) {
            //$category_info = $this->model_catalog_category->getCategoriesByData(['filter_name' => $parentName,'parent' =>0]);
            return $filter_info;//[$filter_group_id => $filter_info];
        }
        return false;

    }

    public function updateImages($data, $model, $forsage_path, $forsage_id = false)
    {
        $images = [];
        $sort_order = 0;

        if ($forsage_id) $forsage_name = $forsage_id . '-' . $model;
        else $forsage_name = $model;
        foreach ($data as $char) {
            switch ($char->type) {
                case 'image':
                    $image = '';
                    $image_path = (string)$char->value;
                    if ((string)$char->name == 'Фото 1') {

                        $image = $this->getImage($image_path, $forsage_name . '/' . $forsage_path);//$store['forsage_path']);//, $sort_order . '_');
                        $sort_order = 1;
                        //$product['image'] = $imageBasee;
                    } elseif ((string)$char->name == 'Фото 2') {

                        $sort_order = 2;
                        $image = $this->getImage($image_path, $forsage_name . '/' . $forsage_path);//$store['forsage_path'], $sort_order . '_');
                    } elseif ((string)$char->name == 'Фото 3') {
                        $sort_order = 3;
                        $image = $this->getImage($image_path, $forsage_name . '/' . $forsage_path);//$store['forsage_path'], $sort_order . '_');
                    }
                    if ($image != '') {
                        $images[] = [
                            'image' => $image,
                            'store_id' => 0,//$store['store_id'],
                            'sort_order' => $sort_order
                        ];
                    }
                    break;
            }
        }
        return $images;

    }

    public function setPrices($data, $forsage_id, $product_id = false)
    {


        $price = $_old_price = $cost_price = $cost_price_old = 0;
        $curs = 1;
        $currency_id = false;
        foreach ($data as $char) {
            switch ($char->name) {
                case 'Валюта закупки':
                    //гривна
                    $currency_text = $char->value;
                    if (preg_match('/доллар?|\$/', $currency_text)) {
                        $currency_info = $this->model_localisation_currency->getCurrencyByCode('UAH');
                        $curs = $currency_info['value'];
                    }
                    break;

            }
        }


        foreach ($data as $char) {
            switch ($char->name) {
                case 'Цена продажи':
                    $price = $char->value;
                    break;
                case 'Старая цена продажи':
                    $_old_price = $char->value;
                    break;
                case 'Цена закупки':
                    $cost_price = $char->value;
                    break;
                case 'Старая цена закупки':
                    $cost_price_old = $char->value;
                    break;

            }
        }
        $currency_id = false;
        $price_val = $price;
        $cost_price_val = $cost_price;
        foreach ($data as $char) {
            switch ($char->name) {
                case 'Валюта закупки':
                    //гривна
                    $currency_text = $char->value;
                    if (preg_match('/доллар?|\$/', $currency_text)) {
                        $currency_info = $this->model_localisation_currency->getCurrencyByCode('UAH');

                        $curs = $currency_info['value'];
                        $currency_id = 2;
                        $cost_price_val = round($cost_price * $curs, 0);
                    } else {
                        $currency_id = 1;
                        $cost_price_val = $cost_price;
                    }
                    break;

                case 'Валюта продажи':
                    //гривна
                    $currency_text = $char->value;
                    if (preg_match('/доллар?|\$/', $currency_text)) {
                        $currency_info = $this->model_localisation_currency->getCurrencyByCode('UAH');

                        $curs = $currency_info['value'];
                        $currency_id = 2;
                        $cost_price_val = round($cost_price * $curs, 0);

                        $price_val = round($price * $curs, 0);
                    } else {
                        $currency_id = 1;
                        $cost_price_val = $cost_price;
                    }
                    break;
            }
        }


        $result = [];
        $product_special = [];
        $result = [
            'result' => false,
            'product_special' => [],
            'price' => 0,
            'cost_price' => 0,
            'message' => [],
            'forsage_id' => $forsage_id,
            'product_id' => $product_id
        ];


        if (($price == 0) || ($cost_price == 0)) {

            $result = [
                'result' => false,
                'message' => 'Проблемы с ценой',
                'forsage_id' => $forsage_id,
                'product_id' => $product_id
            ];
        } else {
            $result['result'] = true;
            $result['price'] = $price;
            $result['cost_price'] = $cost_price;
            if ($price < $_old_price) {
                if ($price > 0) {
                    $product_special = [
                        0 => ['price' => $price, 'customer_group_id' => (int)$this->config->get('config_customer_group_id')]
                    ];
                } else $product_special = [];
                $result['product_special'] = $product_special;
            }
        }


        return $result;
    }

    public function setManufacturer($supplier, $forsage_id, $product_id = false)
    {
        $return = [
            'result' => true,
            'message' => '',
            'manufacturer_id' => false,
            'new_brands' => [],
            'new_manufacturer' => false,
            'send_mail' => false
        ];
        $manufacturer_id = false;

        $supplier_name = $supplier->company;

        //echo $supplier_name . ' ' . htmlspecialchars($supplier_name) . "\r\n\r\n";

        //$supplier_name = $supplier->company;
        //if ($supplier_name != 'No brand') {
        $manufacturer_info = $this->model_catalog_manufacturer->getManufacturerByName($supplier_name);

        if ($manufacturer_info) {//!empty($manufacturer_info)) {
            $manufacturer_id = $manufacturer_info['manufacturer_id'];
            $return['manufacturer_id'] = $manufacturer_id;
//            echo '<pre>';
//            print_r($return['manufacturer_id']);
//            echo '</pre>';

            return $manufacturer_id;
        } else {
//            echo '<pre>$manufacturer_info';
//            print_r($manufacturer_info);
//            echo '</pre>';
            echo 'new manufacturer $supplier_name' . $supplier_name . "\r\n\r\n";

            //die();
            $address = $supplier->address;

            $return['new_manufacturer'] = true;

            $new_brand = [
                'name' => $supplier_name,
                'email' => $supplier->email,
                'phone' => $supplier->phone,
                'address' => $address,
                'status' => 1,
                'sort_order' => 0,
                'forsage_id'=>$forsage_id
            ];


            // создать бренд
            $new_brand['manufacturer_store'] = [0];//, 1];
            $manufacturer_seo_url = $this->model_extension_ocfilter->translit(str_replace([' ', '&amp;'], ['_', 'and'], mb_strtolower($new_brand['name'])));
            $new_brand['manufacturer_seo_url'] = [
                0 => [
                    2 => $manufacturer_seo_url,
                    3 => $manufacturer_seo_url
                ],
                1 => [
                    2 => $manufacturer_seo_url,
                    3 => $manufacturer_seo_url
                ]
            ];

            $new_brand['status'] = 1;

            echo '<pre>$new_brand';
            print_r($new_brand);
            echo '</pre>';
            $manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($new_brand);
            return $manufacturer_id;

            //$return['manufacturer_id'] = $manufacturer_id;

        }
        //}
        return $return;//$manufacturer_id;
    }


}
