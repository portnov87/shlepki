<?php
class ControllerCommonLanguage extends Controller {
	public function index() {
		$this->load->language('common/language');

		$data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

		$data['code'] = $this->session->data['language'];

		$this->load->model('localisation/language');

		$data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();
        $url_data = $this->request->get;
        unset($url_data['_route_']);

        $route = $url_data['route'];


        unset($url_data['route']);
        unset($url_data['lng']);

		/*foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'name' => $result['name'],
					'code' => $result['code']
				);
			}
		}*/

        foreach ($results as $result) {
            if ($result['status']) {
                $expl = (explode('-', $result['code']));

                $url = '';
                if (!empty($url_data)) {
                    $url = '&' . urldecode(http_build_query($url_data, '', '&'));
                }
                $url .= '&lng=' . $result['code'];


                if ($route == '') {
                    if ($result['code'] == 'uk-ua')
                        $_url = '/ua';
                    else $_url = '/';

                } else
                    $_url = $this->url->link($route, $url);

                $data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code'],
                    'url' => $_url,
                    'code_name' => ucfirst(trim($expl[1]))
                );
            }
        }

        if (!isset($this->request->get['route']) || (isset($this->request->get['route']) && substr($this->request->get['route'], 0, 4) == 'api/')) {
            $data['redirect'] = $this->url->link('common/home');
            $lang = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
            $data['code'] = $lang['code'];
        } else {
            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data['route'];

            unset($url_data['route']);
            unset($url_data['lng']);
            unset($url_data['filters_group']);

            $url = '';

            if (!empty($url_data)) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }


            $data['redirect'] = $this->url->link($route, $url);
        }

        if (isset($this->request->get['route'])) {
            $data['route'] = $this->request->get['route'];
        } else {
            $data['route'] = 'common/home';
        }

        if (isset($url)) {
            $data['url'] = $url;
        } else {
            $data['url'] = '';
        }

        /*

		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}
*/
		return $this->load->view('common/language', $data);
	}

	public function language() {
		/*if (isset($this->request->post['code'])) {
			$this->session->data['language'] = $this->request->post['code'];
		}

		if (isset($this->request->post['redirect'])) {
			$this->response->redirect($this->request->post['redirect']);
		} else {
			$this->response->redirect($this->url->link('common/home'));
		}*/
        if (isset($this->request->post['code'])) {
            $this->session->data['language'] = $this->request->post['code'];
        }else
            $this->session->data['language'] = 'ru';

        if (isset($this->request->post['route']) && isset($this->request->post['url'])) {
            $this->request->get['lng'] = $this->request->post['code'];
            $this->request->post['redirect'] = $this->url->link($this->request->post['route'], str_replace('&amp;', '&', $this->request->post['url']));
        }else{
            $this->request->get['lng'] = 'ru';
        }

//        echo '<pre>';
//        print_r($this->session->data);
//        echo '</pre>';
//        echo '<pre>post';
//        print_r($this->request->post);
//        echo '</pre>';
//        echo '<pre>get';
//        print_r($this->request->get);
//        echo '</pre>';
//        die();
        if (isset($this->request->post['redirect'])) {
            if ($this->request->post['redirect']=='https://shlepki.com.ua/')
            {
                $url = 'https://shlepki.com.ua/ru';
            }
            else {
//            if ($url=='https://shlepki.com.ua/')
//                $url = $this->request->post['redirect'];
//                else

                $url = $this->request->post['redirect'];
            }
        } else {
            $url = $this->url->link('common/home');
        }

        $this->response->redirect($url);
	}
}