<?php
class ControllerAccountAccountNav extends Controller {
  public function index($data) {

    $this->load->language('account/account');

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    $data['edit'] = $this->url->link('account/edit', '', true);
    $data['password'] = $this->url->link('account/password', '', true);
    $data['address'] = $this->url->link('account/address', '', true);
    $data['account'] = $this->url->link('account/account', '', true);

    $data['credit_cards'] = array();

    $files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');

    foreach ($files as $file) {
      $code = basename($file, '.php');

      if ($this->config->get('payment_' . $code . '_status') && $this->config->get('payment_' . $code . '_card')) {
        $this->load->language('extension/credit_card/' . $code, 'extension');

        $data['credit_cards'][] = array(
          'name' => $this->language->get('extension')->get('heading_title'),
          'href' => $this->url->link('extension/credit_card/' . $code, '', true)
        );
      }
    }

    $data['logout'] = $this->url->link('account/logout');
    $data['wishlist'] = $this->url->link('account/wishlist');
    $data['viewed'] = $this->url->link('account/viewed');
    $data['order'] = $this->url->link('account/order', '', true);
    $data['download'] = $this->url->link('account/download', '', true);

    if ($this->config->get('total_reward_status')) {
      $data['reward'] = $this->url->link('account/reward', '', true);
    } else {
      $data['reward'] = '';
    }

    $data['return'] = $this->url->link('account/return', '', true);
    $data['transaction'] = $this->url->link('account/transaction', '', true);
    $data['newsletter'] = $this->url->link('account/newsletter', '', true);
    $data['recurring'] = $this->url->link('account/recurring', '', true);


    $this->load->model('account/customer');

    $affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());

    if (!$affiliate_info) {
      $data['affiliate'] = $this->url->link('account/affiliate/add', '', true);
    } else {
      $data['affiliate'] = $this->url->link('account/affiliate/edit', '', true);
    }

    if ($affiliate_info) {
      $data['tracking'] = $this->url->link('account/tracking', '', true);
    } else {
      $data['tracking'] = '';
    }








      return $this->load->view('account/account_nav', $data);
  }

}
