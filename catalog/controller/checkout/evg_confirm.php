<?php

class ControllerCheckoutEvgConfirm extends Controller
{
    public function getTotals()
    {
        // Totals
        $this->load->model('setting/extension');

        $totals = array();
        $taxes = 0;//$this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes' => &$taxes,
            'total' => &$total
        );

        // Display prices
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get('total_' . $result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);
        }

        $data['totals'] = array();

        foreach ($totals as $total) {
            $data['totals'][] = array(
                'title' => $total['title'],
                'text' => $this->currency->format($total['value'], $this->session->data['currency'], '', true, true)
            );
        }
        die(json_encode($totals));
    }

    public function index()
    {

        $redirect = '';
        $telegramData = [];

        $this->load->controller('checkout/evg_addsession');


        // Validate if shipping address has been set.
        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {
            $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $shipping_address = $this->session->data['guest']['shipping'];
        }

        if (empty($shipping_address)) {
            $redirect = $this->url->link('checkout/lite_checkout', '', 'SSL');
        }

        // Validate if shipping method has been set.
        if (!isset($this->session->data['shipping_method'])) {
            $redirect = $this->url->link('checkout/lite_checkout', '', 'SSL');
        }

        // Validate if payment address has been set.
        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
            $payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            $payment_address = $this->session->data['guest']['payment'];
        }

        if (empty($payment_address)) {
            $redirect = $this->url->link('checkout/checkout', '', 'SSL');
        }

        // Validate if payment method has been set.
        if (!isset($this->session->data['payment_method'])) {
            $redirect = $this->url->link('checkout/checkout', '', 'SSL');
        }

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers']))) {
            $redirect = $this->url->link('checkout/cart');
        }

        // Validate minimum quantity requirments.
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $redirect = $this->url->link('checkout/cart');
                break;
            }
        }

        if (!$redirect) {
            // Totals
            $this->load->model('setting/extension');

            $this->load->model('catalog/product');
            $this->load->model('catalog/category');
          //  $this->load->model('catalog/filter');

            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;

            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes' => &$taxes,
                'total' => &$total
            );

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get('total_' . $result['code'] . '_status')) {
                    $this->load->model('extension/total/' . $result['code']);

                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                }
            }

            $sort_order = array();

            foreach ($totals as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $totals);

            $this->load->language('checkout/checkout');

            $order_data = array();

            $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');

            $this->load->model('account/customer');

//            echo '<pre>';
//            print_r($this->request->post);
//            echo '</pre>';
//            echo '<pre>';
//            print_r($this->session->data['guest']);
//            echo '</pre>';
//
//            die();
            if ($this->customer->isLogged()) {
                $order_data['customer_id'] = $this->customer->getId();
                $order_data['customer_group_id'] = $this->customer->getGroupId();
                //$order_data['customer_guest'] = $this->customer->getGuest();
                $order_data['firstname'] = isset($this->request->post['personal_data']['name']) ? $this->request->post['personal_data']['name'] : $this->customer->getFirstName();
                $order_data['lastname'] = isset($this->request->post['personal_data']['lname']) ? $this->request->post['personal_data']['lname'] : $this->customer->getLastName();
                $order_data['email'] = isset($this->request->post['personal_data']['email']) ? $this->request->post['personal_data']['email'] : $this->customer->getEmail();
                $order_data['telephone'] = isset($this->request->post['personal_data']['phone']) ? $this->request->post['personal_data']['phone'] : $this->customer->getTelephone();
                //$order_data['inputs'] = isset($this->session->data['guest']['inputs']) ? $this->session->data['guest']['inputs'] : '';

                $this->load->model('account/address');

                $payment_address = $this->session->data['guest']['payment'];
                //$payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
            } elseif (isset($this->session->data['guest'])) {

//          echo '<pre>guest';
//          print_r($this->session->data['guest']);
//          echo '</pre>';
//          die();

//                if ($this->model_account_customer->getTotalCustomersByEmailOrPhone($this->session->data['guest']['email'], $this->session->data['guest']['telephone'])) {
//
//                    $customer_info = $this->model_account_customer->getCustomerByEmailOrPhone($this->session->data['guest']['email'], $this->session->data['guest']['telephone']);
//                    $customer_id = $customer_info['customer_id'];
//                    $this->model_account_customer->editCustomer($customer_id, [
//                        'firstname' => $this->session->data['guest']['firstname'],
//                        'lastname' => $this->session->data['guest']['lastname'],
//                        'telephone' => $this->session->data['guest']['telephone'],
//                        'email' => $customer_info['email'],
//                        'birthday' => $customer_info['birthday'],
//                        'password' => $customer_info['password'],
//                        'guest' => '1'
//                    ]);
//                    $customer_group_id = $customer_info['customer_group_id'];
//                    $customer_guest = $customer_info['guest'];
//
//                } else {
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    $customer_id = $this->model_account_customer->addCustomer([
                        'firstname' => $this->session->data['guest']['firstname'],
                        'lastname' => $this->session->data['guest']['lastname'],
                        'email' => $this->session->data['guest']['email'],
                        'telephone' => $this->session->data['guest']['telephone'],
                        'birthday' => '',
                        'password' => substr(str_shuffle($chars), 0, 8),
                        'guest' => '1'
                    ]);
                    $customer_group_id = CUSTOMER_GROUP_ID;
                    $customer_guest = '1';
                //}
                $order_data['customer_guest'] = $customer_guest;
                $order_data['customer_id'] = $customer_id;
                $order_data['customer_group_id'] = $customer_group_id;
                $order_data['firstname'] = $this->session->data['guest']['firstname'];
                $order_data['lastname'] = $this->session->data['guest']['lastname'];
                $order_data['email'] = $this->session->data['guest']['email'];
                $order_data['telephone'] = $this->session->data['guest']['telephone'];
                $order_data['inputs'] = isset($this->session->data['guest']['inputs']) ? $this->session->data['guest']['inputs'] : '';

                $payment_address = $this->session->data['guest']['payment'];
            }

            if ($order_data['store_id']) {
                $order_data['store_url'] = $this->config->get('config_url');
            } else {
                $order_data['store_url'] = HTTP_SERVER;
            }

            $order_data['payment_firstname'] = $payment_address['firstname'];
            $order_data['payment_lastname'] = $payment_address['lastname'];
            $order_data['payment_company'] = $payment_address['company'];
            $order_data['payment_company_id'] = $payment_address['company_id'];
            $order_data['payment_tax_id'] = $payment_address['tax_id'];
            $order_data['payment_address_1'] = $payment_address['address_1'];
            $order_data['payment_address_2'] = $payment_address['address_2'];
            $order_data['payment_city'] = $payment_address['city'];
            $order_data['payment_postcode'] = $payment_address['postcode'];
            $order_data['payment_zone'] = $payment_address['zone'];
            $order_data['payment_zone_id'] = $payment_address['zone_id'];
            $order_data['payment_country'] = $payment_address['country'];
            $order_data['payment_country_id'] = $payment_address['country_id'];
            $order_data['payment_address_format'] = $payment_address['address_format'];

            if (isset($this->session->data['payment_method']['title'])) {
                $order_data['payment_method'] = $this->session->data['payment_method']['title'];
            } else {
                $order_data['payment_method'] = '';
            }

            if (isset($this->session->data['payment_method']['code'])) {
                $order_data['payment_code'] = $this->session->data['payment_method']['code'];
            } else {
                $order_data['payment_code'] = '';
            }

            if ($this->customer->isLogged()) {
                $shipping_address = $this->session->data['guest']['shipping'];
            } elseif (isset($this->session->data['guest'])) {
                $shipping_address = $this->session->data['guest']['shipping'];
            }

            $order_data['shipping_firstname'] = $shipping_address['firstname'];
            $order_data['shipping_lastname'] = $shipping_address['lastname'];
            $order_data['shipping_company'] = $shipping_address['company'];
            $order_data['shipping_address_1'] = $shipping_address['address_1'];
            $order_data['shipping_address_2'] = $shipping_address['address_2'];
            $order_data['shipping_city'] = $shipping_address['city'];
            $order_data['shipping_postcode'] = $shipping_address['postcode'];
            $order_data['shipping_zone'] = $shipping_address['zone'];
            $order_data['shipping_zone_id'] = $shipping_address['zone_id'];
            $order_data['shipping_country'] = $shipping_address['country'];
            $order_data['shipping_country_id'] = $shipping_address['country_id'];
            $order_data['shipping_address_format'] = $shipping_address['address_format'];

            if (isset($this->session->data['shipping_method']['title'])) {
                $order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
            } else {
                $order_data['shipping_method'] = '';
            }

            if (isset($this->session->data['shipping_method']['code'])) {
                $order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
            } else {
                $order_data['shipping_code'] = '';
            }

            if (isset($this->request->post['delivery_method']['type_delivery']) && !empty($this->request->post['delivery_method']['type_delivery'])) {
                $order_data['shipping_code'] = $order_data['shipping_code'] . '.' . $this->request->post['delivery_method']['type_delivery'];
            }

            $product_data = array();

            $this->load->model('catalog/product');
            $this->load->model('catalog/category');

            $product_quantity_difference = [];

            foreach ($this->cart->getProducts() as $key => $product) {

                $product_info = $this->model_catalog_product->getProduct($product['product_id'], 'all');

                if ($product_info) {
                    if (!is_null($product_info['quantity'])) {
                        $product_balance = (int)$product_info['quantity'] - (int)$product['quantity'];
                        if ((int)$product_balance < 0) {
                            $product_quantity_difference[$key]['available'] = (int)$product_info['quantity'];
                            $product_quantity_difference[$key]['name'] = $product['model'];
                            $product_quantity_difference[$key]['id'] = $product['product_id'];
                        }
                    }
                }

                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value_id' => $option['product_option_value_id'],
                        'option_id' => $option['option_id'],
                        'option_value_id' => $option['option_value_id'],
                        'name' => $option['name'],
                        'value' => $option['value'],
                        'type' => $option['type']
                    );
                }

                if ($product['currency_code'] !== CURRENCY_CODE) {
                    $product['price'] = $this->currency->convert($product['price'], $product['currency_code'], CURRENCY_CODE);
                    $product['total'] = $this->currency->convert($product['total'], $product['currency_code'], CURRENCY_CODE);
                    $product['cost_price'] = $this->currency->convert($product['cost_price'], $product['currency_code'], CURRENCY_CODE);
                }

                $custom_data = [];
                $dimension = [];
                $in_box = 1;
                $size = false;
                $is_bag = 0;

                $product_data[] = array(
                    'product_id' => $product['product_id'],
                    'manufacturer_id' => $product['manufacturer_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'download' => $product['download'],
                    'quantity' => $product['quantity'],
                    'subtract' => $product['subtract'],
                    'price' => $product['price'],
                    'sizes' => $sizes,
                    'cost_price' => $product['cost_price'],
                    'total' => $product['total'],
                    'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward' => $product['reward'],
                    'custom_data' => $custom_data,
                    //'is_bag' => $is_bag,
                    'in_box' => isset($custom_data['in_box']) ? $custom_data['in_box'] : 1,
                    'stock_status_id' => $product['stock_status_id']
                );
            }

            // Gift Voucher
            $voucher_data = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'code' => substr(md5(mt_rand()), 0, 10),
                        'to_name' => $voucher['to_name'],
                        'to_email' => $voucher['to_email'],
                        'from_name' => $voucher['from_name'],
                        'from_email' => $voucher['from_email'],
                        'voucher_theme_id' => $voucher['voucher_theme_id'],
                        'message' => $voucher['message'],
                        'amount' => $voucher['amount']
                    );
                }
            }

            $order_data['products'] = $product_data;
            $order_data['vouchers'] = $voucher_data;
            $order_data['totals'] = $totals;
            $order_data['comment'] = $this->session->data['comment'];
            $order_data['total'] = $total;

            if (isset($this->request->cookie['tracking'])) {
                $this->load->model('affiliate/affiliate');

                $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
                $subtotal = $this->cart->getSubTotal();

                if ($affiliate_info) {
                    $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
                    $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
                } else {
                    $order_data['affiliate_id'] = 0;
                    $order_data['commission'] = 0;
                }

                $order_data['tracking'] = $this->request->cookie['tracking'];
                // Marketing
                $this->load->model('checkout/marketing');

                $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

                if ($marketing_info) {
                    $order_data['marketing_id'] = $marketing_info['marketing_id'];
                } else {
                    $order_data['marketing_id'] = 0;
                }
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission'] = 0;
                $order_data['marketing_id'] = 0;
                $order_data['tracking'] = '';
            }

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
            $order_data['currency_code'] = $this->session->data['currency'];
            // $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
            $order_data['currency_value'] = 1;
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $order_data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $order_data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $order_data['accept_language'] = '';
            }


//      echo '<pre>$this->session->data';
//      print_r($order_data);
//      echo '</pre>';
//      die();
            //if (empty($product_quantity_difference)) {

                $this->load->model('checkout/order');

                $total_products = 0;

                foreach ($this->cart->getProducts() as $product) {
                    //$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency'])
                    $currencyBot = $product['currency_id'];
                    $in_box = (isset($product['in_box']) ? (int)$product['in_box'] : 0);
                    $total_products = $total_products + $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * (int)$product['quantity'], $currencyBot);

                }
                $order_data['total'] = $total_products;

                $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

                $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('config_order_status_id'));

                $data['order_id'] = $this->session->data['order_id'];
                $data['column_name'] = $this->language->get('column_name');
                $data['column_model'] = $this->language->get('column_model');
                $data['column_quantity'] = $this->language->get('column_quantity');
                $data['column_price'] = $this->language->get('column_price');
                $data['column_total'] = $this->language->get('column_total');

                $data['text_recurring_item'] = $this->language->get('text_recurring_item');
                $data['text_payment_profile'] = $this->language->get('text_payment_profile');

                $data['products'] = array();

                // total box in order
                $number_boxes = 0;
                foreach ($this->cart->getProducts() as $product) {

                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                        );
                    }

                    $profile_description = '';

                    if ($product['recurring']) {
                        $frequencies = array(
                            'day' => $this->language->get('text_day'),
                            'week' => $this->language->get('text_week'),
                            'semi_month' => $this->language->get('text_semi_month'),
                            'month' => $this->language->get('text_month'),
                            'year' => $this->language->get('text_year'),
                        );

                        if ($product['recurring_trial']) {
                            $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                            $profile_description = sprintf($this->language->get('text_trial_description'), $recurring_price, $product['recurring_trial_cycle'], $frequencies[$product['recurring_trial_frequency']], $product['recurring_trial_duration']) . ' ';
                        }

                        $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'), $this->session->data['currency']));

                        if ($product['recurring_duration']) {
                            $profile_description .= sprintf($this->language->get('text_payment_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                        } else {
                            $profile_description .= sprintf($this->language->get('text_payment_until_canceled_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                        }
                    }

                    $number_boxes += (int)$product['quantity'];
                    $data['products'][] = array(
                        'cart_id' => $product['cart_id'],
                        'product_id' => $product['product_id'],
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'sizes' => $product['sizes'],
                        'subtract' => $product['subtract'],
                        'price' => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
                        'total' => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']),
                        'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                        'recurring' => $product['recurring']
                        //'profile_name'        => $product['profile_name'],
                        //'profile_description' => $profile_description,
                    );


                    //foreach ($products as $product) {
                    $product_total = 0;
                    $categories = [];
                    $product_data = $this->model_catalog_product->getProduct($product['product_id']);
                    $categories_array = $this->model_catalog_product->getCategories($product['product_id']);
                    foreach ($categories_array as $cat) {

                        $category_info = $this->model_catalog_category->getCategory($cat['category_id']);

                        if ($category_info) {
                            $categories[] = $category_info['name'];

                        }
                    }


                    $products_for_ec[] = [
                        'id' => $product['product_id'],
                        'name' => $product['name'],
                        'category' => implode('/', $categories),
                        'brand' => $product_data['manufacturer'],
                        'price' => $product['price'],
                        'quantity' => $product['quantity']
                    ];

                }

                // Gift Voucher
                $data['vouchers'] = array();

                if (!empty($this->session->data['vouchers'])) {
                    foreach ($this->session->data['vouchers'] as $voucher) {
                        $data['vouchers'][] = array(
                            'description' => $voucher['description'],
                            'amount' => $this->currency->format($voucher['amount'], $this->session->data['currency'])
                        );
                    }
                }

                $data['totals'] = array();


                $data['payment'] = $this->load->controller('payment/' . $this->session->data['payment_method']['code']);


                $order_id = $this->session->data['order_id'];
                $revenue = $total['value'];


                $this->response->redirect($this->url->link('checkout/success'));

//                $this->load->controller('checkout/success', $data)
//                die(
//                json_encode(array(
//                    'order_id' => $order_id,
//                    'revenue' => $revenue,
//                    'products' => $products_for_ec,
//                    'success' => $this->load->controller('checkout/success', $data),
//                    'afterClose' => $this->url->link('common/home', '', true)
//                ))
//                );
            //} else {
                $this->response->redirect($this->url->link('checkout/success'));
//
//                $message = $this->language->get('text_quantity_available') . '  <br>';
//                $ids = [];
//                foreach ($product_quantity_difference as $key => $product) {
//                    $ids[] = $product['id'];
//                    $message .= $product['name'] . " " . $product['available'] . ' ' . $this->language->get('text_quantity_box') . '<br>';
//                }
//                $message .= $this->language->get('text_quantity_change');
//                die(
//                json_encode(
//                    [
//                        'success' => false,
//                        'ids' => $ids,
//                        'message' => $message
//                    ],
//                    JSON_UNESCAPED_UNICODE
//                )
//                );
           // }
        }
    }
}
