<?php

// Heading 
$_['zemez_heading_title']  = 'Блог';

$_['text_date_format']     = 'd.m.y';

$_['text_popular_all']     = 'Популярные новости';
$_['text_latest_all']      = 'Последние новости';
$_['text_button_continue'] = 'Перейти к новости';
$_['text_comments']        = '';
$_['text_comment']         = '';
$_['text_on']         = ' ';


$_['text_no_result']       = 'Нет результатов!';

?>