<?php

class ControllerExtensionShippingDeliveryAuto extends Controller {
    public function index() {
        $this->load->language('extension/shipping/delivery_auto');

        $data['text_message']             = $this->language->get('text_message');
        $data['button_continue']          = $this->language->get('button_continue');
        $data['text_district']            = $this->language->get('text_district');
        $data['text_select_district']     = $this->language->get('text_select_district');
        $data['text_city']                = $this->language->get('text_city');
        $data['text_delivery_type']       = $this->language->get('text_delivery_type');
        $data['text_delivery_department'] = $this->language->get('text_delivery_department');
        $data['text_delivery_address']    = $this->language->get('text_delivery_address');
        $data['text_office']              = $this->language->get('text_office');
        $data['text_address']             = $this->language->get('text_address');
        $data['text_order_comment']       = $this->language->get('text_order_comment');

        $data['continue'] = $this->url->link('common/home');

        $this->load->model('extension/shipping/delivery_auto');

        $data['countries'] = $this->model_extension_shipping_delivery_auto->getCountries();

        $data['shipping_cost'] = $this->config->get('delivery_auto_delivery_price');

        $data['free_total_coast'] = $this->config->get('delivery_auto_min_total_for_free_delivery');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
          $this->response->setOutput($this->load->view('extension/shipping/delivery_auto', $data));
        }else{
          return $this->load->view('extension/shipping/delivery_auto', $data);
        }
    }

    public function state() {
        $this->load->model('extension/shipping/delivery_auto');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            $cities = $this->model_extension_shipping_delivery_auto->getZonesByCountryId($this->request->post['country_id']);

            $response = sprintf('<option value="%s" >%s</option>', '-1', 'Выберите город...');

            foreach($cities as $city) {
                $response .= sprintf('<option value="%s" >%s</option>', $city['zone_id'], $city['name']);
            }

            $this->response->setOutput($response);
        }
    }

    public function city() {
        $this->load->model('extension/shipping/delivery_auto');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            $offices = $this->model_extension_shipping_delivery_auto->getCitiesByZoneId($this->request->post['zone_id']);

            $response = sprintf('<option value="%s" >%s</option>', '-1', 'Выберите отделение...');

            foreach($offices as $office) {
                $response .= sprintf('<option value="%s" >%s</option>', $office['name'], $office['name']);
            }

            $this->response->setOutput($response);
        }
    }

}
