<?php
/**
 * API Meest Express
 *
 * @author    Maxim Miroshnichenko <max@webproduction.ua>
 * @copyright WebProduction
 * @package   MeestExpress
 */
class sat_API {

    public function __construct($key, $language = 'ru') {
        $this->_key = $key;

    }


    public function getCityByRef($ref)
    {
        $result = $this->_requestQuery(
            'getTowns',
            ['ref'=>$ref]
        );
        return $result;
    }
    public function getCities() {

      $result = $this->_requestQuery(
          'getTowns'
      );
//
//      if (!isset($result->result_table->items[0])) {
//          throw new MeestExpress_Exception('No result for '.$regionUID);
//      }
//
//      return $result->result_table->items;
    }


    public function getWarehoues()
    {
        $result = $this->_requestQuery(
            'getRsp'
        );
        //https://api.sat.ua/study/hs/api/v1.0/main/{format}/getRsp[?language][&searchString][&ref]
        return $result;
    }

    public function getWarehouesByCity($cityRef) {

        $result = $this->_requestQuery(
            'City',
            "Regionuuid='$regionUID' AND IsBranchInCity='1'",
            ''
        );

        if (!isset($result->result_table->items[0])) {
            throw new MeestExpress_Exception('No result for '.$regionUID);
        }

        return $result->result_table->items;
    }

    private function _requestQuery($function,$params=[]){//}, $where, $order, $filter=null) {
        // формируем подпись

//
        // отправляем запрос
        //https://api.sat.ua/study/hs/api/v1.0/main/
        return $this->_request('https://api.sat.ua/study/hs/api/v1.0/main/json/'.$function,$params);

        //https://api.sat.ua/study/hs/api/v1.0/main/{format}/getTowns[?searchString][&rsp]

        //http://urm.sat.ua/openws/hs/api/v1.0/ (http://urm.sat.ua/openws/hs/api/v1.0/)

    }

    private function _request($url,$params=[]) {
        //echo $url;
        $ch = curl_init();
        if (count($params)>0) {

            $pars=[];
            foreach ($params as $key=>$value)
            {
                $pars[]=$key.'='.$value;
            }
            $url .= '?'.implode('&', $pars);
        }
        //echo $url;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/sjon'));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        ///curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        //curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response=json_decode($response,true);//->data);//,true);


        if ($response['success'])
        return $response['data'];
        else return [];
//
//        if (!$response) {
//            throw new MeestExpress_Exception('Empty response');
//        }
//
//        $xml = simplexml_load_string($response);
//
//        $resultCode = trim($xml->errors->code.'');
//        $resultMessage = trim($xml->errors->name.'');
//
//        if ($resultCode == '000') {
//            return $xml;
//        }
//
//        throw new MeestExpress_Exception($resultMessage, $resultCode);
    }

    private function _xmlEscape($string) {
        return htmlspecialchars($string);
    }

    private $_key;


    private $_language;

}