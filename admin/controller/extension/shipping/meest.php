<?php

class ControllerExtensionShippingMeest extends Controller {

    private $error = array();

    public function uninstall() {
        $this->db->query("DROP TABLE `" . DB_PREFIX . "meest_branch`, `" . DB_PREFIX . "meest_city`, `" . DB_PREFIX . "meest_region`;");
    }

    public function install() {
        $file = DIR_APPLICATION . 'controller/extension/shipping/meest.sql';
        if (!file_exists($file)) {
            exit('Could not load sql file: ' . $file);
        }
        $lines = file($file);
        if ($lines) {
            $sql = '';
            foreach($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    $sql .= $line;

                    if (preg_match('/;\s*$/', $line)) {
                        $sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("CREATE TABLE IF NOT EXISTS `oc_", "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . DB_PREFIX, $sql);

                        $this->db->query($sql);

                        $sql = '';
                    }
                }
            }
        }
    }

    public function index() {
        $this->load->language('extension/shipping/meest');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('shipping_meest', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['entry_delivery_nal_fixed'] = $this->language->get('entry_delivery_nal_fixed');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');
        $data['text_none'] = $this->language->get('text_none');

        $data['entry_tax'] = $this->language->get('entry_tax');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['entry_delivery_order'] = $this->language->get('entry_delivery_order');
        $data['entry_delivery_price'] = $this->language->get('entry_delivery_price');
        $data['entry_delivery_insurance'] = $this->language->get('entry_delivery_insurance');
        $data['entry_delivery_nal'] = $this->language->get('entry_delivery_nal');
        $data['entry_min_total_for_free_delivery'] = $this->language->get('entry_min_total_for_free_delivery');
        $data['entry_api_key'] = $this->language->get('entry_api_key');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_update'] = $this->language->get('button_update');
        $data['button_check_update'] = $this->language->get('button_check_update');

        $data['tab_general'] = $this->language->get('tab_general');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_shipping'),
            'href'      => $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/shipping/meest', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('extension/shipping/meest', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['meest_check'] = str_replace("&amp;","&", $this->url->link('extension/shipping/meest/checkupdate', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        $data['update'] = str_replace("&amp;","&", HTTP_SERVER . 'meest_update.php');

        $data['cancel'] = $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL');

        if (isset($this->request->post['shipping_meest_login'])) {
            $data['shipping_meest_login'] = trim($this->request->post['shipping_meest_login']);
        } else {
            $data['shipping_meest_login'] = $this->config->get('shipping_meest_login');
        }

        if (isset($this->request->post['shipping_meest_password'])) {
          $data['shipping_meest_password'] = trim($this->request->post['shipping_meest_password']);
        } else {
          $data['shipping_meest_password'] = $this->config->get('shipping_meest_password');
        }

        if (isset($this->request->post['shipping_meest_uid'])) {
          $data['shipping_meest_uid'] = trim($this->request->post['shipping_meest_uid']);
        } else {
          $data['shipping_meest_uid'] = $this->config->get('shipping_meest_uid');
        }

        if (isset($this->request->post['shipping_meest_geo_zone_id'])) {
            $data['shipping_meest_geo_zone_id'] = $this->request->post['shipping_meest_geo_zone_id'];
        } else {
            $data['shipping_meest_geo_zone_id'] = $this->config->get('shipping_meest_geo_zone_id');
        }

        if (isset($this->request->post['shipping_meest_status'])) {
            $data['shipping_meest_status'] = $this->request->post['shipping_meest_status'];
        } else {
            $data['shipping_meest_status'] = $this->config->get('shipping_meest_status');
        }

        if (isset($this->request->post['shipping_meest_sort_order'])) {
            $data['shipping_meest_sort_order'] = $this->request->post['shipping_meest_sort_order'];
        } else {
            $data['shipping_meest_sort_order'] = $this->config->get('shipping_meest_sort_order');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/shipping/meest', $data));
    }

    public function checkupdate() {
        $status_file = DIR_APPLICATION . "meest_updating.txt";
        if (file_exists($status_file) && ((int)file_get_contents($status_file) + 5*60) > time()) { // 5 minutes
            $this->response->setOutput("RUNNING");
        }else{
            if (file_exists($status_file)){
                unlink($status_file);
            }
            $this->response->setOutput("STOPPED");
        }
    }

    public function updating() {
      $status_file = DIR_APPLICATION . "meest_updating.txt";
      exec("NULL> " . $status_file);
      exec("chmod 777 " . $status_file);
      file_put_contents($status_file, time());

      ini_set('max_execution_time', 300); //300 seconds = 5 minutes

      $this->load->language('extension/shipping/meest');

      $json = [];

      include(DIR_SYSTEM.'delivery/meest-express/include.php');

      $login = trim($this->config->get('shipping_meest_login'));
      $password = trim($this->config->get('shipping_meest_password'));
      $clientUID = trim($this->config->get('shipping_meest_uid'));

      $api = new MeestExpress_API($login, $password, $clientUID);

      $countryUID = $api->getCountryUID('Украина');

      $this->load->model('extension/shipping/meest');

      $regions = $api->getRegion($countryUID);
      foreach ($regions as $region) {
        $region_info = $this->model_extension_shipping_meest->getRegion($region->uuid);
        if (!empty($region_info)){
          $this->model_extension_shipping_meest->editRegion(array_replace_recursive($region_info, (array)$region));
        }else{
          $this->model_extension_shipping_meest->addRegion((array)$region);
        }
        $cities = $api->getCity($region->uuid);
        foreach ($cities as $city) {
          $city_info = $this->model_extension_shipping_meest->getCity($city->uuid);
          if (!empty($city_info)){
            $this->model_extension_shipping_meest->editCity(array_replace_recursive($city_info, (array)$city));
          }else{
            $this->model_extension_shipping_meest->addCity((array)$city);
          }
          $branches = $api->getBranch($city->uuid);
          foreach ($branches as $branch) {
            $branch_info = $this->model_extension_shipping_meest->getBranch($branch->UUID);
            if (!empty($branch_info)){
              $this->model_extension_shipping_meest->editBranch(array_replace_recursive($branch_info, (array)$branch));
            }else{
              $this->model_extension_shipping_meest->addBranch((array)$branch);
            }
          }
        }
      }

      $json['success'] = $this->language->get('text_update_success');

      unlink($status_file);

      /* EK: Retrieve session ID */
      global $session_id;
      session_id($session_id);
      $this->response->setOutput(json_encode($json));

    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'extension/shipping/meest')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>
