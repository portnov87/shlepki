<?php

class ControllerExtensionShippingSat extends Controller
{

    private $error = array();

    public function uninstall()
    {
        $this->db->query("DROP TABLE `" . DB_PREFIX . "sat_cities`, `" . DB_PREFIX . "sat_warehouse`;");
    }

    public function install()
    {
        $file = DIR_APPLICATION . 'controller/extension/shipping/sat.sql';
        if (!file_exists($file)) {
            exit('Could not load sql file: ' . $file);
        }
        $lines = file($file);
        if ($lines) {
            $sql = '';
            foreach ($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    $sql .= $line;

                    if (preg_match('/;\s*$/', $line)) {
                        $sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("CREATE TABLE IF NOT EXISTS `oc_", "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . DB_PREFIX, $sql);

                        $this->db->query($sql);

                        $sql = '';
                    }
                }
            }
        }
    }

    public function index()
    {
        $this->load->language('extension/shipping/sat');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('shipping_sat', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['entry_delivery_nal_fixed'] = $this->language->get('entry_delivery_nal_fixed');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');
        $data['text_none'] = $this->language->get('text_none');

        $data['entry_tax'] = $this->language->get('entry_tax');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['entry_delivery_order'] = $this->language->get('entry_delivery_order');
        $data['entry_delivery_price'] = $this->language->get('entry_delivery_price');
        $data['entry_delivery_insurance'] = $this->language->get('entry_delivery_insurance');
        $data['entry_delivery_nal'] = $this->language->get('entry_delivery_nal');
        $data['entry_min_total_for_free_delivery'] = $this->language->get('entry_min_total_for_free_delivery');
        $data['entry_api_key'] = $this->language->get('entry_api_key');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_update'] = $this->language->get('button_update');
        $data['button_check_update'] = $this->language->get('button_check_update');

        $data['tab_general'] = $this->language->get('tab_general');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_shipping'),
            'href' => $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/shipping/novaposhta', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('extension/shipping/sat', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['np_check'] = str_replace("&amp;", "&", $this->url->link('extension/shipping/sat/checkupdate', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        $data['update'] = str_replace("&amp;", "&", HTTP_SERVER . 'np_update.php');

        $data['cancel'] = $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL');

        if (isset($this->request->post['shipping_sat_api_key'])) {
            $data['shipping_sat_api_key'] = $this->request->post['shipping_sat_api_key'];
        } else {
            $data['shipping_sat_api_key'] = $this->config->get('shipping_sat_api_key');
        }
        /*
                if (isset($this->request->post['shipping_novaposhta_min_total_for_free_delivery'])) {
                    $data['shipping_novaposhta_min_total_for_free_delivery'] = $this->request->post['shipping_novaposhta_min_total_for_free_delivery'];
                } else {
                    $data['shipping_novaposhta_min_total_for_free_delivery'] = $this->config->get('shipping_novaposhta_min_total_for_free_delivery');
                }

                if (isset($this->request->post['shipping_novaposhta_delivery_order'])) {
                    $data['shipping_novaposhta_delivery_order'] = $this->request->post['shipping_novaposhta_delivery_order'];
                } else {
                    $data['shipping_novaposhta_delivery_order'] = $this->config->get('shipping_novaposhta_delivery_order');
                }

                if (isset($this->request->post['shipping_novaposhta_delivery_price'])) {
                    $data['shipping_novaposhta_delivery_price'] = $this->request->post['shipping_novaposhta_delivery_price'];
                } else {
                    $data['shipping_novaposhta_delivery_price'] = $this->config->get('shipping_novaposhta_delivery_price');
                }

                if (isset($this->request->post['shipping_novaposhta_delivery_insurance'])) {
                    $data['shipping_novaposhta_delivery_insurance'] = $this->request->post['shipping_novaposhta_delivery_insurance'];
                } else {
                    $data['shipping_novaposhta_delivery_insurance'] = $this->config->get('shipping_novaposhta_delivery_insurance');
                }

                if (isset($this->request->post['shipping_novaposhta_delivery_nal'])) {
                    $data['shipping_novaposhta_delivery_nal'] = $this->request->post['shipping_novaposhta_delivery_nal'];
                } else {
                    $data['shipping_novaposhta_delivery_nal'] = $this->config->get('shipping_novaposhta_delivery_nal');
                }

                if (isset($this->request->post['shipping_novaposhta_delivery_nal_fixed'])) {
                    $data['shipping_novaposhta_delivery_nal_fixed'] = $this->request->post['shipping_novaposhta_delivery_nal_fixed'];
                } else {
                    $data['shipping_novaposhta_delivery_nal_fixed'] = $this->config->get('shipping_novaposhta_delivery_nal_fixed');
                }

                if (isset($this->request->post['shipping_novaposhta_geo_zone_id'])) {
                    $data['shipping_novaposhta_geo_zone_id'] = $this->request->post['shipping_novaposhta_geo_zone_id'];
                } else {
                    $data['shipping_novaposhta_geo_zone_id'] = $this->config->get('shipping_novaposhta_geo_zone_id');
                }
        */

        if (isset($this->request->post['shipping_sat_status'])) {
            $data['shipping_sat_status'] = $this->request->post['shipping_sat_status'];
        } else {
            $data['shipping_sat_status'] = $this->config->get('shipping_sat_status');
        }
//
        if (isset($this->request->post['shipping_sat_sort_order'])) {
            $data['shipping_sat_sort_order'] = $this->request->post['shipping_sat_sort_order'];
        } else {
            $data['shipping_sat_sort_order'] = $this->config->get('shipping_sat_sort_order');
        }

        //  $this->load->model('localisation/geo_zone');

        // $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/shipping/sat', $data));
    }

    public function checkupdate()
    {
        $status_file = DIR_APPLICATION . "sat_updating.txt";
        if (file_exists($status_file) && ((int)file_get_contents($status_file) + 5 * 60) > time()) { // 5 minutes
            $this->response->setOutput("RUNNING");
        } else {
            if (file_exists($status_file)) {
                unlink($status_file);
            }
            $this->response->setOutput("STOPPED");
        }
    }

    public function updating()
    {

        $status_file = DIR_APPLICATION . "sat_updating.txt";
        exec("NULL> " . $status_file);
        exec("chmod 777 " . $status_file);
        file_put_contents($status_file, time());

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes

        $this->load->language('extension/shipping/sat');


        $json = array();

        $this->load->model('sat/city');
        $this->load->model('sat/warehouse');
        $this->load->model('sat/region');

        include(DIR_SYSTEM . 'delivery/sat/Sat_API.class.php');

        $api_key = $this->config->get('shipping_sat_api_key');
        $api = new Sat_API($api_key);

        $warehouses = $api->getWarehoues();

        echo '<pre>$warehouses';
        print_r($warehouses);
        echo '</pre>';

        if (count($warehouses) > 0) {
            $this->model_sat_region->truncateRegions();

            $this->model_sat_city->truncateCities();
            $this->model_sat_warehouse->truncateWarehouse();

            foreach ($warehouses as $warehouse) {

                $cityRef = $warehouse['cityRef'];
                $city = $this->model_sat_city->getCityByRef($cityRef);//(['name' => $region]);
                $warehouseRef = $warehouse['ref'];
                if (!$city) {
                    $city = $api->getCityByRef($cityRef);


                    foreach ($city as $c) {



                        $region = $c['region'];
                        $cityName = $c['description'];


                        $regionId = $this->model_sat_region->getRegionByName($region);//(['name' => $region]);
                        if (!$regionId)
                            $regionId = $this->model_sat_region->addRegion(['name' => $region]);


                        $cityId = $this->model_sat_city->addCity([
                            'name' => $cityName,
                            'guid' => $cityRef,
                            'region_id' => $regionId,
                            'region' => $region,
                            'status' => 1
                        ]);



                        $warehouseId = $this->model_sat_warehouse->addWarehouse([
                            'name' => $warehouse['description'],
                            'address'=>$warehouse['address'],
                            'cityRef' => $cityRef,
                            'city_id' => $cityId,
                            'guid' => $warehouseRef,
                            'number' => $warehouse['number'],
                            'phone' => $warehouse['phone'],
                            'region_id' => $regionId,
                            'status' => 1,
                        ]);

//                        $warehouseId = $this->model_sat_warehouse->addWarehouse([
//                            'name' => $region,
//                            'cityRef' => $cityRef,
//                            'city_id' => $cityId,
//                            'guid' => $warehouseRef,
//                            'number' => $warehouse['number'],
//                            'phone' => $warehouse['phone'],
//                            'region_id' => $regionId,
//                            'status' => 1,
//                        ]);
                    }
                }else{


                    $region = $city['region'];
                    $cityName = $city['name'];

                    $regionId = $this->model_sat_region->getRegionByName($region);
                    if (!$regionId)
                        $regionId = $this->model_sat_region->addRegion(['name' => $region]);


                    /*$cityId = $this->model_sat_city->addCity([
                        'name' => $cityName,
                        'guid' => $cityRef,
                        'region_id' => $regionId,
                        'region' => $region,
                        'status' => 1
                    ]);
                    */
                    $cityId=$city['id'];


                    $warehouseId = $this->model_sat_warehouse->addWarehouse([
                        'name' => $warehouse['description'],
                        'address'=>$warehouse['address'],
                        'cityRef' => $cityRef,
                        'city_id' => $cityId,
                        'guid' => $warehouseRef,
                        'number' => $warehouse['number'],
                        'phone' => $warehouse['phone'],
                        'region_id' => $regionId,
                        'status' => 1,
                    ]);

                }
            }
        }
        die();
        /*

        include(DIR_SYSTEM . 'delivery/NovaPoshtaApi2Areas.php');
        include(DIR_SYSTEM . 'delivery/NovaPoshtaApi2.php');

        $api_key = $this->config->get('shipping_sat_api_key');

        // EK: clear cache before
        $np_cache_folder = DIR_SYSTEM . 'cache/';
        if (function_exists('exec')) {
            exec('rm -rf ' . $np_cache_folder . '*');
        }

        if (!$api_key) {
            $json['error'] = $this->language->get('text_api_key_error');
        } else {

            $novaposhta = new \LisDev\Delivery\NovaPoshtaApi2($api_key);

            $areas = $novaposhta->getAreas();
            $cities = $novaposhta->getCities();

            $this->load->model('np/city');
            $this->load->model('np/zone');
            //$this->model_np_city->truncateCities();
            //$this->model_np_zone->truncateZones();

            if ($areas['success'] && $cities['success']) {

                echo 'count area = ' . count($areas['data']) . "\r\n\r\n";
                echo 'count cities = ' . count($cities['data']) . "\r\n\r\n";
                //die();

                foreach ($areas['data'] as $key => $area) {
                    $country_id = $areas_alias[$area['Description']];

                        foreach ($cities['data'] as $city) {
                            if ($city['Area'] == $area['Ref']) {
                                $zone_id = $this->model_np_zone->getZoneByData(['name' => $city['DescriptionRu'], 'country_id' => $country_id]);
                                if (!$zone_id) {
                                    $zone_id = $this->model_np_zone->addZone(array(
                                        'name' => $city['DescriptionRu'],
                                        'country_id' => $country_id,
                                        'code' => '',
                                        'guid' => $city['Ref'],
                                        'status' => 1
                                    ));
                                } else {
                                    $this->model_np_zone->editZone($zone_id,
                                        array(
                                            'name' => $city['DescriptionRu'],
                                            'country_id' => $country_id,
                                            'code' => '',
                                            'guid' => $city['Ref'],
                                            'status' => 1
                                        ));

                                }

                                $warehouses = $novaposhta->getWarehouses($city['Ref']);//$area
                                if ($warehouses['success']) {
                                    $citiesToSave = [];
                                    foreach ($warehouses['data'] as $warehouse) {
                                        if (empty($warehouse['DescriptionRu'])) continue;
                                        $citiesToSave[] = array(
                                            'name' => $warehouse['DescriptionRu'],
                                            'zone_id' => $zone_id,
                                            'code' => '',
                                            'guid' => $warehouse['Ref'],
                                            'status' => 1
                                        );
                                    }

                                    $this->model_np_city->addCities($citiesToSave);


                                }

                            }
                        }

                }

                //$this->model_np_city->addCities($citiesToSave);

                $json['success'] = $this->language->get('text_update_success');

            } else {

                $json['error'] = $this->language->get('text_update_error');

            }
        }
*/
        // EK: clear cache after
        if (function_exists('exec')) {
            exec('rm -rf ' . $np_cache_folder . '*');
        }

        unlink($status_file);

        /* EK: Retrieve session ID */
        global $session_id;
        session_id($session_id);
        $this->response->setOutput(json_encode($json));
    }

    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/shipping/novaposhta')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>
