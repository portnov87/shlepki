<?php
// Text
$_['text_home']          = 'Главная';

				$_['text_compare'] = 'Сравнение (%s)';
				$_['text_phones']  = 'Телефоны:';
				$_['text_open']    = 'Мы открыты:';
				
$_['text_wishlist']      = 'Избранное';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';

