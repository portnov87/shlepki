<?php

class ControllerCheckoutLiteCheckout extends Controller
{
    public function index()
    {

        // Validate cart has products and has stock.
        if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers']))) {
            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->document->addScript('catalog/view/javascript/lite_cart.js');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        // Validate minimum quantity requirments.
        $products = $this->cart->getProducts();

        $data['stock_status_error'] = [];
        $ga_ecommerce = $ga_ecommerce_products = [];
        $position = 1;


        foreach ($products as $product) {
            $product_total = 0;
            $categories = [];
            if ($product['stock_status']) {
                $data['stock_status_error'][] = $product['product_id'];
            }

            /*echo '<pre>$product';
            print_r($product);
            echo '</pre>';*/
            $product_data = $this->model_catalog_product->getProduct($product['product_id']);
            $categories_array = $this->model_catalog_product->getCategories($product['product_id']);
            foreach ($categories_array as $cat) {

                $category_info = $this->model_catalog_category->getCategory($cat['category_id']);

                if ($category_info) {
                    $categories[] = $category_info['name'];

                }
            }


//        $ga_ecommerce = [
//            'list' => 'cart',
//            'link' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
//            'id' => $product['product_id'],
//            'name' => $product['name'],
//            'price'=>$unit_price,
//            'category' => implode('/', $categories),
//            'brand' => isset($manufacturer_info['name']) ? $manufacturer_info['name'] : '',
//            'position' => $position
//        ];
            $ga_ecommerce_products[] = "{
                                        'name': '" . $product['name'] . "',
                      'id': '" . $product['product_id'] . "',                      
                      'price': " . $product['price'] . ",
                      'brand': '" . $product_data['manufacturer'] . "',
                      'category': '" . implode('/', $categories) . "',
                      'quantity': " . $product['quantity'] . "
                   }";

            $position++;


            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $this->response->redirect($this->url->link('checkout/cart'));
            }
        }

        $data['stock_status_error'] = (!empty($data['stock_status_error']) && count($data['stock_status_error']) !== count($products)) ? true : false;

        $this->language->load('checkout/lite_checkout');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart'),
            'separator' => $this->language->get('text_separator')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/lite_checkout', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $data['heading'] = '';//$this->load->view('helpers/heading');

        $data['action_confirm'] = $this->url->link('checkout/evg_confirm', '', 'SSL');

        //$data['breadcrumbs'] = $this->load->view('helpers/breadcrumbs', ['breadcrumbs' => $data['breadcrumbs']]);

//        $data['breadcrumbs'] = array();
//
//        $data['breadcrumbs'][] = array(
//            'text' => $this->language->get('text_home'),
//            'href' => $this->url->link('common/home')
//        );
//
//        $data['breadcrumbs'][] = array(
//            'text' => $this->language->get('text_cart'),
//            'href' => $this->url->link('checkout/cart')
//        );
//
//        $data['breadcrumbs'][] = array(
//            'text' => $this->language->get('heading_title'),
//            'href' => $this->url->link('checkout/lite_checkout', '', true)
//        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_loading'] = $this->language->get('text_loading');
        $data['text_checkout_option'] = $this->language->get('text_checkout_option');
        $data['text_checkout_account'] = $this->language->get('text_checkout_account');
        $data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
        $data['text_checkout_shipping_address'] = $this->language->get('text_checkout_shipping_address');
        $data['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method');
        $data['text_checkout_payment_method'] = $this->language->get('text_checkout_payment_method');
        $data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm');
        $data['text_modify'] = $this->language->get('text_modify');
        $data['text_new_personal'] = $this->language->get('text_new_personal');
        $data['text_new_edit'] = $this->language->get('text_new_edit');
        $data['text_new_name'] = $this->language->get('text_new_name');
        $data['text_new_city'] = $this->language->get('text_new_city');
        $data['text_new_phone'] = $this->language->get('text_new_phone');
        $data['text_new_mail'] = $this->language->get('text_new_mail');
        $data['text_new_access_cabinet'] = $this->language->get('text_new_access_cabinet');
        $data['text_new_required'] = $this->language->get('text_new_required');
        $data['text_new_select_delivery'] = $this->language->get('text_new_select_delivery');
        $data['text_new_next'] = $this->language->get('text_new_next');
        $data['text_new_delivery_method'] = $this->language->get('text_new_delivery_method');
        $data['text_new_pickup'] = $this->language->get('text_new_pickup');
        $data['text_new_courier'] = $this->language->get('text_new_courier');
        $data['text_new_np'] = $this->language->get('text_new_np');
        $data['text_new_address'] = $this->language->get('text_new_address');
        $data['text_new_select'] = $this->language->get('text_new_select');
        $data['text_new_comment'] = $this->language->get('text_new_comment');
        $data['text_new_select_payment'] = $this->language->get('text_new_select_payment');
        $data['text_new_method_payment'] = $this->language->get('text_new_method_payment');
        $data['text_new_cash'] = $this->language->get('text_new_cash');
        $data['text_new_pay_at_office'] = $this->language->get('text_new_pay_at_office');
        $data['text_new_non_cash'] = $this->language->get('text_new_non_cash');
        $data['text_new_credit'] = $this->language->get('text_new_credit');
        $data['text_new_confirm'] = $this->language->get('text_new_confirm');

        $data['error_email'] = $this->language->get('error_email');
        $data['error_phone'] = $this->language->get('error_phone');
        $data['error_enter_phone'] = $this->language->get('error_enter_phone');
        $data['error_text_length'] = $this->language->get('error_text_length');

        $data['logged'] = $this->customer->isLogged();
        $data['shipping_required'] = $this->cart->hasShipping();
        $data['balance'] = false;

        if ($data['logged']) {
            $data['firstname'] = $this->customer->getFirstName();
            $data['lastname'] = $this->customer->getLastName();
            $data['email'] = $this->customer->getEmail();
            $data['telephone'] = $this->customer->getTelephone();



            // city and address for session
            $this->load->model('account/address');
            $_aId = $this->customer->getAddressId();
            if (isset($_aId)) {
                $address_info = $this->model_account_address->getAddress($this->customer->getAddressId());
                $data['city'] = $address_info['city'];
                // address
                $data['address'] = $address_info['address_1'];
            }
        } else {
            $data['firstname'] = (isset($this->session->data['guest']['firstname'])) ? $this->session->data['guest']['firstname'] : '';
            $data['lastname'] = (isset($this->session->data['guest']['lastname'])) ? $this->session->data['guest']['lastname'] : '';
            $data['email'] = (isset($this->session->data['guest']['email'])) ? $this->session->data['guest']['email'] : '';
            $data['telephone'] = (isset($this->session->data['guest']['telephone'])) ? $this->session->data['guest']['telephone'] : '';
            $data['city'] = (isset($this->session->data['guest']['city'])) ? $this->session->data['guest']['city'] : '';
            $data['address'] = (isset($this->session->data['guest']['shipping']['address_1'])) ? $this->session->data['guest']['shipping']['address_1'] : '';
        }

        $this->load->model('setting/setting');
        $_data = $this->model_setting_setting->getSetting('evercart');
        $data['inputs'] = array();
        $inputs = isset($_data['evercart_inputs']) ? $_data['evercart_inputs'] : array();
        $session_inputs = (isset($this->session->data['guest']['inputs'])) ? unserialize($this->session->data['guest']['inputs']) : array();

        foreach ($inputs as $input) {
            $data['inputs'][] = array(
                'name' => $input['name'],
                'description' => $input['description'][(int)$this->config->get('config_language_id')],
                'value' => isset($session_inputs[$input['name']]) ? $session_inputs[$input['name']] : ''
            );
        }


        if (isset($this->request->get['quickconfirm'])) {
            $data['quickconfirm'] = $this->request->get['quickconfirm'];
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['lite_minicart'] = $this->load->controller('checkout/lite_minicart');


        $data['delivery_methods'] = $this->load->controller('checkout/evg_shipping_method');

        $data['payment_methods'] = $this->load->controller('checkout/evg_payment_method');

        $ga_ecommerce = 'gtag("event", "checkout_progress", {
          "items": [
            '.implode(",\r\n", $ga_ecommerce_products).'    
          ],
          "coupon": ""
        });';
        $data['ga_ecommerce'] = $ga_ecommerce;

        $this->response->setOutput($this->load->view('checkout/lite_checkout', $data));
    }

    public function country()
    {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id' => $country_info['country_id'],
                'name' => $country_info['name'],
                'iso_code_2' => $country_info['iso_code_2'],
                'iso_code_3' => $country_info['iso_code_3'],
                'address_format' => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone' => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status' => $country_info['status']
            );
        }

        $this->response->setOutput(json_encode($json));
    }

    public function check_customer()
    {
        $this->load->model('account/customer');

        $email=$this->request->post['personal_data']['email'];
        $phone=$this->request->post['personal_data']['telephone'];

        $customer = $this->model_account_customer->getCustomerByEmailOrPhone($email,$phone);
        //($this->request->post['personal_data']['email']);

        if (count($customer) == 0) {
            $this->response->setOutput(json_encode(array('status' => false)));
        } else {
            $this->response->setOutput(json_encode(array('status' => true)));
        }


    }

}

?>
