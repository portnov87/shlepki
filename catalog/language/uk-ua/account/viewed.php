<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

//version 2.1.0.x
//Changed by fradee (Oleksandr Fradynsky) with love to Ukraine and OpenCart at 22 October.2015
//fradee@gmail.com

// Heading
$_['heading_title'] = 'Переглянуті товари';

$_['text_empty']    = 'Ви ще не переглядали товари';
