<?php
class ModelExtensionShippingMeest extends Model {
  public function addRegion($data) {
    $this->db->query(
      "INSERT INTO 
        ".DB_PREFIX."meest_region 
      SET 
        uuid='".$data['uuid']."',
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
    ");
  }

  public function editRegion($data) {
    $this->db->query(
      "UPDATE 
        ".DB_PREFIX."meest_region 
      SET 
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
      WHERE
        uuid='".$data['uuid']."'
    ");
  }

  public function getRegion($uuid) {
    $q = $this->db->query(
      "SELECT 
        * 
      FROM 
        ".DB_PREFIX."meest_region 
      WHERE
        uuid='".$uuid."'
      LIMIT 1
    ");

    return $q->row;
  }

  public function addCity($data) {
    $this->db->query(
      "INSERT INTO 
        ".DB_PREFIX."meest_city 
      SET 
        uuid='".$data['uuid']."',
        Regionuuid='".$data['Regionuuid']."',
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
    ");
  }

  public function editCity($data) {
    $this->db->query(
      "UPDATE 
        ".DB_PREFIX."meest_city 
      SET 
        Regionuuid='".$data['Regionuuid']."',
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
      WHERE
        uuid='".$data['uuid']."'
    ");
  }

  public function getCity($uuid) {
    $q = $this->db->query(
      "SELECT 
        * 
      FROM 
        ".DB_PREFIX."meest_city 
      WHERE
        uuid='".$uuid."'
      LIMIT 1
    ");

    return $q->row;
  }

  public function addBranch($data) {
    $this->db->query(
      "INSERT INTO 
        ".DB_PREFIX."meest_branch 
      SET 
        UUID='".$data['UUID']."',
        RegionUUID='".$data['RegionUUID']."',
        CityUUID='".$data['CityUUID']."',
        BranchCode='".$data['BranchCode']."',
        Branchtype='".$data['Branchtype']."',
        Limitweight='".$data['Limitweight']."',
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
    ");
  }

  public function editBranch($data) {
    $this->db->query(
      "UPDATE 
        ".DB_PREFIX."meest_branch 
      SET 
        RegionUUID='".$data['RegionUUID']."',
        CityUUID='".$data['CityUUID']."',
        BranchCode='".$data['BranchCode']."',
        Branchtype='".$data['Branchtype']."',
        Limitweight='".$data['Limitweight']."',
        DescriptionUA='".$data['DescriptionUA']."',
        DescriptionRU='".$data['DescriptionRU']."'
      WHERE
        UUID='".$data['UUID']."'
    ");
  }

  public function getBranch($uuid) {
    $q = $this->db->query(
      "SELECT 
        * 
      FROM 
        ".DB_PREFIX."meest_branch 
      WHERE
        UUID='".$uuid."'
      LIMIT 1
    ");

    return $q->row;
  }
}