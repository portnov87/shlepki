<?php
class ControllerExtensionModuleZemezLogo extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/zemez_logo');

		$this->load->model('tool/image');

		$data['home'] = $this->url->link('common/home');

        if ((int)$this->config->get('config_language_id')==2)
            $data['home']=$this->url->link('common/home');
        else
            $data['home']=$this->url->link('common/home').'ua';


		$data['name'] = $this->config->get('config_name');
        $data['phone1'] = $this->config->get('config_telephone');
        $data['phone2'] = $this->config->get('config_telephone2');
        $data['phone3'] = $this->config->get('config_telephone3');
        $data['config_open'] = $this->config->get('config_open');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), $setting['width'], $setting['height']);
		} else {
			$data['logo'] = '';
		}

		return $this->load->view('extension/module/zemez_logo', $data);
	}
}