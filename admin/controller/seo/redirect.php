<?php
class ControllerSeoRedirect extends Controller {
  private $error = array();
  private $stores = [];

  public function __construct($params) {
    parent::__construct($params);
    $this->load->language('seo/redirect');
    $this->load->model('setting/store');
    $this->load->model('design/seo_url');
    $this->stores[0] = $this->config->get('config_name');

    $stores = $this->model_setting_store->getStores();

    foreach ($stores as $store) {
      $this->stores[$store['store_id']] = $store['name'];
    }
  }

  public function index() {
    $this->load->language('seo/redirect');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/redirect');

    $this->getList();
  }

  public function add() {
    $this->load->language('seo/redirect');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/redirect');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      $this->model_seo_redirect->addRedirect($this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/redirect', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function edit() {
    $this->load->language('seo/redirect');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/redirect');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      $this->model_seo_redirect->editRedirect($this->request->get['seo_redirect_id'], $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/redirect', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function delete() {
    $this->load->language('seo/redirect');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/redirect');

    if (isset($this->request->post['selected']) && $this->validateDelete()) {
      foreach ($this->request->post['selected'] as $seo_redirect_id) {
        $this->model_seo_redirect->deleteRedirect($seo_redirect_id);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/redirect', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getList();
  }

  protected function getList() {
    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'seo_redirect_id';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    if (isset($this->request->get['filter_to'])) {
      $data['filter_to'] = $this->request->get['filter_to'];
    }else{
      $data['filter_to'] = '';
    }

    if (isset($this->request->get['filter_keyword'])) {
      $data['filter_keyword'] = $this->request->get['filter_keyword'];
      $get_seo_url = $this->model_design_seo_url->getSeoUrls([
        'filter_keyword' => '%'.$data['filter_keyword'].'%',
        'filter_language_id' => $this->config->get('config_language_id')
      ]);
      $filter_seo_url_id = [];
      foreach ($get_seo_url as $seo_url) {
        $filter_seo_url_id[] = $seo_url['seo_url_id'];
      }
    } else {
      $data['filter_keyword'] = '';
      $filter_seo_url_id = null;
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    if (isset($this->request->get['filter_to'])) {
      $url .= '&filter_to=' . $this->request->get['filter_to'];
    }

    if (isset($this->request->get['filter_keyword'])) {
      $url .= '&filter_keyword=' . $this->request->get['filter_keyword'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('seo/redirect', 'user_token=' . $this->session->data['user_token'] . $url, true)
    );

    $data['add'] = $this->url->link('seo/redirect/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
    $data['delete'] = $this->url->link('seo/redirect/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

    $data['redirects'] = array();

    $filter_data = array(
      'sort'                => $sort,
      'order'               => $order,
      'filter_seo_url_id'   => $filter_seo_url_id,
      'filter_to'           => $data['filter_to'],
      'start'               => ($page - 1) * $this->config->get('config_limit_admin'),
      'limit'               => $this->config->get('config_limit_admin')
    );

    $redirect_total = $this->model_seo_redirect->getTotal();

    $results = $this->model_seo_redirect->getRedirects($filter_data);

    foreach ($results as $result) {
      $seo_url_info = $this->model_design_seo_url->getSeoUrl($result['seo_url_id']);
      $data['redirects'][] = array(
        'seo_redirect_id' => $result['seo_redirect_id'],
        'from'            => isset($seo_url_info['keyword'])?$seo_url_info['keyword']:'-',
        'store'           => $this->stores[$result['store_id']],
        'seo_url'         => isset($seo_url_info['keyword'])?$seo_url_info['keyword']:'-',
        'to'              => $result['to'],
        'edit'            => $this->url->link('seo/redirect/edit', 'user_token='.$this->session->data['user_token'].'&seo_redirect_id='.$result['seo_redirect_id'].$url, true)
      );
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    if (isset($this->request->post['selected'])) {
      $data['selected'] = (array)$this->request->post['selected'];
    } else {
      $data['selected'] = array();
    }

    $url = '';

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_store'] = $this->url->link('seo/redirect', 'user_token='.$this->session->data['user_token'].'&sort=store'.$url, true);
    $data['sort_seo_url_id'] = $this->url->link('seo/redirect', 'user_token='.$this->session->data['user_token'].'&sort=seo_url_id'.$url, true);
    $data['sort_to'] = $this->url->link('seo/redirect', 'user_token='.$this->session->data['user_token'].'&sort=to'.$url, true);

    $data['user_token'] = $this->session->data['user_token'];

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $redirect_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('seo/redirect', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

    $data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($redirect_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($redirect_total - $this->config->get('config_limit_admin'))) ? $redirect_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $redirect_total, ceil($redirect_total / $this->config->get('config_limit_admin')));

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('seo/redirect_list', $data));
  }

  protected function getForm() {
    $data['text_form'] = !isset($this->request->get['seo_redirect_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['seo_url'])) {
      $data['error_seo_url'] = $this->error['seo_url'];
    } else {
      $data['error_seo_url'] = '';
    }

    if (isset($this->error['to'])) {
      $data['error_to'] = $this->error['to'];
    } else {
      $data['error_to'] = '';
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['user_token'] = $this->session->data['user_token'];

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('seo/redirect', 'user_token='.$this->session->data['user_token'].$url, true)
    );

    if (!isset($this->request->get['seo_redirect_id'])) {
      $data['action'] = $this->url->link('seo/redirect/add', 'user_token='.$this->session->data['user_token'].$url, true);
    } else {
      $data['action'] = $this->url->link('seo/redirect/edit', 'user_token='.$this->session->data['user_token'].'&seo_redirect_id='.$this->request->get['seo_redirect_id'].$url, true);
    }

    $data['cancel'] = $this->url->link('seo/redirect', 'user_token='.$this->session->data['user_token'].$url, true);

    if (isset($this->request->get['seo_redirect_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $redirect_info = $this->model_seo_redirect->getRedirect($this->request->get['seo_redirect_id']);
    }else{
      $redirect_info = [];
    }

    if (isset($this->request->post['seo_url'])) {
      $data['seo_url'] = $this->request->post['seo_url'];
    } elseif (!empty($redirect_info)) {
      $seo_url_info = $this->model_design_seo_url->getSeoUrl($redirect_info['seo_url_id']);
      if (!empty($seo_url_info)){
        $data['seo_url'] = $seo_url_info['keyword']." [{$this->stores[$seo_url_info['store_id']]}]";
      }else{
        $data['seo_url'] = '';
      }
    } else {
      $data['seo_url'] = '';
    }

    if (isset($this->request->post['seo_url_id'])) {
      $data['seo_url_id'] = $this->request->post['seo_url_id'];
    } elseif (!empty($redirect_info)) {
      $data['seo_url_id'] = $redirect_info['seo_url_id'];
    } else {
      $data['seo_url_id'] = '';
    }

    if (isset($this->request->post['store_id'])) {
      $data['store_id'] = $this->request->post['store_id'];
    } elseif (!empty($redirect_info)) {
      $data['store_id'] = $redirect_info['store_id'];
    } else {
      $data['store_id'] = '0';
    }

    if (isset($this->request->post['code'])) {
      $data['code'] = $this->request->post['code'];
    } elseif (!empty($redirect_info)) {
      $data['code'] = $redirect_info['code'];
    } else {
      $data['code'] = '301';
    }

    if (isset($this->request->post['to'])) {
      $data['to'] = $this->request->post['to'];
    } elseif (!empty($redirect_info)) {
      $data['to'] = $redirect_info['to'];
    } else {
      $data['to'] = '';
    }

    $this->load->model('localisation/language');

    $data['languages'] = $this->model_localisation_language->getLanguages();

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('seo/redirect_form', $data));
  }

  protected function validateForm() {
    if (!$this->user->hasPermission('modify', 'seo/redirect')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if ((utf8_strlen($this->request->post['seo_url']) < 3) || (utf8_strlen($this->request->post['seo_url']) > 32)) {
      $this->error['seo_url'] = $this->language->get('error_seo_url');
    }

    if ((utf8_strlen($this->request->post['to']) < 3) || (utf8_strlen($this->request->post['to']) > 32)) {
      $this->error['to'] = $this->language->get('error_to');
    }

    return !$this->error;
  }

  protected function validateDelete() {
    if (!$this->user->hasPermission('modify', 'seo/redirect')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }
}
