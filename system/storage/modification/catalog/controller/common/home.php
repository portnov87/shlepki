<?php

class ControllerCommonHome extends Controller
{
    public function index()
    {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');

        $data['top'] = $this->load->controller('common/top');
        $data['bottom'] = $this->load->controller('common/bottom');


        $data['mobile_banner'] = $this->load->controller('common/mobile_banner');
        //$data['desctop_banner'] = $this->load->controller('common/desctop_banner');

        $data['list_manufacturers'] = $this->load->controller('extension/module/zemez_manufacturer');

        $data['desctop_banner'] = $this->load->controller('extension/module/new_collection');


        $data['content_adventure'] = $this->load->controller('common/content_adventure');

        //$data['home_text'] = $this->load->controller('common/home_text');


        $this->load->model('setting/module');

        $module_id = 109;
        $super_module_info = $this->model_setting_module->getModule($module_id);

        if ($super_module_info && $super_module_info['status']) {
            $data['home_text'] = $this->load->controller('extension/module/html', $super_module_info);
        } else {
            $data['home_text'] = '';
        }
        if ((int)$this->config->get('config_language_id')==2)
            $data['meta_h1']='Интернет-магазин обуви оптом';
        else
            $data['meta_h1']='Iнтернет-магазiн взуття оптом';

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/home', $data));
    }
}
