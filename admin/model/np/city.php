<?php
class ModelNpCity extends Model {
	public function addCity($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "np_city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', zone_id = '" . (int)$data['zone_id'] . "', guid = '" . $this->db->escape($data['guid']) . "'");
		//code = '" . $this->db->escape($data['code']) . "',

		$this->cache->delete('city');
	}


    public function getCityByData($data) {


        $zone_id=$data['zone_id'];
        $name=$data['name'];
        $sql="SELECT DISTINCT * FROM " . DB_PREFIX . "np_city WHERE name = '" . $name . "' AND zone_id = '" . $zone_id . "' LIMIT 1";
        $query = $this->db->query($sql);

        if ($query->row)
            return $query->row['city_id'];
        else return false;

    }


	public function addCities($data) {
		if (!empty($data)) {
            $data_new=[];
            foreach ($data as $id=>$city_d) {
                $city_id = $this->getCityByData($city_d);
                if (!$city_id) {
                    $data_new[]=$city_d;
                }else{// uodate
                    $this->editCity($city_id,$city_d);
                }
            }

            if (count($data_new)>0) {
                $sql = "INSERT INTO " . DB_PREFIX . "np_city (status,name,zone_id,guid) VALUES ";// zone_id,
                $sql_where=[];
                foreach ($data_new as $id => $city) {
                    $sql_where[]="('" . (int)$city['status'] . "','" . $this->db->escape($city['name']) . "','" . (int)$city['zone_id'] . "', '".$city['guid']."')";
                    //$sql .= "('" . (int)$city['status'] . "','" . $this->db->escape($city['name']) . "','" . (int)$city['zone_id'] . "', '".$city['guid']."')";//'" . $this->db->escape($city['code']) . "',
                    //if ($id != count($data) - 1) {
                        //$sql .= ",";
                    //}
                }
                if (count($sql_where)>0) {
                    $sql.=implode(',',$sql_where);

                    //echo '$sql ' . $sql . "\r\n\r\n";
                    $this->db->query($sql);
                }
            }
		}
	}

	public function editCity($city_id, $data) {
		//$this->db->query("UPDATE " . DB_PREFIX . "np_city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', zone_id = '" . (int)$data['zone_id'] . "' WHERE zone_id = '" . (int)$zone_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "np_city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', zone_id = '" . (int)$data['zone_id'] . "', guid = '" . $data['guid'] . "' WHERE city_id = '" . (int)$city_id . "'");
        //code = '" . $this->db->escape($data['code']) . "',

		$this->cache->delete('city');
	}

	public function deleteCity($city_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "np_city WHERE city_id = '" . (int)$city_id . "'");

		$this->cache->delete('zone');	
	}

	public function getCity($city_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "np_city WHERE city_id = '" . (int)$city_id . "'");

		return $query->row;
	}

	public function truncateCities() {
		$this->db->query("TRUNCATE TABLE " . DB_PREFIX . "np_city");
		$this->cache->delete('city');
	}

	public function getCities($data = array()) {
		$sql = "SELECT *, z.name, c.name AS city FROM " . DB_PREFIX . "np_city z LEFT JOIN " . DB_PREFIX . "np_zone c ON (z.zone_id = c.zone_id)";

		$sort_data = array(
			'c.name',
			'z.name',
			'z.code'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY c.name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCitiesByZoneId($zone_id) {
		$city_data = $this->cache->get('zone.' . (int)$zone_id);

		if (!$city_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "np_city WHERE zone_id = '" . (int)$zone_id . "' AND status = '1' ORDER BY name");

			$city_data = $query->rows;

			$this->cache->set('zone.' . (int)$zone_id, $city_data);
		}

		return $city_data;
	}

	public function getTotalCities() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "np_city");

		return $query->row['total'];
	}

	public function getTotalCitiesByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "np_city WHERE zone_id = '" . (int)$zone_id . "'");

		return $query->row['total'];
	}
}
?>