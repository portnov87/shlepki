<?php

class ControllerCommonHeader extends Controller
{
    public function index()
    {
        // Analytics
        $this->load->model('setting/extension');

        $data['analytics'] = array();

        $analytics = $this->model_setting_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();


        $data['styles'] = $this->document->getStyles();

        // OCFilter start
        $data['noindex'] = $this->document->isNoindex();
        // OCFilter end

        $isMobile=$this->isMobile();
        if (!$isMobile) {
            $data['header_nav'] = $this->load->controller('common/header_nav');
            $data['header_top'] = $this->load->controller('common/header_top');

            $data['navigation'] = $this->load->controller('common/navigation');
        }
        else {
            $data['navigation'] =false;
            $data['header_nav'] = '';
            $data['header_top'] = '';
        }

        $data['scripts'] = $this->document->getScripts('header');
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');


        $data['page_direction'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_page_direction');


        $data['responsive'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_responsive');

        $data['name'] = $this->config->get('config_name');

        $this->user = new Cart\User($this->registry);
        $module_zemez_color_switcher_permission = $this->user->hasPermission('modify', 'extension/module/zemez_color_switcher');

        if (!$module_zemez_color_switcher_permission && isset($_COOKIE['module_zemez_color_switcher_scheme'])) {
            $color_scheme = $_COOKIE['module_zemez_color_switcher_scheme'];
        } else {
            $color_scheme = $this->config->get('module_zemez_color_switcher_scheme') ? $this->config->get('module_zemez_color_switcher_scheme') : '0';
        }

        if (is_file(DIR_TEMPLATE . $this->config->get('theme_' . $this->config->get('config_theme') . '_directory') . "/stylesheet/color_schemes/" . $color_scheme . ".css")) {
            $data['color_scheme_link'] = "catalog/view/theme/" . $this->config->get('theme_' . $this->config->get('config_theme') . '_directory') . "/stylesheet/color_schemes/" . $color_scheme . ".css";
        } else {
            $data['color_scheme_link'] = "";
        }


        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        $data['theme_path'] = $this->config->get('theme_' . $this->config->get('config_theme') . '_directory');


        $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));


        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');
            $data['count_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
            $data['count_wishlist'] = isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;
        }


        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');

        $url_data = $this->request->get;

        unset($url_data['_route_']);

        $route = $url_data['route'];

        unset($url_data['route']);
        unset($url_data['lng']);
        unset($url_data['filters_group']);



        $url = '';
        if (!empty($url_data)) {
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
        }
        $url .= '&lng=ru-ru';
        $_url = $this->url->link($route, $url);

        $data['current_url_ru'] = $_url;


        $url = '';
        if (!empty($url_data)) {
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
        }
        $url .= '&lng=uk-ua';
        $route=$this->request->get['route'];
        //$route='product/product';
        $_url = $this->url->link($route, $url);

        $data['current_url_ua'] = $_url;


        if (strpos($_SERVER['REQUEST_URI'], '/ua/')!==false) //$_url==$_SERVER['REQUEST_URI'])
        {
            $this->document->setNoindex(true);
        }

        $data['noindex'] = $this->document->isNoindex();

        //$data['current_url_ua'] =<meta name="robots" content="noindex, nofollow" />

        $data['phone1'] = $this->config->get('config_telephone');
        $data['phone2'] = $this->config->get('config_telephone2');
        $data['phone3'] = $this->config->get('config_telephone3');
        $data['config_open'] = $this->config->get('config_open');



        $data['fax'] = $this->config->get('config_fax');
        $data['open'] = $this->config->get('config_open');
        $data['comment'] = $this->config->get('config_comment');
        $data['compare'] = $this->url->link('product/compare');


        $data['language'] = $this->load->controller('common/language');

        $data['language_mob'] = $this->load->controller('common/languagemob');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');
        $data['menu'] = $this->load->controller('common/menu');


        $data['carturl'] = $this->url->link('checkout/cart');




        $countProducts=count($this->cart->getProducts());

        $data['counts_cart']=$countProducts;

        //$data['mobile_nav'] = $this->load->controller('common/mobilenav');


        if ($isMobile) {
            $data['mobile_nav'] = $this->load->controller('common/mobilenav');
            $data['mobile_search'] = $this->load->controller('common/mobilesearch');
        }
        else {
            $data['mobile_nav'] = '';
            $data['mobile_search'] ='';
        }



        return $this->load->view('common/header', $data);
    }


    public function isMobile()
    {
        include_once(DIR_APPLICATION . 'model/tool/mobile/Detect.php');
        $mobile_detect = new Mobile_Detect();
        return $mobile_detect->isMobile();

    }
}
