<?php
class ControllerExtensionShippingCourier extends Controller {
    public function index() {
        $this->load->language('extension/shipping/courier');

        $data['text_message'] = $this->language->get('text_message');
        $data['button_continue'] = $this->language->get('button_continue');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_order_comment'] = $this->language->get('text_order_comment');

        $data['continue'] = $this->url->link('common/home');

        $data['shipping_cost'] = $this->config->get('courier_cost');

        $this->response->setOutput($this->load->view('extension/shipping/courier', $data));
    }
}
