<?php

class ModelExtensionShippingSat extends Model {


    public function getRegions()
    {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "sat_regions WHERE status = '1' ORDER BY name")->rows;
    }

    public function getCities($regionId) {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "sat_cities WHERE status = '1' AND region_id='$regionId' ORDER BY name")->rows;
    }

    public function getCityById($id) {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "sat_cities WHERE id = '" . (int)$id . "' AND status = '1' ORDER BY name")->rows;
    }

    public function getWarehouse($id) {
      $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sat_warehouse WHERE id = '" . (int)$id . "'");

      return $query->row;
    }

    public function getWarehousesByCity($cityId) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sat_warehouse WHERE city_id = '" . (int)$cityId . "'");

        return $query->rows;
    }

    public function getZone($id) {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "sat_cities WHERE id = '" . (int)$id . "' ORDER BY name")->row;
    }

    public function getCountry($stateId)
    {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "sat_regions WHERE id='$stateId' ORDER BY name")->rows;
    }




    public function getQuote($address) {
        $this->load->language('extension/shipping/sat');

        if ($this->config->get('shipping_sat_status')) {
            //$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('shipping_novaposhta_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            $status = true;
//            if (!$this->config->get('shipping_novaposhta_geo_zone_id')) {
//                $status = true;
//            } elseif ($query->num_rows) {
//                $status = true;
//            } else {
//                $status = false;
//            }
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $quote_data = array();

            $cost = 0.00;

//            if ($this->config->get('shipping_sat_min_total_for_free_delivery') > $this->cart->getSubTotal()) {
//                $cost = (($this->cart->getWeight() * $this->config->get('shipping_sat_delivery_price')) + $this->config->get('shipping_sat_delivery_order') + ($this->cart->getSubTotal() * $this->config->get('shipping_novaposhta_delivery_insurance') / 100));
//
//            }
//            if (!empty($_POST['payment_method'])) {
//                if ($_POST['payment_method'] == 'cash_delivery') {
//                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_sat_delivery_nal') / 100) + $this->config->get('shipping_sat_delivery_nal_fixed');
//                }
//
//            } else {
//                if (isset($this->session->data['payment_method']['code']) && $this->session->data['payment_method']['code'] == 'cash_delivery') {
//                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_sat_delivery_nal') / 100) + $this->config->get('shipping_sat_delivery_nal_fixed');
//                }
//            }

            $quote_data['sat'] = array(
                'code'          => 'sat.sat',
                'title'         => $this->language->get('text_description'),
                'cost'          => $cost,
                'tax_class_id'  => 0,
                'text'          => $this->currency->format($cost, $this->session->data['currency'])
            );

            $method_data = array(
                'code'          => 'sat',
                'title'         => $this->language->get('text_title'),
                'quote'         => $quote_data,
                'sort_order'    => $this->config->get('shipping_sat_sort_order'),
                'error'         => false
            );
        }

        return $method_data;
    }



}

?>
