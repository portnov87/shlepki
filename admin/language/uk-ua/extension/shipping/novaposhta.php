<?php
// Heading
$_['heading_title']    = 'Відправка Новою Поштою';

// Text 
$_['text_shipping']    = 'Доставка';
$_['text_success']     = 'Налаштування модуля оновлені!';

// Entry
$_['entry_geo_zone']   = 'Географична зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортування:';


$_['entry_min_total_for_free_delivery'] = 'Мінімальна сума замовлення для бескоштовной доставки:<br /><span class="help">Якщо сума замовлення більше або равна вказаной сумі, тоді вартість доставки для кліента буде дорівнювати нулю.</span>';
$_['entry_delivery_order'] = 'Вартість оформлення посилки:<br /><span class="help">Обов\'язковий фіксований платіж при оформленні.</span>';
$_['entry_delivery_price'] = 'Тариф доставки:<br /><span class="help">Вартість за 1 кг. На приклад, 1.1</span>';
$_['entry_delivery_insurance'] = 'Вартість страхування:<br /><span class="help">Відсоток від суми замовлення.</span>';
$_['entry_delivery_nal'] = 'Комісія за післяплату:<br /><span class="help">Відсоток від суми замовлення.</span>';
$_['entry_delivery_nal_fixed'] = 'Комісія за післяплату:<br /><span class="help">Фіксована сума.</span>';

// Error
$_['error_permission'] = 'У Вас немає прав для керування цим модулем!';
?>