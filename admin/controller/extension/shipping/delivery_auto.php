<?php

class ControllerExtensionShippingDeliveryAuto extends Controller {

    private $error = array();

    public function uninstall() {
        $this->db->query("DROP TABLE `" . DB_PREFIX . "delivery_auto_city`, `" . DB_PREFIX . "delivery_auto_country`, `" . DB_PREFIX . "delivery_auto_zone`;");
    }

    public function install() {
        $file = DIR_APPLICATION . 'controller/extension/shipping/delivery_auto.sql';
        if (!file_exists($file)) {
            exit('Could not load sql file: ' . $file);
        }
        $lines = file($file);
        if ($lines) {
            $sql = '';
            foreach($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    $sql .= $line;

                    if (preg_match('/;\s*$/', $line)) {
                        $sql = str_replace("DROP TABLE IF EXISTS `oc_", "DROP TABLE IF EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("CREATE TABLE IF NOT EXISTS `oc_", "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX, $sql);
                        $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . DB_PREFIX, $sql);

                        $this->db->query($sql);

                        $sql = '';
                    }
                }
            }
        }
    }

    public function index() {
        $this->load->language('extension/shipping/delivery_auto');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('shipping_delivery_auto', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['entry_delivery_nal_fixed'] = $this->language->get('entry_delivery_nal_fixed');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');
        $data['text_none'] = $this->language->get('text_none');

        $data['entry_tax'] = $this->language->get('entry_tax');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['entry_delivery_order'] = $this->language->get('entry_delivery_order');
        $data['entry_delivery_price'] = $this->language->get('entry_delivery_price');
        $data['entry_delivery_insurance'] = $this->language->get('entry_delivery_insurance');
        $data['entry_delivery_nal'] = $this->language->get('entry_delivery_nal');
        $data['entry_min_total_for_free_delivery'] = $this->language->get('entry_min_total_for_free_delivery');
        $data['entry_api_key'] = $this->language->get('entry_api_key');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_update'] = $this->language->get('button_update');
        $data['button_check_update'] = $this->language->get('button_check_update');

        $data['tab_general'] = $this->language->get('tab_general');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_shipping'),
            'href'      => $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/shipping/delivery_auto', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('extension/shipping/delivery_auto', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['delivery_auto_check'] = str_replace("&amp;","&", $this->url->link('extension/shipping/delivery_auto/checkupdate', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        $data['update'] = str_replace("&amp;","&", HTTP_SERVER . 'delivery_auto_update.php');

        $data['cancel'] = $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL');

        if (isset($this->request->post['shipping_delivery_auto_api_key'])) {
            $data['shipping_delivery_auto_api_key'] = $this->request->post['shipping_delivery_auto_api_key'];
        } else {
            $data['shipping_delivery_auto_api_key'] = $this->config->get('shipping_delivery_auto_api_key');
        }

        if (isset($this->request->post['shipping_delivery_auto_min_total_for_free_delivery'])) {
            $data['shipping_delivery_auto_min_total_for_free_delivery'] = $this->request->post['shipping_delivery_auto_min_total_for_free_delivery'];
        } else {
            $data['shipping_delivery_auto_min_total_for_free_delivery'] = $this->config->get('shipping_delivery_auto_min_total_for_free_delivery');
        }

        if (isset($this->request->post['shipping_delivery_auto_delivery_order'])) {
            $data['shipping_delivery_auto_delivery_order'] = $this->request->post['shipping_delivery_auto_delivery_order'];
        } else {
            $data['shipping_delivery_auto_delivery_order'] = $this->config->get('shipping_delivery_auto_delivery_order');
        }

        if (isset($this->request->post['shipping_delivery_auto_delivery_price'])) {
            $data['shipping_delivery_auto_delivery_price'] = $this->request->post['shipping_delivery_auto_delivery_price'];
        } else {
            $data['shipping_delivery_auto_delivery_price'] = $this->config->get('shipping_delivery_auto_delivery_price');
        }

        if (isset($this->request->post['shipping_delivery_auto_delivery_insurance'])) {
            $data['shipping_delivery_auto_delivery_insurance'] = $this->request->post['shipping_delivery_auto_delivery_insurance'];
        } else {
            $data['shipping_delivery_auto_delivery_insurance'] = $this->config->get('shipping_delivery_auto_delivery_insurance');
        }

        if (isset($this->request->post['shipping_delivery_auto_delivery_nal'])) {
            $data['shipping_delivery_auto_delivery_nal'] = $this->request->post['shipping_delivery_auto_delivery_nal'];
        } else {
            $data['shipping_delivery_auto_delivery_nal'] = $this->config->get('shipping_delivery_auto_delivery_nal');
        }

        if (isset($this->request->post['shipping_delivery_auto_delivery_nal_fixed'])) {
            $data['shipping_delivery_auto_delivery_nal_fixed'] = $this->request->post['shipping_delivery_auto_delivery_nal_fixed'];
        } else {
            $data['shipping_delivery_auto_delivery_nal_fixed'] = $this->config->get('shipping_delivery_auto_delivery_nal_fixed');
        }

        if (isset($this->request->post['shipping_delivery_auto_geo_zone_id'])) {
            $data['shipping_delivery_auto_geo_zone_id'] = $this->request->post['shipping_delivery_auto_geo_zone_id'];
        } else {
            $data['shipping_delivery_auto_geo_zone_id'] = $this->config->get('shipping_delivery_auto_geo_zone_id');
        }

        if (isset($this->request->post['shipping_delivery_auto_status'])) {
            $data['shipping_delivery_auto_status'] = $this->request->post['shipping_delivery_auto_status'];
        } else {
            $data['shipping_delivery_auto_status'] = $this->config->get('shipping_delivery_auto_status');
        }

        if (isset($this->request->post['shipping_delivery_auto_sort_order'])) {
            $data['shipping_delivery_auto_sort_order'] = $this->request->post['shipping_delivery_auto_sort_order'];
        } else {
            $data['shipping_delivery_auto_sort_order'] = $this->config->get('shipping_delivery_auto_sort_order');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/shipping/delivery_auto', $data));
    }

    public function checkupdate() {
        $status_file = DIR_APPLICATION . "delivery_auto_updating.txt";
        if (file_exists($status_file) && ((int)file_get_contents($status_file) + 5*60) > time()) { // 5 minutes
            $this->response->setOutput("RUNNING");
        }else{
            if (file_exists($status_file)){
                unlink($status_file);
            }
            $this->response->setOutput("STOPPED");
        }
    }

    public function updating() {

        $status_file = DIR_APPLICATION . "delivery_auto_updating.txt";
        exec("NULL> " . $status_file);
        exec("chmod 777 " . $status_file);
        file_put_contents($status_file, time());

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes

        $this->load->language('extension/shipping/delivery_auto');

        $json = array();

        include(DIR_SYSTEM . 'delivery/DeliveryAutoApi2.php');

        // $api_key = $this->config->get('shipping_delivery_auto_api_key');
        $api_key = true;

        // EK: clear cache before
        $delivery_auto_cache_folder = DIR_SYSTEM . 'cache/';
        if (function_exists('exec')) {
            exec('rm -rf ' . $delivery_auto_cache_folder . '*');
        }

        if(!$api_key) {
            $json['error'] = $this->language->get('text_api_key_error');
        }else {

            $delivery_auto = new \LisDev\Delivery\DeliveryAutoApi2();

            $this->load->model('delivery_auto/country');
            $regions = $delivery_auto->getRegionList();

            if ($regions['status']) {
              $this->model_delivery_auto_country->truncateCountries();
              foreach ($regions['data'] as $region) {
                if ($region['id'] != '-1'){
                  $this->model_delivery_auto_country->addCountry(array(
                      'country_id' => $region['id'],
                      'name' => $region['name'],
                      'iso_code_2' => 'UA',
                      'iso_code_3' => 'UKR',
                      'address_format' => '',
                      'postcode_required' => 0,
                      'status' => 1
                  ));
                }
              }
            } else {
              $json['error_region'] = $regions['message'];
            }

            $this->load->model('delivery_auto/city');
            $this->load->model('delivery_auto/zone');

            $cities = $delivery_auto->GetAreasList();

            if ($cities['status']) {

              $this->model_delivery_auto_zone->truncateZones();
              $this->model_delivery_auto_city->truncateCities();

              $citiesToSave = array();

              foreach ($cities['data'] as $city) {
                  $zone_id = $this->model_delivery_auto_zone->addZone(array(
                      'name' => $city['name'],
                      'country_id' => $city['regionId'],
                      'code' => '',
                      'status' => 1
                  ));

                  $warehouses = $delivery_auto->getWarehousesList(false, $city['id']);
                  if ($warehouses['status']) {
                      foreach ($warehouses['data'] as $warehouse) {
                          if (empty($warehouse['name'])) continue;
                          $citiesToSave[] = array(
                              'name' => mb_convert_case($warehouse['name'], MB_CASE_TITLE, "UTF-8"),
                              'zone_id' => $zone_id,
                              'code' => '',
                              'status' => 1
                          );
                      }
                  }
              }

              $this->model_delivery_auto_city->addCities($citiesToSave);

              $json['success'] = $this->language->get('text_update_success');

            } else {
                $json['error_cities'] = $cities['message'];
            }

        }

        // EK: clear cache after
        if (function_exists('exec')) {
            exec('rm -rf ' . $delivery_auto_cache_folder. '*');
        }

        unlink($status_file);

        /* EK: Retrieve session ID */
        global $session_id;
        session_id($session_id);
        $this->response->setOutput(json_encode($json));
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'extension/shipping/delivery_auto')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>
