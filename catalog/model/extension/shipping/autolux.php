<?php

class ModelExtensionShippingAutolux extends Model {

    public function getQuote($address) {
        $this->load->language('extension/shipping/autolux');

        if ($this->config->get('shipping_autolux_status')) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('shipping_autolux_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            if (!$this->config->get('shipping_autolux_geo_zone_id')) {
                $status = true;
            } elseif ($query->num_rows) {
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $quote_data = array();

            $cost = 0.00;

            if ($this->config->get('shipping_autolux_min_total_for_free_delivery') > $this->cart->getSubTotal()) {
                $cost = (($this->cart->getWeight() * $this->config->get('shipping_autolux_delivery_price')) + $this->config->get('shipping_autolux_delivery_order') + ($this->cart->getSubTotal() * $this->config->get('shipping_autolux_delivery_insurance') / 100));

            }
            if (!empty($_POST['payment_method'])) {
                if ($_POST['payment_method'] == 'cash_delivery') {
                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_autolux_delivery_nal') / 100) + $this->config->get('shipping_autolux_delivery_nal_fixed');
                }

            } else {
                if (isset($this->session->data['payment_method']['code']) && $this->session->data['payment_method']['code'] == 'cash_delivery') {
                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_autolux_delivery_nal') / 100) + $this->config->get('shipping_autolux_delivery_nal_fixed');
                }
            }

            $quote_data['autolux'] = array(
                'code'          => 'autolux.autolux',
                'title'         => $this->language->get('text_description'),
                'cost'          => $cost,
                'tax_class_id'  => 0,
                'text'          => $this->currency->format($cost, $this->session->data['currency'])
            );

            $method_data = array(
                'code'          => 'autolux',
                'title'         => $this->language->get('text_title'),
                'quote'         => $quote_data,
                'sort_order'    => $this->config->get('shipping_autolux_sort_order'),
                'error'         => false
            );
        }

        return $method_data;
    }

}

?>
