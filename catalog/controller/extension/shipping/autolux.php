<?php

class ControllerExtensionShippingAutolux extends Controller {
    public function index() {
        $this->load->language('extension/shipping/autolux');

        $data['text_message']             = $this->language->get('text_message');
        $data['button_continue']          = $this->language->get('button_continue');
        $data['text_district']            = $this->language->get('text_district');
        $data['text_select_district']     = $this->language->get('text_select_district');
        $data['text_city']                = $this->language->get('text_city');
        $data['text_delivery_type']       = $this->language->get('text_delivery_type');
        $data['text_delivery_department'] = $this->language->get('text_delivery_department');
        $data['text_delivery_address']    = $this->language->get('text_delivery_address');
        $data['text_office']              = $this->language->get('text_office');
        $data['text_address']             = $this->language->get('text_address');
        $data['text_order_comment']       = $this->language->get('text_order_comment');

        $data['continue'] = $this->url->link('common/home');

        $this->load->model('extension/shipping/autolux');

        $data['shipping_cost'] = $this->config->get('autolux_delivery_price');

        $data['free_total_coast'] = $this->config->get('autolux_min_total_for_free_delivery');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $this->response->setOutput($this->load->view('extension/shipping/autolux', $data));
        }else{
            return $this->load->view('extension/shipping/autolux', $data);
        }
    }


}
