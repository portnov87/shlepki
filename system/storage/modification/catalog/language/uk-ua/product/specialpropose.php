<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']       = 'Спеціальні пропозиції';
$_['label_specialpropose']       = 'Спеціальні пропозиції';

// Text
$_['text_empty_special']  = 'Зараз немає спеціальних пропозицій.';
$_['text_quantity']       = 'Кількість:';
$_['text_manufacturer']   = 'Бренд:';
$_['text_model']          = 'Артикул:';
$_['text_points']         = 'Бонусні бали:';
$_['text_price']          = 'Ціна за ящ. (eд.)';
$_['text_tax']            = 'Без ПДВ:';
$_['text_compare']        = 'Порівняти товар (%s)';
$_['text_sort']           = 'Сортувати за:';
$_['text_default']        = 'За замовчуванням';
$_['text_name_asc']       = 'Ім\'я (А - Я)';
$_['text_date_added_asc'] = 'По новинкам';
$_['text_name_desc']      = 'Ім\'я (Я - A)';
$_['text_price_asc']      = 'Від дешевих до дорогих';
$_['text_price_desc']     = 'Від дорогих до дешевих';
$_['text_special_asc']    = 'За знижку';
$_['text_rating_asc']     = 'Рейтинг (низький)';
$_['text_rating_desc']    = 'Рейтинг (високий)';
$_['text_popular_desc']   = 'За популярністю';
$_['text_model_asc']      = 'Артикул (A - Я)';
$_['text_model_desc']     = 'Артикул (Я - A)';
$_['text_limit']          = 'Показати:';
