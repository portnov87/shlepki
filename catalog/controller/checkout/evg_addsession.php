<?php

class ControllerCheckoutEvgAddsession extends Controller {
    private $error = array();

    public function index() {
      $this->language->load('checkout/lite_checkout');

      if ($this->validate()){
          $this->session->data['guest']['customer_group_id'] = 1;
          $this->session->data['guest']['firstname'] = $this->request->post['personal_data']['name'];
          $this->session->data['guest']['lastname'] = $this->request->post['personal_data']['lname'];
          $this->session->data['guest']['city'] = '';
          $this->session->data['guest']['email'] = $this->request->post['personal_data']['email'];
          $this->session->data['guest']['telephone'] = $this->request->post['personal_data']['phone'];
          $this->session->data['guest']['fax'] = '';
          $this->session->data['guest']['inputs'] = isset($this->request->post['inputs']) ? serialize($this->request->post['inputs']) : '';
          $this->session->data['guest']['payment']['firstname'] = $this->request->post['personal_data']['name'];
          $this->session->data['guest']['payment']['lastname'] = '';
          $this->session->data['guest']['payment']['company'] = '';
          $this->session->data['guest']['payment']['company_id'] = '';
          $this->session->data['guest']['payment']['tax_id'] = '';
          $this->session->data['guest']['payment']['address_1'] = isset($this->request->post['delivery_method']['address']) ? $this->request->post['delivery_method']['address'] : '';
          $this->session->data['guest']['payment']['address_2'] = '';
          $this->session->data['guest']['payment']['postcode'] = '';
          $this->session->data['guest']['payment']['city'] = isset($this->request->post['delivery_method']['office']) ? $this->request->post['delivery_method']['office'] : '';
          $this->session->data['guest']['payment']['country_id'] = isset($this->request->post['delivery_method']['state']) ? $this->request->post['delivery_method']['state'] : '';
          $this->session->data['guest']['payment']['zone_id'] = isset($this->request->post['delivery_method']['city']) ? $this->request->post['delivery_method']['city'] : '';
          $this->session->data['guest']['payment']['country'] = '';
          $this->session->data['guest']['payment']['iso_code_2'] = '';
          $this->session->data['guest']['payment']['iso_code_3'] = '';
          $this->session->data['guest']['payment']['address_format'] = '';
          $this->session->data['guest']['payment']['zone'] = '';
          $this->session->data['guest']['payment']['zone_code'] = '';
          $this->session->data['guest']['shipping_address'] = 1;
          $this->session->data['payment_country_id'] = isset($this->request->post['delivery_method']['state']) ? $this->request->post['delivery_method']['state'] : '';
          $this->session->data['payment_zone_id'] = isset($this->request->post['delivery_method']['city']) ? $this->request->post['delivery_method']['city'] : '';
          $this->session->data['guest']['shipping']['firstname'] = $this->request->post['personal_data']['name'];
          $this->session->data['guest']['shipping']['lastname'] = '';
          $this->session->data['guest']['shipping']['company'] = '';
          $this->session->data['guest']['shipping']['address_1'] = isset($this->request->post['delivery_method']['address']) ? $this->request->post['delivery_method']['address'] : '';
          $this->session->data['guest']['shipping']['address_2'] = '';
          $this->session->data['guest']['shipping']['postcode'] = '';
          $this->session->data['guest']['shipping']['city'] = isset($this->request->post['delivery_method']['office']) ? $this->request->post['delivery_method']['office'] : '';
          $this->session->data['guest']['shipping']['country_id'] = isset($this->request->post['delivery_method']['state']) ? $this->request->post['delivery_method']['state'] : '';
          $this->session->data['guest']['shipping']['zone_id'] = isset($this->request->post['delivery_method']['city']) ? $this->request->post['delivery_method']['city'] : '';
          $this->session->data['guest']['shipping']['country'] = '';
          $this->session->data['guest']['shipping']['iso_code_2'] = '';
          $this->session->data['guest']['shipping']['iso_code_3'] = '';
          $this->session->data['guest']['shipping']['address_format'] = '';
          $this->session->data['guest']['shipping']['zone'] = '';
          $this->session->data['guest']['shipping']['zone_code'] = '';
          $this->session->data['shipping_country_id'] = isset($this->request->post['delivery_method']['state']) ? $this->request->post['delivery_method']['state'] : '';
          $this->session->data['shipping_zone_id'] = isset($this->request->post['delivery_method']['city']) ? $this->request->post['delivery_method']['city'] : '';
          $this->session->data['shipping_postcode'] = '';
          $this->session->data['account'] = '';


          $shipping = explode('.', $this->request->post['delivery_method']['shipping_method']);

          if (!in_array($shipping[0], ['pickup', 'autolux', 'meest'])){
              $this->load->model('extension/shipping/'.$shipping[0]);
              $zone = $this->{'model_extension_shipping_' . $shipping[0]}->getZone($this->session->data['shipping_zone_id']);
              $country = $this->{'model_extension_shipping_' . $shipping[0]}->getCountry($zone['country_id']);

              $this->session->data['guest']['payment']['zone']  = $zone['name'];
              $this->session->data['guest']['shipping']['zone'] = $zone['name'];

              $this->session->data['guest']['payment']['country']  = $country['name'];
              $this->session->data['guest']['shipping']['country'] = $country['name'];
          }

          $this->session->data['shipping_method'] = isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]]) ? $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]] : false;
          if ($this->session->data['shipping_method']){
              $this->session->data['shipping_method']['title'] .= ' ' . $this->session->data['guest']['shipping']['country'];
              $this->session->data['shipping_method']['title'] .= ' ' . $this->session->data['guest']['payment']['zone'];
              if (isset($this->request->post['delivery_method']['office']) && !empty($this->request->post['delivery_method']['office'])) {
                $this->session->data['shipping_method']['title'] .= ' ' . $this->request->post['delivery_method']['office'];
              }
              if (isset($this->request->post['delivery_method']['address']) && !empty(trim($this->request->post['delivery_method']['address']))) {
                $this->session->data['shipping_method']['title'] .= ' ' . $this->language->get('text_delivery_address') . ': ' . $this->request->post['delivery_method']['address'];
              }
          }

          $this->session->data['comment'] = isset($this->request->post['order_comment']) ? $this->request->post['order_comment'] : '';

          $this->session->data['payment_method'] = isset($this->session->data['payment_methods'][$this->request->post['payment_method']]) ? $this->session->data['payment_methods'][$this->request->post['payment_method']] : false;
      }else{
          $this->response->redirect($this->url->link('checkout/lite_checkout'));//, 'path=' . $this->request->get['path']));

          die(json_encode(array('errors' => $this->error)));
      }


    }

    private function validate() {
      if ((utf8_strlen(trim($this->request->post['personal_data']['name'])) < 1) || (utf8_strlen(trim($this->request->post['personal_data']['name'])) > 32)) {
        $this->error['personal_data[name]'] = $this->language->get('error_firstname');
      }

      if ((utf8_strlen(trim($this->request->post['personal_data']['lname'])) < 1) || (utf8_strlen(trim($this->request->post['personal_data']['lname'])) > 32)) {
        $this->error['personal_data[lname]'] = $this->language->get('error_lastname');
      }

      if ((utf8_strlen($this->request->post['personal_data']['email']) > 96) || !filter_var($this->request->post['personal_data']['email'], FILTER_VALIDATE_EMAIL)) {
        $this->error['personal_data[email]'] = $this->language->get('error_email');
      }

      if ((utf8_strlen($this->request->post['personal_data']['phone']) < 3) || (utf8_strlen($this->request->post['personal_data']['phone']) > 32)) {
        $this->error['personal_data[phone]'] = $this->language->get('error_telephone');
      }

      if (isset($this->request->post['delivery_method']['type_delivery'])){
        if (empty($this->request->post['delivery_method']['state']) || (int)$this->request->post['delivery_method']['state'] === -1) {
          $this->error['delivery_method[state]'] = $this->language->get('error_state');
        }

        if (empty($this->request->post['delivery_method']['city']) || (int)$this->request->post['delivery_method']['city'] === -1) {
          $this->error['delivery_method[city]'] = $this->language->get('error_city');
        }

        if ($this->request->post['delivery_method']['type_delivery'] === 'warehouse') {
          if (empty($this->request->post['delivery_method']['office']) || (int)$this->request->post['delivery_method']['office'] === -1) {
            $this->error['delivery_method[office]'] = $this->language->get('error_office');
          }
        }else{
          if (empty($this->request->post['delivery_method']['address'])) {
            $this->error['delivery_method[address]'] = $this->language->get('error_address');
          }
        }
      }


      return !$this->error;
    }
}
