<?php
// Text
$_['text_subject']            = '%s - Оновлення замовлення %s';
$_['text_order_id']           = '№ замовлення:';
$_['text_date_added']         = 'Дата замовлення:';
$_['text_order_status']       = 'Ваше замовлення оновлено за наступним статусом:';
$_['text_comment']            = 'Коментар до Вашого замовлення:';
$_['text_link']               = 'Для перегляду замовлення перейдіть за посиланням нижче:';
$_['text_footer']             = 'Якщо у Вас є будь-які питання, зв\'яжіться з нами будь-яким зручним способом.';
$_['text_products']           = 'Товари';
$_['text_product']            = 'Товар';
$_['text_name']               = 'Назва';
$_['text_image']              = 'Модель';
$_['text_reduction']          = 'Знижка';
$_['text_model']              = 'Артикул';
$_['text_quantity']           = 'Кіл. ящ. (од.)';
$_['text_number_of_pockets']  = 'Кіл. вiд.';
$_['text_in_box']             = 'Пар в ящику';
$_['text_price_item']         = 'Ціна за пару (од.)';
$_['text_price']              = 'Ціна за ящ.';
$_['text_order_total']        = 'Сума';
$_['text_total']              = 'Разом';
$_['text_size']               = 'Розмiр';
