<?php
class ControllerCheckoutEvgPaymentMethod extends Controller {
	public function index() {
		$this->load->language('checkout/lite_checkout');

		$this->load->model('account/address');

		if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
			$payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
		} elseif (isset($this->session->data['guest'])) {
			$payment_address = $this->session->data['guest']['payment'];
		}

                $payment_address = 1;//SR

		if (!empty($payment_address)) {
      // Totals
      $this->load->model('setting/extension');

      $totals = array();
      $taxes = $this->cart->getTaxes();
      $total = 0;

      // Because __call can not keep var references so we put them into an array.
      $total_data = array(
        'totals' => &$totals,
        'taxes'  => &$taxes,
        'total'  => &$total
      );

      // Display prices
      if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
          $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
          if ($this->config->get('total_' . $result['code'] . '_status')) {
            $this->load->model('extension/total/' . $result['code']);

            // We have to put the totals in an array so that they pass by reference.
            $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
          }
        }

        $sort_order = array();

        foreach ($totals as $key => $value) {
          $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $totals);
      }

      $data['totals'] = array();

      foreach ($totals as $total) {
        $data['totals'][] = array(
          'title' => $total['title'],
          'text'  => $this->currency->format($total['value'], $this->session->data['currency'], '', true, true)
        );
      }

			// Payment Methods
			$method_data = array();

			$results = $this->model_setting_extension->getExtensions('payment');

      $shipping_method = isset($this->request->post['shipping_method']) ? $this->request->post['shipping_method'] : '';

			$recurring = $this->cart->hasRecurringProducts();

      foreach ($results as $result) {
				if ($this->config->get('payment_' . $result['code'] . '_status')) {
          if ($shipping_method === 'pickup') {
            if ($result['code'] === 'cheque') {
              $this->load->model('extension/payment/' . $result['code']);
              $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($payment_address, $total);
              if ($method) {
                if ($recurring) {
                  if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                    $method_data[$result['code']] = $method;
                  }
                } else {
                  $method_data[$result['code']] = $method;
                }
              }
            }
          }else{
            if ($result['code'] != 'cheque') {
              $this->load->model('extension/payment/' . $result['code']);
              $method = $this->{'model_extension_payment_' . $result['code']}->getMethod($payment_address, $total);
              if ($method) {
                if ($recurring) {
                  if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
                    $method_data[$result['code']] = $method;
                  }
                } else {
                  $method_data[$result['code']] = $method;
                }
              }
            }
          }
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['payment_methods'] = $method_data;

		}

		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_comments'] = $this->language->get('text_comments');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['payment_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods'];
		} else {
			$data['payment_methods'] = array();
		}

		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_checkout_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->session->data['agree'])) {
			$data['agree'] = $this->session->data['agree'];
		} else {
			$data['agree'] = '';
		}

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
        $this->response->setOutput($this->load->view('checkout/evg_payment_method', $data));
    }else{
        return $this->load->view('checkout/evg_payment_method', $data);
    }

	}

}
?>
