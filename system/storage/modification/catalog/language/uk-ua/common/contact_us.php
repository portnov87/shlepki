<?php
// Text
$_['text_success']   = 'Ваше повідомлення успішно відправлено!';

// Entry
$_['entry_email']    = 'E-Mail';
$_['entry_name']     = 'Iм\'я';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення від %s';

// Errors
$_['error_name']     = 'Ім\'я повинно містити від 3 до 32 символів!';
$_['error_email']    = 'E-mail адресу введено неправильно!';
$_['error_enquiry']  = 'Довжина тексту повинна бути від 10 до 3000 символів!';
