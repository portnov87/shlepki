<?php
// Heading
$_['heading_title']    = 'Кэширование';

// Text
$_['text_success']     = 'Кэш успешно пересобран';
$_['text_default']     = 'Основной магазин';

// Entry
$_['entry_import']     = 'Файл импорта .sql';
$_['entry_progress']   = 'Процесс';
$_['entry_export']     = '';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_cache']      = 'Внимание: Вы должны выбрать хотя бы один action!';
$_['error_file']       = 'Файл не найден!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to access the API!';
$_['error_stock']      = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum']    = 'Minimum order amount for %s is %s!';
$_['error_store']      = 'Product can not be bought from the store you have choosen!';
$_['error_required']   = '%s required!';
