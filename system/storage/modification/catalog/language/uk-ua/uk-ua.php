<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Locale
$_['code']                      = 'uk';
$_['direction']                 = 'ltr';
$_['date_format_short']         = 'd/m/Y';
$_['date_format_long']          = 'l dS F Y';
$_['time_format']               = 'h:i:s A';
$_['datetime_format']           = 'd/m/Y H:i:s';
$_['decimal_point']             = '.';
$_['thousand_point']            = ' ';

// Text
$_['text_home']          = 'Головна';
$_['text_meta_home']            = 'Головна';
$_['text_yes']                  = 'Так';
$_['text_no']                   = 'Ні';
$_['text_none']                 = ' --- Немає --- ';
$_['text_select']               = ' --- Оберіть --- ';
$_['text_all_zones']            = 'Усі зони';
$_['text_pagination']           = 'Відображено %d по %d з %d';
$_['text_loading']              = 'Завантажується...';
$_['text_no_results']           = 'Немає даних!';
$_['text_carousel_heading']     = 'Популярні бренди';
$_['text_carousel_heading_engros'] = 'Рекомендованi бренды';

$_['text_search']               = 'Я шукаю...';
$_['text_price_item']           = 'Ціна за пару';
$_['text_price_item_bags']      = 'Ціна за од.';
$_['text_price']                = 'Ціна за ящ.';
$_['text_new']                  = 'New';
$_['text_color']                = 'Колір';
$_['text_created']              = 'Розробили хлопці з: ';
$_['text_filter_brand']         = 'Бренд';
$_['text_filter_price']         = 'Ціна';
$_['text_filter_size']          = 'Розмiр';
$_['text_from']                 = 'Від';
$_['text_to']                   = 'До';
$_['text_phone']                = 'Номер мобільного в такому форматі +38 (XXX) XXX-XX-XX';
$_['text_table_order_id']       = 'Номер';
$_['text_table_order_date']     = 'Дата';
$_['text_table_order_stat']     = 'Статус';
$_['text_table_order_prc']      = 'Кінцева ціна';
$_['text_in_box_amount']        = 'Пар в ящику';
$_['text_amount']               = 'Кількість';
$_['text_box_amount']           = 'Кількість ящиків';
$_['text_reduction']            = 'Знижка';
$_['text_excuse']               = 'Вибачте';
$_['text_excuse_text']          = 'Cторінка, яку Ви запитуєте, не існує. Можливо вона була видалена, можливо Ви набрали невірну адресу.';
$_['text_show_whole_order']     = 'Показати всі товари';
$_['text_hide_whole_order']     = 'Сховати всі товари';
$_['text_see_all']              = 'Переглянути всi типи';
$_['text_category']             = 'Категорії';
$_['text_information']          = 'Iнформація';
$_['text_contact']              = 'Контакти';
$_['text_contact_us']           = 'Написати нам';
$_['text_phones']           = 'Телефони';
$_['text_social']               = 'Ми в соціальних мережах';
$_['text_site_map']             = 'Карта сайту';
$_['text_subscribe']            = 'Підписатися на розсилку';
$_['text_subscribe_info']       = 'Знижок, новинок, новин';
$_['text_reserved']             = 'Всі права захищені';
$_['text_filter']               = 'Фільтр';
$_['text_profile']              = 'Особистий кабінет';
$_['text_product_total']        = 'Кількість товарів';
$_['text_preview']              = 'Перегляд';
$_['text_quantity']             = 'Кількість';
$_['text_min_bags']             = 'Мінімальне замовлення з категорії "Аксессуари" - 5ть будь-яких одиниць';
$_['text_type']             		= 'Тип';
$_['text_about_us']             = 'Про нас';
$_['text_delivery_and_payment'] = 'Доставка та оплата';
$_['text_how_to_order'] 				= 'Як замовити';
$_['text_working'] 							= 'Умови роботи';
$_['text_stock_status'] 			  = 'Доставка';

// Buttons
$_['button_load_more']          = 'Завантажити ще';
$_['button_address_add']        = 'Додати адресу';
$_['button_back']               = 'Назад';
$_['button_continue']           = 'Продовжити';
$_['button_cart']               = 'До кошику';
$_['button_cart_size']               = 'Вибрати розміри';
$_['button_cancel']             = 'Відмінити';
$_['button_compare']            = 'Порівняти цей товар';
$_['button_wishlist']           = 'До бажань';
$_['button_wishlist_add']       = 'Додати до списку бажань';
$_['button_wishlist_remove']    = 'Видалити зi списку бажань';
$_['button_checkout']           = 'Перейти до оформлення замовлення';
$_['button_confirm']            = 'Підтвердити замовлення';
$_['button_coupon']             = 'Використати купон';
$_['button_delete']             = 'Знищити';
$_['button_download']           = 'Завантажити';
$_['button_edit']               = 'Змінити';
$_['button_filter']             = 'Підібрати';
$_['button_new_address']        = 'Додати адресу';
$_['button_change_address']     = 'Змінити адресу';
$_['button_reviews']            = 'Огляди';
$_['button_write']              = 'Написати огляд';
$_['button_login']              = 'Вхід';
$_['button_update']             = 'Оновлення';
$_['button_remove']             = 'Видалити';
$_['button_reorder']            = 'Повторне замовлення';
$_['button_return']             = 'Повернутися';
$_['button_shopping']           = 'Повернутися до вибору товарів';
$_['button_search']             = 'Пошук';
$_['button_shipping']           = 'Вибрати доставку';
$_['button_submit']             = 'Підтвердити';
$_['button_guest']              = 'Оформити замовлення без реєстрації';
$_['button_view']               = 'Подивитися';
$_['button_voucher']            = 'Використати подарунковий сертифікат';
$_['button_upload']             = 'Завантажити файл';
$_['button_reward']             = 'Використати бонусні бали';
$_['button_quote']              = 'Отримати ціни';
$_['button_list']               = 'Список';
$_['button_grid']               = 'Сітка';
$_['button_map']                = 'Переглянути на мапі';
$_['button_newsletter']         = 'Підписатися';
$_['button_send']               = 'Відправити';
$_['button_save']               = 'Зберегти';
$_['button_about_us']           = 'Про нас';
$_['button_delivery']           = 'Доставка i оплата';
$_['button_how_to_order']       = 'Як замовити';
$_['button_work_conditions']    = 'Умови роботи';
$_['button_contacts']           = 'Контакти';
$_['button_filters']            = 'Фільтр';
$_['button_hide']               = 'Сховати';
$_['button_reset']              = 'Cкинути';
$_['button_reset_filter']       = 'Скинути діапазон за розмірами';
$_['button_reset_price']        = 'Скинути діапазон за цінами';
$_['button_checkout_engros']    = 'Оформити замовлення';
$_['button_sort_by']            = 'Сортування';

// Error
$_['error_exception']       = 'Код помилки(%s): %s в %s лінія %s';
$_['error_upload_1']        = 'Завантажуваний файл перевищує директиву upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Завантажуваний файл перевищує директиву MAX_FILE_SIZE яка зазначена в HTML формі!';
$_['error_upload_3']        = 'Завантажуваний файл завантажено частково!';
$_['error_upload_4']        = 'Файл не завантажено!';
$_['error_upload_6']        = 'Немає тимчасової директорії!';
$_['error_upload_7']        = 'Неможливо записати файл на диск!';
$_['error_upload_8']        = 'Завантаження файлу припинено через неправильне розширення!';
$_['error_upload_999']      = 'В цієї помилки навіть коду немає!?';
$_['error_curl']            = 'CURL: Error Code(%s): %s';
$_['error_state']						=	'Виберіть область';

// Datepicker
$_['datepicker']                    = 'uk';
$_['seo_text_btn']                  = 'Читати повністю &nbsp;→';
$_['seo_text_btn_engros']           = 'Читати повністю ⬇';
$_['entry_name']      = 'Iм\'я';
$_['entry_email']     = 'E-Mail';
$_['entry_enquiry']   = 'Повідомлення';