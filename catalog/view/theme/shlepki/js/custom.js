$(document).ready(function () {
    $('#zemez-newsletter').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: 'index.php?route=extension/module/zemez_newsletter/addNewsletter',
            type: 'POST',
            dataType: 'json',
            data: $('#zemez-newsletter').find('input[type=\'text\']'),
            success: function (json) {
                if (json['success']) {
                    $('#zemez-newsletter_error').html('');
                    $('#zemez-newsletter_success').stop(true, true).html(json['success']).fadeIn(300).delay(4000).fadeOut(300);
                } else {
                    $('#zemez-newsletter_success').html('');
                    $('#zemez-newsletter_error').stop(true, true).html(json['error']).fadeIn(300).delay(4000).fadeOut(300);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
});
/*
$(function () {
    //$('body').append($('.ocfilter-mobile').remove().get(0).outerHTML);

    var options = {
        mobile: $('.ocfilter-mobile').is(':visible'),
        php: {
            searchButton: true,
            showPrice: true,
            showCounter: true,
            manualPrice: false,
            link: 'http://shlepki.test/index.php?route=product/category&path=185',
            path: '185',
            params: '',
            index: 'filter_ocfilter'
        },
        text: {
            show_all: 'Показать все',
            hide: 'Скрыть',
            load: 'Загрузка...',
            any: 'Все',
            select: 'Укажите параметры'
        }
    };

    if (options.mobile) {
        $('.ocf-offcanvas-body').html($('#ocfilter').remove().get(0).outerHTML);
    }

    $('[data-toggle="offcanvas"]').on('click', function (e) {
        $(this).toggleClass('active');
        $('body').toggleClass('modal-open');
        $('.ocfilter-mobile').toggleClass('active');
    });

    setTimeout(function () {
        console.log('ocfilter options');
        console.log(options);
        $('#ocfilter').ocfilter(options);
    }, 1);
});*/


var classNamespace = {

    // SHOW/HIDE ORDER MESSAGE TEXt
    showHideOrderMsg: function () {
        var $textarea = $('.form__textarea--add-comment');
        if ($textarea.css('display') === 'none') {
            $textarea.slideDown();
        }
        else {
            $textarea.slideUp();
        }
    },

    // CHANGE TABS SINGLE ITEM PAGE
    changeActiveTab: function () {
        var $tabs = $('.product__tab-nav-item'),
            activeTab = ~~$(this).attr('data-tab-num');
        if (!$(this).hasClass('product__tab-nav-item--active') && $tabs.length > 1) {
            for (var i = 0; i < $tabs.length; i++) {
                if ($($tabs[i]).hasClass('product__tab-nav-item--active')) {
                    $($tabs[i]).removeClass('product__tab-nav-item--active')
                }
            }
            $(this).addClass('product__tab-nav-item--active');
            showActiveTab(activeTab);
        }
    },

    // SHOW HEADER SEARCH INPUT
    showHeadSearch: function (e) {
        var $headSearch = $('.header__search-input', $(this).parent());
        $headSearch.focus();
        if (!$headSearch.hasClass('header__search-input--show')) {
            $headSearch.addClass('header__search-input--show');
            if (!$(this).hasClass('header__button--no-border')) {
                $(this).addClass('header__button--no-border')
            }
            if ($(window).width() < 767) {
                $('.header__lang').fadeOut();
                if (!$(this).hasClass('header__button--right-side')) {
                    $(this).addClass('header__button--right-side');
                }
                if (!$headSearch.hasClass('header__search-input--full-width')) {
                    $headSearch.addClass('header__search-input--full-width');
                }
            }
            e.preventDefault();
            e.target.onclick = function () {
                $headSearch.val()
                    ? $('.header__search-form').submit()
                    : classNamespace.hideHeadSearch();
            };
        }
    },

    // HIDE HEADER SEARCH INPUT
    hideHeadSearch: function (e) {
        var $headSearch = $('.header__search-input'),
            $searchBtn = $('.header__button.header__button--search');
        if ((!$headSearch.is(e.target) && ($headSearch.has(e.target).length === 0)) &&
            (!$searchBtn.is(e.target) && ($searchBtn.has(e.target).length === 0))) {
            if ($headSearch.hasClass('header__search-input--show')) {
                $headSearch.removeClass('header__search-input--show');
            }
            setTimeout(function () {
                if ($('.header__button--search').hasClass('header__button--no-border')) {
                    $('.header__button--search').removeClass('header__button--no-border')
                }
            }, 300);
            if ($(window).width() < 767) {
                $('.header__lang').fadeIn();
                if ($('.header__button--search').hasClass('header__button--right-side')) {
                    $('.header__button--search').removeClass('header__button--right-side');
                }
                if ($headSearch.hasClass('header__search-input--full-width')) {
                    $headSearch.removeClass('header__search-input--full-width');
                }
            }
        }
    },

    // SHOW/HIDE CHANGE PASSWORD FORM CONTROLS
    showHideChangePass: function () {
        var $newPassRow = $('.form__control--new-psw'),
            $newPassConfirm = $('.form__control--new-psw-confirm');
        if ($newPassRow.css('display') === 'none') {
            $newPassRow.css('display', 'block').hide().fadeIn();
        }
        else {
            $newPassRow.fadeOut(function () {
                var $newPassInput = $('.profile-content__input', $newPassRow);
                $newPassInput.val('');
            });
        }
        if ($newPassConfirm.css('display') === 'none') {
            $newPassConfirm.css('display', 'block').hide().fadeIn();
        }
        else {
            $newPassConfirm.fadeOut(function () {
                var $newPassConfirmInput = $('.profile-content__input', $newPassConfirm);
                $newPassConfirmInput.val('');
            });
        }
    },

    // BLUR ELEMENT
    blurElement: function (e) {
        if (e.keyCode === 13) {
            $(this).blur();
        }
    },

    // INPUT ONLY POSITIVE NUMBERS
    posNumInput: function () {
        var node = $(this);
        node.val(node.val().replace(/[^0-9]/g, ''));
        if ($(this).is('.cart-content__order-count') || $(this).is('.product__box-order-count')) {
            if (~~$(this).val() > 1000) {
                $(this).val(1000);
            }
        }
    },

    // SUBSCRIBE
    subscribe: function (e) {
        var $submitBtn = $('.footer__subscription-button'),
            sendData;
        $submitBtn.prop('disabled', true);
        e.preventDefault();
        sendData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: sendData,
            success: function (data) {
                var $emailInput = $('.footer__subscription-input'),
                    $errorMsgContainer = $('.footer__subscription-validation-text');
                if (data.error_email) {
                    $errorMsgContainer.html(data.error_email);
                    if (!$emailInput.hasClass('footer__subscription-input--error')) {
                        $emailInput.addClass('footer__subscription-input--error')
                    }
                    if ($errorMsgContainer.css('display') === 'none') {
                        $errorMsgContainer.show();
                    }
                }
                else {
                    var $popUp = $('.subscribe-popup'),
                        $popUpWindow = $('.subscribe-popup__window'),
                        $popUpText = $('.subscribe-popup__text');
                    $emailInput.val('');
                    if ($emailInput.hasClass('footer__subscription-input--error')) {
                        $emailInput.removeClass('footer__subscription-input--error')
                    }
                    if ($errorMsgContainer.css('display') !== 'none') {
                        $errorMsgContainer.hide();
                    }
                    $popUpText.html(data.success);
                    if ($popUp.css('display') === 'none') {
                        $popUp.fadeIn(function () {
                            $popUp.css('display', 'flex');
                            $popUpWindow.fadeIn('fast');
                        });
                    }
                }
                $submitBtn.prop('disabled', false);
            },
            error: function (xhr, str) {
                var $emailInput = $('.footer__subscription-input'),
                    $errorMsgContainer = $('.footer__subscription-validation-text');
                $errorMsgContainer.html(xhr.responseCode);
                if (!$emailInput.hasClass('footer__subscription-input--error')) {
                    $emailInput.addClass('footer__subscription-input--error')
                }
                if ($errorMsgContainer.css('display') === 'none') {
                    $errorMsgContainer.show();
                }
                $submitBtn.prop('disabled', false);
            }
        });
    },

    // CLOSE SUCCESS SUBSCRIBE POPUP
    closeSubscribePopUp: function () {
        var $popUp = $('.subscribe-popup');
        if ($popUp.css('display') !== 'none') {
            $popUp.fadeOut(function () {
                var $popUpWindow = $('.subscribe-popup__window');
                $popUpWindow.hide();
            });
        }
    },

    // SET MIN INPUT VALUE TO 1
    setMinValTo1: function () {
        if (~~$(this).val() < 1) {
            $(this).val(1);
        }
    },

    // AUTOCOMPLETE
    autocomplete: function (e) {
        var search_field = $(this).attr('class');
        var val = $(this).val();

        //setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: 'index.php?route=product/search/ajax',
            dataType: 'json',
            data: {
                keyword: val
            },
            success: function (data) {
                console.log(data);
                var $searchContainer = $('.desktop-search-result');
                //if ((data.indexOf('search-autocomplete__item') !== -1) && ($searchInput.val() !== '')) {
                //$searchContainer.append(data);
                //$searchContainer.show();


                var htmlSearch = '';
                $.each(data, function (key, value) {
                    console.log(key + ' ' + value);


                    htmlSearch += ' <div class="desktop-search-result-item">\n' +
                        '                            <img src="' + value.image + '" alt="">\n' +
                        '                            <a href="' + value.href + '">' + value.name + '</a>\n' +
                        '                        </div>';

                });
                if (htmlSearch != '') {
                    $searchContainer.html(htmlSearch);
                    $searchContainer.show();
                } else {
                    $searchContainer.html('');
                    $searchContainer.hide();
                }

                //$searchContainer.append(data);
                // <div class="desktop-search-result-item">
                //         <img src="/catalog/view/theme/shlepki/img/images/mobile-search-photo.jpg" alt="">
                //         <p>Босоножки 00001-beige</p>
                //     </div>
                //}

                // if (search_field === 'slider__search-input') {
                //     var $autocomplete = $('.search-autocomplete'),
                //         $searchInput = $('.slider__search-input');
                //     if ($autocomplete.is('.search-autocomplete')) {
                //         $autocomplete.remove();
                //     }
                //     if (data) {
                //         var $searchContainer = $('.slider__search');
                //         if ((data.indexOf('search-autocomplete__item') !== -1) && ($searchInput.val() !== '')) {
                //             $searchContainer.append(data);
                //         }
                //     }
                // } else {
                //     var $headerAutocomplete = $('.search-autocomplete'),
                //         $headerSearchInput = $('.header__search-input');
                //     if ($headerAutocomplete.is('.search-autocomplete')) {
                //         $headerAutocomplete.remove();
                //     }
                //     if (data) {
                //         var $headerSearchContainer = $('.header__search-form');
                //         if ((data.indexOf('search-autocomplete__item') !== -1) && ($headerSearchInput.val() !== '')) {
                //             $headerSearchContainer.append(data);
                //         }
                //     }
                // }
            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
        //}, 1000);
    },

    autocomplete_mobile: function (e) {

        var search_field = $(this).attr('class');
        var val = $(this).val();

        //setTimeout(function () {
        $.ajax({
            type: 'GET',
            url: 'index.php?route=product/search/ajax',
            dataType: 'json',
            data: {
                keyword: val
            },
            success: function (data) {
                console.log(data);
                var $searchContainer = $('.mobile-header_search_list');

                var htmlSearch = '';
                $.each(data, function (key, value) {
                    console.log(key + ' ' + value);

                    htmlSearch += '<div class="mobile-header_search_list-item">\n' +
                        '                        <img src="' + value.image + '" alt="">\n' +
                        '                            <a href="' + value.href + '">' + value.name + '</a>\n' +
                        '                    </div>';



                });
                if (htmlSearch != '') {
                    $searchContainer.html(htmlSearch);
                    $searchContainer.show();
                } else {
                    $searchContainer.html('');
                    $searchContainer.hide();
                }

            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
    },

    // AJAX PAGINATION
    loadNextPage: function (e) {
        var $clicked = $(this);
        e.preventDefault();

        $('.loader').fadeIn('slow');
        var url = $clicked.attr('href');
        window.history.pushState('', document.title, url);

        // GET NEW PAGE AND BUILD VIEW
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                $('.content__main').replaceWith($(data).find('.content__main'));
                setTimeout(function () {
                    setCatalogItemsBorder($('.catalog-items__item'));
                });
                $('html, body').animate({scrollTop: 0}, 500, function () {
                    $('.loader').fadeOut('slow');
                });
                var placeholder = '/catalog/view/theme/shlepki/img/Spinner.gif';
                var img_threshold = '';
                jQuery("img.lazyload").lazyload({
                    effect : "fadeIn",
                    data_attribute  : "src",
                    offset: img_threshold,
                    placeholder : placeholder,
                    load : function(elements_left, settings) {
                        this.style.width = "auto";
                    }
                });
                // var placeholder = 'https://chereviki.com.ua/catalog/view/theme/chereviki/build/img/Spinner.gif';
                // var img_threshold = '';
                // console.log(placeholder);
                // jQuery("img.lazyload").lazyload({
                //     effect: "fadeIn",
                //     data_attribute: "src",
                //     offset: img_threshold,
                //     placeholder: placeholder,
                //     load: function (elements_left, settings) {
                //         this.style.width = "auto";
                //     }
                // });
                // $(window).scrollTop(0);
            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
    },

    // LOAD MORE
    loadMore: function (e) {
        var $clicked = $(this);
        e.preventDefault();
        $('.main-product-list').addClass('loader');
        $('.loader').fadeIn('slow');
        var url = $clicked.attr('data-href');
        window.history.pushState('', document.title, url);
        $('.catalog-items__load-next-page-icon', $clicked).addClass('catalog-items__load-next-page-icon--animated');
        $clicked.prop('disabled', true);

        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {

                $('.catalog-items__item--load-next').remove();
                const $new_products = $('[data-catalog="items"]').append($(data).find('.main-product-list_item'));
                const $new_content = $(data).find('.main-katalog-wrapper');
                $new_content.find('[data-catalog="items"]').replaceWith($new_products);
                $('.main-katalog-wrapper').replaceWith($new_content);
                setTimeout(function () {
                    setCatalogItemsBorder($new_content.find('.main-product-list_item'));
                });
                $clicked.prop('disabled', false);
                $('.catalog-items__load-next-page-icon', $clicked).removeClass('catalog-items__load-next-page-icon--animated');
                //$('.loader').fadeOut('slow');

                $('.main-product-list').removeClass('loader');
                $('.show_more_products_wrapper').html($(data).find('.show_more_products'));//removeClass('loader');



                var placeholder = '/catalog/view/theme/shlepki/img/Spinner.gif';
                var img_threshold = '';
                jQuery("img.lazyload").lazyload({
                    effect : "fadeIn",
                    data_attribute  : "src",
                    offset: img_threshold,
                    placeholder : placeholder,
                    load : function(elements_left, settings) {
                        this.style.width = "auto";
                    }
                });
                //
                // $('.catalog-items__item--load-next').remove();
                // const $new_products = $('[data-catalog="items"]').append($(data).find('.main-product-list'));
                // const $new_content = $(data).find('.main-katalog-wrapper');
                // $new_content.find('[data-catalog="items"]').replaceWith($new_products);
                // //$('.main-product-list').replaceWith($new_content);
                //
                //
                //
                // //console.log(data);
                // console.log($(data).find('.main-product-list'));
                // console.log($new_products.html());
                // console.log($new_content.html());


                // setTimeout(function () {
                //     setCatalogItemsBorder($new_content.find('.main-product-list'));
                // });
                //$clicked.prop('disabled', false);
                //$('.catalog-items__load-next-page-icon', $clicked).removeClass('catalog-items__load-next-page-icon--animated');
                //$('.loader').fadeOut('slow');

            },
            error: function (xhr, str) {
                console.log(xhr.responseCode);
            }
        });
    },

    resetSizeSlider: function () {
        const $slider = $('[data-slider="size"]'),
            min = $('[data-size-min]').attr('data-size-min'),
            max = $('[data-size-max]').attr('data-size-max');
        $('[data-size-min]').val(min);
        $('[data-size-max]').val(max);
        $slider.find('.filters__handler-val-from').html(min);
        $slider.find('.filters__handler-val-to').html(max);
        $slider.slider('values', 0, min);
        $slider.slider('values', 1, max);
    },

    disabledFilters: function () {
        // filters
        if (disabled_category || disabled_manufacturer) {
            $('[data-filter-id="' + (disabled_category || disabled_manufacturer) + '"]').attr('disabled', 'disabled');
        }
    },

    // CONFIRM ORDER
    confirmOrder: function (e) {
        e.preventDefault();
        var $submitBtn = $('.form__submit-btn'),
            $form = $(this),
            sendData = $(this).serialize();
        $submitBtn.prop('disabled', true);


        if ($form.hasClass('form--login')) {
            $('.form.form--login').css('background', '#fff');
            $('.form.form--login').css('opacity', 0.3);
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: sendData,
                dataType: 'json',
                success: function (json) {

                    console.log(json);

                    if (!json.errors) {
                        if (json.success == false) {
                            $('.form__error-message').html(json.message);
                            $submitBtn.prop('disabled', false);
                        }
                        if (json.success) {
                            if ($.featherlight.current() !== null) {
                                $.featherlight.current().close();
                            }

                        }

                        if (json.result && json.new_password) {

                            //console.log(json.message);

                            $('.box_auth_credo').hide();
                            $('.box_auth_credo.login_password_sms').show();
                            $('.form__success-message').html(json.message);
                            $('.box_type_auth_select').hide();

                            $submitBtn.prop('disabled', false);
                        }
                        if (json.redirect) {
                            window.location.replace(json.redirect);
                        }
                        if (json.warning) {
                            // var $form = $('.form--popup');
                            if (!$form.hasClass('form--popup-error')) {
                                $('.form__heading').append(`<p class='form__form-warn'>${json.warning}</p>`);
                                $form.addClass('form--popup-error');
                            }
                            else {
                                $('.form__form-warn').html(json.warning);
                            }
                            $submitBtn.prop('disabled', false);
                        }
                    }
                    else {
                        if (json.errors.warning) {
                            var $emailLabel = $form.find("input[name='email']").parent();
                            $('.form__name', $form).append(`<p class='form__form-warn form__form-warn--static'>${json.errors.warning}</p>`);
                            if (!$emailLabel.hasClass('form__form-control--error')) {
                                $emailLabel.addClass('form__form-control--error');
                            }
                        }
                        $('.form__form-warn').html('');
                        $.each(json.errors, function (key, value) {
                            const $target = $(`[name='${key}']`, $form);
                            if ($target.hasClass('form__input--phone')) {
                                $target.parent().addClass('form__form-control--error');
                            }
                            else {

                                // Popup form
                                if ($form.hasClass('form--popup')) {
                                    var _value = $('.form__form-warn').html();
                                    $('.form__form-warn').html(_value + '<br/>'.value);
                                    if ($target.prev().find('.form__tip').length) {
                                        $target.prev().find('.form__tip').html(value);
                                    }
                                    else {
                                        $target
                                            .prev()
                                            .addClass('form__form-control--error')
                                            .append(`<p class="form__tip form__tip--popup">${value}</p>`);
                                        if ($target.is('textarea')) {
                                            $target.prev().find('.form__tip').addClass('form__tip--textarea');
                                            $target.addClass('form__textarea--error');
                                        }
                                        else {
                                            $target.addClass('form__input--error');
                                        }
                                    }
                                }

                                // Regular form
                                else {
                                    if ($target.parent().find('.form__tip').length) {
                                        $target.parent().find('.form__tip').html(value);
                                    }
                                    else {
                                        $target
                                            .after($('<p/>', {class: 'form__tip', text: value}))
                                            .parent().addClass('form__form-control--error');
                                    }
                                }
                            }
                        });

                        // Add confirmed validation style on form control
                        if ($form.is('.form--popup')) {
                            var $formControls = $('.form__form-control--required', $form).next();
                        }
                        else {
                            var $formControls = $('.form__input', $form);
                        }
                        for (var i = 0; i < $formControls.length; i++) {
                            if ($form.is('.form--popup')) {
                                if (!$($formControls[i]).prev().find('.form__tip').length) {
                                    if ($($formControls[i]).is('textarea')) {
                                        if (!$($formControls[i]).hasClass('form__textarea--valid')) {
                                            $($formControls[i]).addClass('form__textarea--valid');
                                        }
                                    }
                                    else {
                                        if (!$($formControls[i]).hasClass('form__input--valid')) {
                                            $($formControls[i]).addClass('form__input--valid');
                                        }
                                    }
                                }
                            }
                            else {
                                if (!$($formControls[i]).parent().find('.form__tip').length) {
                                    if (!$($formControls[i]).hasClass('form__input--valid')) {
                                        $($formControls[i]).addClass('form__input--valid');
                                    }
                                }
                            }
                        }
                        $submitBtn.prop('disabled', false);
                    }
                    ;
                    $('.form.form--login').css('background', 'none');
                    $('.form.form--login').css('opacity', 1);

                },
                error: function (xhr, str) {
                    console.log(xhr.responseCode);
                    $submitBtn.prop('disabled', false);
                }
            });


        } else {
            $('.form.form--order-form').css('background', '#fff');
            $('.form.form--order-form').css('opacity', 0.3);
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: sendData,
                dataType: 'json',
                success: function (json) {
                    if (!json.errors) {
                        if (json.success == false) {
                            $('.form__error-message').html(json.message);
                            json.ids.forEach(function (item, i, arr) {
                                $('#product-' + item).addClass('form__order-details-row--error');
                            });
                            $('html, body').animate({
                                scrollTop: $('[data-ajax-content="container"]').offset().top
                            }, 1000);
                            $submitBtn.prop('disabled', false);
                        }
                        if (json.success) {
                            if ($.featherlight.current() !== null) {
                                $.featherlight.current().close();
                            }
                            showPopup(json.success, json.afterClose ? json.afterClose : false);

                        }
                        if (json.redirect) {
                            window.location.replace(json.redirect);
                        }
                        if (json.warning) {
                            // var $form = $('.form--popup');
                            if (!$form.hasClass('form--popup-error')) {
                                $('.form__heading').append(`<p class='form__form-warn'>${json.warning}</p>`);
                                $form.addClass('form--popup-error');
                            }
                            else {
                                $('.form__form-warn').html(json.warning);
                            }
                            $submitBtn.prop('disabled', false);
                        }
                    }
                    else {
                        if (json.errors.warning) {
                            var $emailLabel = $form.find("input[name='email']").parent();
                            $('.form__name', $form).append(`<p class='form__form-warn form__form-warn--static'>${json.errors.warning}</p>`);
                            if (!$emailLabel.hasClass('form__form-control--error')) {
                                $emailLabel.addClass('form__form-control--error');
                            }
                        }
                        $.each(json.errors, function (key, value) {
                            const $target = $(`[name='${key}']`, $form);
                            if ($target.hasClass('form__input--phone')) {
                                $target.parent().addClass('form__form-control--error');
                            }
                            else {

                                // Popup form
                                if ($form.hasClass('form--popup')) {
                                    if ($target.prev().find('.form__tip').length) {
                                        $target.prev().find('.form__tip').html(value);
                                    }
                                    else {
                                        $target
                                            .prev()
                                            .addClass('form__form-control--error')
                                            .append(`<p class="form__tip form__tip--popup">${value}</p>`);
                                        if ($target.is('textarea')) {
                                            $target.prev().find('.form__tip').addClass('form__tip--textarea');
                                            $target.addClass('form__textarea--error');
                                        }
                                        else {
                                            $target.addClass('form__input--error');
                                        }
                                    }
                                }

                                // Regular form
                                else {
                                    if ($target.parent().find('.form__tip').length) {
                                        $target.parent().find('.form__tip').html(value);
                                    }
                                    else {
                                        $target
                                            .after($('<p/>', {class: 'form__tip', text: value}))
                                            .parent().addClass('form__form-control--error');
                                    }
                                }
                            }
                        });

                        // Add confirmed validation style on form control
                        if ($form.is('.form--popup')) {
                            var $formControls = $('.form__form-control--required', $form).next();
                        }
                        else {
                            var $formControls = $('.form__input', $form);
                        }
                        for (var i = 0; i < $formControls.length; i++) {
                            if ($form.is('.form--popup')) {
                                if (!$($formControls[i]).prev().find('.form__tip').length) {
                                    if ($($formControls[i]).is('textarea')) {
                                        if (!$($formControls[i]).hasClass('form__textarea--valid')) {
                                            $($formControls[i]).addClass('form__textarea--valid');
                                        }
                                    }
                                    else {
                                        if (!$($formControls[i]).hasClass('form__input--valid')) {
                                            $($formControls[i]).addClass('form__input--valid');
                                        }
                                    }
                                }
                            }
                            else {
                                if (!$($formControls[i]).parent().find('.form__tip').length) {
                                    if (!$($formControls[i]).hasClass('form__input--valid')) {
                                        $($formControls[i]).addClass('form__input--valid');
                                    }
                                }
                            }
                        }
                        $submitBtn.prop('disabled', false);
                    }
                    $('.form.form--order-form').css('background', '#fff');
                    $('.form.form--order-form').css('opacity', 0.3);

                },
                error: function (xhr, str) {
                    console.log(xhr.responseCode);
                    $submitBtn.prop('disabled', false);
                }
            });
        }
    },

    // OPEN/CLOSE ACORDEON
    openCloseAcordeon: function (e) {
        if (e === undefined) {
            var id = window.location.pathname.split('&')[1].split('=')[1],
                $idCells = $('.table__cell.table__cell--id');
            for (var i = 1; i < $idCells.length; i++) {
                if (~~id === ~~$($idCells[i]).html()) {
                    var $tableRow = $($idCells[i]).parent(),
                        $expanded = $('.table__acordeon-content', $tableRow);
                    break;
                }
            }
            if (!$tableRow) {
                return false;
            }
        }
        else {
            var $tableRow = $(this),
                $expanded = $('.table__acordeon-content', $(this));
        }
        if ($expanded.is('.table__acordeon-content')) {
            var $hideInfo = $('.table__hide-info-border', $tableRow);
            if (!$(e.target).is('img') && !$(e.target).is('a')) {
                if ($expanded.css('display') === 'none') {
                    if (!$tableRow.hasClass('table__row--active')) {
                        $tableRow.addClass('table__row--active');
                    }
                    $hideInfo.show();
                    $expanded.slideDown();
                }
                else {
                    if ($tableRow.hasClass('table__row--active')) {
                        $tableRow.removeClass('table__row--active');
                    }
                    $hideInfo.hide();
                    $expanded.slideUp();
                }
            }
        }
        else {
            $.ajax({
                type: 'POST',
                url: $tableRow.attr('data-ajax-handler'),
                data: '',
                success: function (data) {
                    if (!$tableRow.hasClass('table__row--active')) {
                        $tableRow.addClass('table__row--active');
                    }
                    $tableRow
                        .append(data)
                        .append("<div class='table__hide-info-border'><div class='table__hide-info'></div></div>")
                        .find('.table__acordeon-content').slideDown(function () {
                        $('html, body').animate({scrollTop: $tableRow.offset().top - 100});
                    });
                    $("[data-zoom-image]").ezPlus();
                },
                error: function (xhr, str) {
                    console.log(xhr.responseCode);
                }
            });
        }

    },

};


function openModal(modalBlock) {
    $('.popup').removeClass('active');
    $('.popup-signin').addClass('active');
}

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


$(document).ready(function () {
    $('#searchingField').keyup(delay(classNamespace.autocomplete, 500));
    $('#searchingMobileField').keyup(delay(classNamespace.autocomplete_mobile, 500));

    $(document).on('click', '.show_more_products', classNamespace.loadMore);
    //catalog-items__load-next-page

    $(document).on('click', '.table__row--acordeon', classNamespace.openCloseAcordeon);


    $('.cd-slider-wrapper .cd-slider li').on('click',function(){
        console.log('show carusel');
        $('.popup-carousel').addClass('active');
    });

    $('.popup__close').on('click',function(){
        $('.popup').removeClass('active');
    });


    $(document).on('change','select[name=\'delivery_method[state]\']',function () {
        var shipping_method = $('input[name=\'delivery_method[shipping_method]\']:checked').val().split('.')[0];
        console.log(shipping_method);
        $.post('index.php?route=extension/shipping/' + shipping_method + '/state', {country_id: $(this).val()}, function (response) {
            $('select[name=\'delivery_method[city]\']').html(response).removeAttr('disabled');
        });
    });

    $(document).on('change','select[name=\'delivery_method[city]\']',function () {
        //console.log('delivery_methoddddd');
        var shipping_method = $('input[name=\'delivery_method[shipping_method]\']:checked').val().split('.')[0];
        console.log(shipping_method);
        var shipping_type = $('input[name=\'delivery_method[type_delivery]\']:checked').val();
        console.log(shipping_type);
        if (shipping_type == 'warehouse') {
            $.post('index.php?route=extension/shipping/' + shipping_method + '/city', {zone_id: $(this).val()}, function (response) {
                $('select[name=\'delivery_method[office]\']').html(response).removeAttr('disabled');
            });
        }
    });

    $(document).on('change','input[name=\'delivery_method[type_delivery]\']',function () {
        //console.log($(this).val());
        if ($(this).val() == 'address') {
            $('select[name=\'delivery_method[office]\']').attr('disabled', 'disabled').parents('.wrap').hide();
            $('input[name=\'delivery_method[address]\']').removeAttr('disabled').val('').parent().show();
            $('input[name=\'delivery_method[address]\']').attr('required',true);

            $('input[name=\'delivery_method[address]\']').addClass('required');
            $('input[name=\'delivery_method[office]\']').removeClass('required');
        } else {
            var shipping_method = $('input[name=\'delivery_method[shipping_method]\']:checked').val().split('.')[0];
            //console.log(shipping_method);
            var zone_id = $('select[name=\'delivery_method[city]\']').val();
            //console.log(zone_id);
            $('input[name=\'delivery_method[office]\']').attr('required',true);
            $('input[name=\'delivery_method[office]\']').addClass('required');
            $('input[name=\'delivery_method[address]\']').removeAttr('required');
            $('input[name=\'delivery_method[address]\']').removeClass('required');
            if (zone_id != -1) {
                $.post('index.php?route=extension/shipping/' + shipping_method + '/city', {'zone_id': zone_id}, function (response) {
                    $('select[name=\'delivery_method[office]\']').html(response).removeAttr('disabled').parent().show();
                    $('input[name=\'delivery_method[address]\']').attr('disabled', 'disabled').parents('.wrap').hide();
                });
            } else {
                $('select[name=\'delivery_method[office]\']').parents('.wrap').show();
                $('input[name=\'delivery_method[address]\']').attr('disabled', 'disabled').parent().hide();
            }
        }
    });

    /*setTimeout(function(){
        const range_slider = $(".js-range-slider");
        console.log('js-range-slider');
        range_slider.ionRangeSlider({
            type: "double",
            step: 10,
            min: 0,
            max: 10000,
            from: 50,
            to: 1000,
            onChange: function (data) {
                $('#min-price_input').val(data.from);
                $('#max-price_input').val(data.to);
            },
        });

        var range_slider_instance = range_slider.data("ionRangeSlider");
        $('#min-price_input').change((e) => {
            range_slider_instance.update({
                from: e.target.value
            })
        });
        $('#max-price_input').change((e) => {
            range_slider_instance.update({
                to: e.target.value
            })
        });


        const range_slider_mobile = $(".js-range-slider_mobile");
        range_slider_mobile.ionRangeSlider({
            type: "double",
            step: 10,
            min: 0,
            max: 10000,
            from: 50,
            to: 1000,
            onChange: function (data) {
                $('#min-price_input_mobile').val(data.from);
                $('#max-price_input_mobile').val(data.to);
            },
        });

        var range_slider_instance_mobile = range_slider_mobile.data("ionRangeSlider");
        $('#min-price_input_mobile').change((e) => {
            range_slider_instance_mobile.update({
                from: e.target.value
            })
        });
        $('#max-price_input_mobile').change((e) => {
            range_slider_instance_mobile.update({
                to: e.target.value
            })
        });
    },2000);*/

    // if ($(".form__input--phone").length) {
    //     var selector = $(".form__input--phone");//document.getElementById("userPhone");
    //     var im = new Inputmask("+38 (999) 999-99-99");
    //     im.mask(selector);
    // }
    // if ($(".form__input--phone").length) {
    //     var selector = $(".form__input--phone");//document.getElementById("mobuserPhone");
    //     var im = new Inputmask("+38 (999) 999-99-99");
    //     im.mask(selector);
    // }

    //$('.form__input--phone').

    //$('.header__search-input--mobile').keyup(delay(classNamespace.autocomplete_mobile, 500));

    var placeholder = '/catalog/view/theme/shlepki/img/Spinner.gif';
    var img_threshold = '';
    jQuery("img.lazyload").lazyload({
        effect : "fadeIn",
        data_attribute  : "src",
        offset: img_threshold,
        placeholder : placeholder,
        load : function(elements_left, settings) {
            this.style.width = "auto";
        }
    });
});
