<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 30.10.2019
 * Time: 07:30
 */
class ModelForsageApi extends Model {
    protected $keys=[
        0=>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzMDAsImlzcyI6Imh0dHBzOi8vZm9yc2FnZS1zdHVkaW8uY29tL2dlbmVyYXRlVG9rZW4vMTMwMCIsImlhdCI6MTU3OTA5OTE1MCwiZXhwIjo0NjQ2Mjk5MTUwLCJuYmYiOjE1NzkwOTkxNTAsImp0aSI6Ik5lUXl3VlVxZnNCakFFT1QifQ.nS5CI6FyyHHgZgQu27TjspA_tOz03Ay7aRQe0zqwejk'
    ];
    protected $url_api = 'https://forsage-studio.com/api/';

    public function getKey($store=0)
    {
        $keys=$this->getKeys();
        if (isset($keys[$store])) return $keys[$store];

        return false;
    }
    public function getKeys()
    {
        return $this->keys;
    }

    public function getStore()
    {
        return 0;
    }

    public function setStore($store)
    {
        $this->store=$store;
        return $this;
    }




    protected function request($action, $post = [],$get=[])
    {

        $store=$this->getStore();
        //echo '$store = '.$store."\r\n";
        $key = $this->getKey($store);
        //echo $key."\r\n\r\n";

        $url = $this->url_api . $action . '?token=' . $key;
        if (count($get)>0){

            foreach ($get as $key=>$value) {
                $url .= '&' . $key.'='.$value;//implode('&', $get);
            }
        }
        echo ' url '.$url."<br/>\r\n\r\n";//die();

        if ($ch = curl_init($url)) {
            curl_setopt($ch, CURLOPT_HEADER, 0);
            //$header=['Authorization'=> 'Bearer '.$key];
            //curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
            curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
            //curl_setopt($ch, CURLOPT_USERPWD, $login . ':' . $password);


            //{ "Authorization": "Bearer {your_api_token}" }
            if (count($post) > 0) {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                $postdata = json_encode($post);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            } else curl_setopt($ch, CURLOPT_POST, 0);

            $res = curl_exec($ch);
            curl_close($ch);
            $res1 = json_decode($res);

            return $res1;
        }
        return false;

    }

    public function getChanges($type='full',$period=false,$period_finish=false)
    {
        $action = 'get_changes';
        if ($period){
            $post = [
                'start_date' => strtotime($period),//"-5 week"),
            ];
        }else {
            $post = [
                'start_date' => strtotime("-1 week"),
            ];
        }
        $post['products'] = $type;

        if ($period_finish){
            $post['end_date'] =  strtotime($period_finish);//

        }
        $result = $this->request($action,[],$post);
        return $result;
    }

    public function getProducts($period=false,$period_finish=false, $stock=false)
    {
        $action = 'get_products';
        $post=[];
        if ($period){
            $post['start_date'] =  strtotime($period);//,//"-5 week"),

        }else {
            $post['start_date'] =  strtotime("-5 week");//
            /*$post = [
                'start_date' => strtotime("-5 week"),
            ];*/
        }
        if ($stock){
            $post['quantity'] = $stock;
        }
        if ($period_finish){
            $post['end_date'] =  strtotime($period_finish);//

        }
        $result = $this->request($action,[],$post);
        return $result;
    }



    public function getRefbookCharacteristics()
    {
        $action = 'get_refbook_characteristics';
        $result = $this->request($action);
        return $result;
    }
    public function getBrands()
    {
        $action = 'get_brands';
        $result = $this->request($action);
        return $result;
    }

    public function getSuppliers()
    {
        $action = 'get_suppliers';
        $result = $this->request($action);
        return $result;
    }

    public function GetProductsBySupplier($supplier_id,$stock=false)
    {
        $action = 'get_products_by_supplier/' . $supplier_id;
        if ($stock){
            $post['quantity'] = $stock;
        }
        $result = $this->request($action,[],$post);
        return $result;

    }

    public function GetProduct($product_id, $params=['watermark'=>'bottom'])
    {
        $action = 'get_product/' . $product_id;
        $result = $this->request($action,[],$params);
        return $result;
    }
}