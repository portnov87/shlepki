<?php
class ControllerCommonLanguage extends Controller {
	public function index() {
		$this->load->language('common/language');

		$data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

		$data['code'] = $this->session->data['language'];

		$this->load->model('localisation/language');

		$data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();
        $url_data = $this->request->get;
        unset($url_data['_route_']);

        $route = $url_data['route'];


        unset($url_data['route']);
        unset($url_data['lng']);

		/*foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'name' => $result['name'],
					'code' => $result['code']
				);
			}
		}*/

        foreach ($results as $result) {
            if ($result['status']) {
                $expl = (explode('-', $result['code']));


                if (isset($url_data['filter_ocfilter'])) {
                    $filter_ocfilter = $url_data['filter_ocfilter'];
                    if (strpos($filter_ocfilter, 'm:') !== false) {
                        $filter_ocfilterarray = explode(';', $filter_ocfilter);
                        $filter_ocfilterarrayCopy = $filter_ocfilterarray;
                        foreach ($filter_ocfilterarray as $key => $f_i) {
                            if (strpos($f_i, 'm:') !== false) {
                                unset($filter_ocfilterarrayCopy[$key]);
                            }
                        }
                        $filter_ocfilter = implode(';', $filter_ocfilterarrayCopy);

                        $url_data['filter_ocfilter']=$filter_ocfilter;
                    }
                }



                $url = '';
                if (!empty($url_data)) {
                    $url = '&' . urldecode(http_build_query($url_data, '', '&'));
                }
                $url .= '&lng=' . $result['code'];





                if ($route == '') {
                    if ($result['code'] == 'uk-ua')
                        $_url = '/ua';
                    else $_url = '/';///ru';

                } else
                    $_url = $this->url->link($route, $url);



                $data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code'],
                    'url' => $_url,
                    'code_name' => ucfirst(trim($expl[1]))
                );
            }
        }

//        echo '<pre> config_language_id = '.$this->config->get('config_language_id');
//        print_r($this->request->get);
//        echo '</pre>';
//        die();

        if (!isset($this->request->get['route']) || (isset($this->request->get['route']) && substr($this->request->get['route'], 0, 4) == 'api/')) {
            $data['redirect'] = $this->url->link('common/home');
            $lang = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
            $data['code'] = $lang['code'];
        } else {


//            $lang = $this->model_localisation_language->getLanguage(2);
//
//            $data['code'] = $lang['code'];

            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data['route'];


            unset($url_data['route']);
            unset($url_data['lng']);
            unset($url_data['filters_group']);

            $url = '';

            if (!empty($url_data)) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }


            $data['redirect'] = $this->url->link($route, $url);
        }

        if (isset($this->request->get['route'])) {
            $data['route'] = $this->request->get['route'];
        } else {
            $data['route'] = 'common/home';
        }

        if (isset($url)) {
            $data['url'] = $url;
        } else {
            $data['url'] = '';
        }

        /*

		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}
        */


		return $this->load->view('common/language', $data);
	}

	public function language() {
		/*if (isset($this->request->post['code'])) {
			$this->session->data['language'] = $this->request->post['code'];
		}

		if (isset($this->request->post['redirect'])) {
			$this->response->redirect($this->request->post['redirect']);
		} else {
			$this->response->redirect($this->url->link('common/home'));
		}*/
        if (isset($this->request->post['code'])) {
            $this->session->data['language'] = $this->request->post['code'];
        }

        if (isset($this->request->post['route']) && isset($this->request->post['url'])) {
            $this->request->get['lng'] = $this->request->post['code'];
            $this->request->post['redirect'] = $this->url->link($this->request->post['route'], str_replace('&amp;', '&', $this->request->post['url']));
        }

        echo '<pre>post';
        print_R($this->request->post);
        echo '</pre>';
        echo '<pre>get';
        print_R($this->request->get);
        echo '</pre>';

        echo '<pre>session';
        print_R($this->session->data);
        echo '</pre>';
        die();
        if (isset($this->request->post['redirect'])) {
            $url = $this->request->post['redirect'];
        } else {
            $url = $this->url->link('common/home');
        }

        $this->response->redirect($url);
	}
}