<?php

class ControllerExtensionModuleNewCollection extends Controller
{
    public function index($setting)
    {
        $this->load->language('extension/module/new_collection');

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['categories'] = array();

        $results = $this->model_catalog_category->getNewCategories();
        //$countSpecialProducts=$this->model_catalog_product->getTotalProductSpecialsPropose();
        //$data['countSpecialProducts'] = $countSpecialProducts;

        if ($results) {

            foreach ($results as $index=>$result) {

//                if ($result['image']) {
//                    $image = $this->model_tool_image->resize($result['image'], 354, 477);
//                } else {
//                    $image = $this->model_tool_image->resize('no_image.png', 354, 477);
//                }

                switch ($index)
                {
                    case 0:
                        $width=589;
                        $height=462;
                        break;
                    case 1:
                        $width=282;
                        $height=462;
                        break;
                    case 2:
                    case 3:
                    case 4:
                    $width=286;
                    $height=139;
                        break;


                }
                $image = $this->model_tool_image->resize($result['image'], $width, $height);

                if ($result['category_id']=='255')
                    $url=$this->url->link('product/latest');
                else $url=$this->url->link('product/category', 'path=' . $result['category_id']);

                $data['categories'][] = array(
                    'thumb' => $image,
                    'name' => $result['name'],
                    'icon' => $result['icon'],
                    //'icon_class' => $result['icon_class'],
                    'href' =>$url
                );
            }
        }

        return $this->load->view('extension/module/new_collection', $data);
    }
}
