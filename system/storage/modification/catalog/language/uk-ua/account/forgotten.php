<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']   = 'Забули пароль?';

// Text
$_['text_account']    = 'Обліковий запис';
$_['text_forgotten']  = 'Забули пароль';
$_['text_your_email'] = 'Ваш E-Mail';
$_['text_email']      = 'Будь ласка впишіть адресу електронної пошти. Ми надішлемо посилання для відновлення пароля.';
$_['text_success']    = 'На пошту %s була вислана посилання для поновлення пароля.';

// Entry
$_['entry_email']     = 'E-Mail';
$_['entry_passDef']   = 'Пароль';
$_['entry_password']  = 'Новий пароль';
$_['entry_confirm']   = 'Підтвердити';

// Error
$_['error_email']     = 'Цей E-Mail не зареєстровано на нашому сайті!';
$_['error_approved']  = 'Увага: Ваш аккаунт має бути підтвердженним перед тим як увійти.';
$_['error_password']  = 'Пароль має містити від 4 до 20 символів!';
$_['error_confirm']   = 'Пароль та пароль для підтвердження не співпадають!';

$_['button_back']     = 'Повернутися';
