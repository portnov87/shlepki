<?php

class ControllerExtensionModuleZemezMegaMenu extends Controller
{

    public function index($setting)
    {
        $this->document->addScript('catalog/view/theme/' . $this->config->get('theme_' . $this->config->get('config_theme') . '_directory') . '/js/zemez_megamenu/superfish.min.js');
        $this->document->addScript('catalog/view/theme/' . $this->config->get('theme_' . $this->config->get('config_theme') . '_directory') . '/js/zemez_megamenu/jquery.rd-navbar.min.js');

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('extension/module/zemez_megamenu');

//        echo (int)$this->config->get('config_language_id');
//        die();
//        $categories = $this->model_catalog_category->getCategories(0);
//        foreach ($categories as $categorie_id => $categorie) {
//            if (!$categorie['top']) {
//                unset($categories[$categorie_id]);
//            }
//        }
//
//        $categories = array_values($categories);

        $data['menu_items'] = [];


//        echo '<pre>$categories';
//        print_r($categories);
//        echo '</pre>';
//        die();

        $cached_menu = $this->cache->get('menu_items.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id'));
//        echo 'menu_items.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id');
//        die();
        //$this->cache->set('menu_items.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id'), false);
        if ($cached_menu) {
            //echo 'cached '.$cached_menu."<br/><br/>";
            $data['menu_items']=$cached_menu;

        } else {
            $categories = $this->model_catalog_category->getCategoriesForMenu(0);

            $menu_items=[];
            //<ul class="sf-menu sf-js-enabled sf-arrows">';
            foreach ($categories as $category)
            {
                $list='';
                $name=$category['name'];
                $category_id=$category['category_id'];

                $categoriesChilds = $this->model_catalog_category->getCategoriesForMenu($category_id);



                if (count($categoriesChilds)>0)
                {

                    $list .= "<li><div>\n<a href=\"" . $this->url->link('product/category', 'path=' . $category_id, true) . "\">" . $name.'<span>
                                 <svg width="13" height="6" viewBox="0 0 13 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.1247 5.50024C6.75948 5.79242 6.24052 5.79242 5.87531 5.50024L1.22609 1.78087C0.487949 1.19036 0.905504 0 1.85078 0L11.1492 0C12.0945 0 12.5121 1.19036 11.7739 1.78087L7.1247 5.50024Z"/>
                                 </svg>
                              </span></a>';
                    $parentId=$category_id;
                   // die();
                    $list .= "<div class=\"sub-menu_wrapper\">
						<ul class=\"sub-menu\">\n";
                    foreach ($categoriesChilds as $catChild)
                    {
                        if ($catChild['count_products']>0) {
                            $category_id = $catChild['category_id'];
                            $name = $catChild['name'];
                            $list .= "<li style=''>\n<a href=\"" . $this->url->link('product/category', 'path=' . $parentId.'_'.$category_id, true) . "\"><span class=\"sub-nav_menu-text\">" . $name . "</span><span class=\"sub-nav_menu-counter\">(" . $catChild['count_products'] . ")</span></a></li>\n";
                        }
                        //<span class=\"sub-nav_menu-counter\">(".$catChild['count_products'].")</span>
                    }
                    $list .= "</ul></div></div>\n";


                }
                else
                {
                    if ($category_id==259)
                        $list .= "<li>\n<a href=\"" . $this->url->link('product/manufacturer') . "\">" . $name.'</a>';
                    elseif ($category_id==258)
                        $list .= "<li>\n<a href=\"" . $this->url->link('product/special') . "\">" . $name.'</a>';
                    elseif ($category_id==255)
                        $list .= "<li>\n<a href=\"" . $this->url->link('product/latest') . "\">" . $name.'</a>';
                    else
                        $list .= "<li>\n<a href=\"" . $this->url->link('product/category', 'path=' . $category_id, true) . "\">" . $name.'</a>';
                }
                $list .=  "\n</li>\n";
                $menu_items[]=$list;
                //float:left; width:50%;dispay:inline;
            }

            $data['menu_items']=$menu_items;//$list;


            $this->cache->set('menu_items.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_language_id'), $data['menu_items']);
        }



        return $this->load->view('extension/module/zemez_megamenu', $data);


    }

    function getCatTree($category_id = 0)
    {
        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        $this->load->model('catalog/category');
        $category_data = "";

        $categories = $this->model_catalog_category->getCategories((int)$category_id);

        foreach ($categories as $category) {
            $name = $category['name'];
            $href = $this->url->link('product/category', 'path=' . $category['category_id']);
            $class = in_array($category['category_id'], $parts) ? " class=\"active\"" : "";
            $parent = $this->getCatTree($category['category_id']);
            if ($parent) {
                $class = $class ? " class=\"active\"" : " class=\"parent\"";
            }
            $category_data .= "<li>\n<a href=\"" . $href . "\"" . $class . ">" . $name . "</a>" . $parent . "\n</li>\n";

        }

        return strlen($category_data) ? "<ul class=\"simple_menu\">\n" . $category_data . "\n</ul>\n" : "";
    }
}