<?php
// Heading
$_['text_newsletter']       = 'Підпишіться на нашу розсилку:';

// Entry
$_['entry_mail']          = 'E-mail';

// Text
$_['button_subscribe']    = 'Подписаться';
$_['text_success']        = 'Вы успешно подписались';

//Errors
$_['error_exist_user']    = 'Пользователь уже зарегистрирован';
$_['error_exist_email']   = 'Указанный адрес электронной почты уже зарегистрирован';
$_['error_invalid_email'] = 'Пожалуйста, введите правильный адрес электронной почты!';