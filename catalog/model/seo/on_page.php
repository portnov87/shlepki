<?php
class ModelSeoOnPage extends Model {
  public function getOnPageText($seo_url_id, $category_id = 0) {
    $result = [];

    if (is_array($category_id)){
        if (count($category_id)>0) {
                    $query_str =
                        "SELECT DISTINCT * FROM
                " . DB_PREFIX . "seo_on_page sp
              LEFT JOIN
               " . DB_PREFIX . "seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
              WHERE
                seo_url_id='" . (int)$seo_url_id . "'
              AND 
                category_id IN ('" . implode(',', $category_id) . "')
              AND
                language_id='" . (int)$this->config->get('config_language_id') . "'
            ";
        }else{
            $query_str =
                "SELECT DISTINCT * FROM
                " . DB_PREFIX . "seo_on_page sp
              LEFT JOIN
               " . DB_PREFIX . "seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
              WHERE
                seo_url_id='" . (int)$seo_url_id . "'              
              AND
                language_id='" . (int)$this->config->get('config_language_id') . "'
            ";

        }
    }else {




        if ($category_id==='60_61_62')
        {

            $query_str =
                "SELECT DISTINCT * FROM
                    " . DB_PREFIX . "seo_on_page sp
                  LEFT JOIN
                   " . DB_PREFIX . "seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
                  WHERE
                    seo_url_id='" . (int)$seo_url_id . "'
                  AND 
                    category_id=0
                  AND
                    language_id='" . (int)$this->config->get('config_language_id') . "'
                ";

        }
        elseif ((int)$category_id>0)//(($category_id==0)||($category_id==''))
        {

            $query_str =
                "SELECT DISTINCT * FROM
                    " . DB_PREFIX . "seo_on_page sp
                  LEFT JOIN
                   " . DB_PREFIX . "seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
                  WHERE
                    seo_url_id='" . (int)$seo_url_id . "'
                  AND 
                    category_id=" . (int)$category_id . "
                  AND
                    language_id='" . (int)$this->config->get('config_language_id') . "'
                ";


        }else {

            $query_str =
                "SELECT DISTINCT * FROM
                    " . DB_PREFIX . "seo_on_page sp
                  LEFT JOIN
                   " . DB_PREFIX . "seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
                  WHERE
                    seo_url_id='" . (int)$seo_url_id . "'
                  AND 
                    category_id=0
                  AND
                    language_id='" . (int)$this->config->get('config_language_id') . "'
                ";

        }
    }



    $query = $this->db->query($query_str);


    if ($query->num_rows){
      $result = [
        'head'             => $query->row['head'],
        'heading'          => $query->row['heading'],
        'seo_text'         => $query->row['seo_text'],
          'short_description'         => $query->row['short_description'],
        'meta_title'       => $query->row['meta_title'],
        'meta_description' => $query->row['meta_description'],
      ];
    }

    return $result;
  }
}
