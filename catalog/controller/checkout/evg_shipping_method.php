<?php

class ControllerCheckoutEvgShippingMethod extends Controller
{
    public function index()
    {
        $this->load->language('checkout/checkout');

        $this->load->model('account/address');

        if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {
            $shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);
        } elseif (isset($this->session->data['guest'])) {
            if (empty($this->session->data['guest']['shipping'])) $this->session->data['guest']['shipping'] = '';//SR
            $shipping_address = $this->session->data['guest']['shipping'];
        }

        $shipping_address = 1;//SR

        if (!empty($shipping_address)) {
            // Shipping Methods
            $method_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('shipping');

//			echo '<pre>$results';
//			print_r($results);
//			echo '</pre>';
//			die();
            foreach ($results as $result) {
                if ($this->config->get('shipping_' . $result['code'] . '_status')) {
                    $this->load->model('extension/shipping/' . $result['code']);

                    $quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($shipping_address);

                    if ($quote) {
                        if (file_exists(DIR_APPLICATION . 'view/theme/' . $this->config->get('theme_default_directory') . '/build/img/' . $quote['code'] . '.png')) {
                            $image = 'catalog/view/theme/' . $this->config->get('theme_default_directory') . '/build/img/' . $quote['code'] . '.png';
                        } else {
                            $image = false;
                        }
                        $method_data[$result['code']] = array(
                            'title' => $quote['title'],
                            'quote' => $quote['quote'],
                            'sort_order' => $quote['sort_order'],
                            'error' => $quote['error'],
                            'image' => $image
                        );
                    }
                }
            }

            $sort_order = array();

            foreach ($method_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $method_data);

            $this->session->data['shipping_methods'] = $method_data;
        }


        $data['theme_path'] = $this->config->get('theme_default_directory');

        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comments'] = $this->language->get('text_comments');

        $data['button_continue'] = $this->language->get('button_continue');

        if (empty($this->session->data['shipping_methods'])) {
            $data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['shipping_methods'])) {
            $data['shipping_methods'] = $this->session->data['shipping_methods'];
            $data['active_shipping_methods'] = $this->load->controller('extension/shipping/novaposhta');
        } else {
            $data['shipping_methods'] = array();
            $data['active_shipping_methods'] = array();
        }

        if (isset($this->session->data['shipping_method']['code'])) {
            $data['code'] = $this->session->data['shipping_method']['code'];
        } else {
            $data['code'] = '';
        }

        if (isset($this->session->data['comment'])) {
            $data['comment'] = $this->session->data['comment'];
        } else {
            $data['comment'] = '';
        }

        return $this->load->view('checkout/evg_shipping_method', $data);
    }

}

?>
