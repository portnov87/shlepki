<?php

class ControllerExtensionShippingMeest extends Controller {
    public function index() {
        $this->load->language('extension/shipping/meest');

        $data['text_message']             = $this->language->get('text_message');
        $data['button_continue']          = $this->language->get('button_continue');
        $data['text_district']            = $this->language->get('text_district');
        $data['text_select_district']     = $this->language->get('text_select_district');
        $data['text_city']                = $this->language->get('text_city');
        $data['text_delivery_type']       = $this->language->get('text_delivery_type');
        $data['text_delivery_department'] = $this->language->get('text_delivery_department');
        $data['text_delivery_address']    = $this->language->get('text_delivery_address');
        $data['text_office']              = $this->language->get('text_office');
        $data['text_address']             = $this->language->get('text_address');
        $data['text_order_comment']       = $this->language->get('text_order_comment');

        $data['continue'] = $this->url->link('common/home');

        $this->load->model('extension/shipping/meest');

        if ($this->language->data['code'] === 'uk'){
          $code = 'UA';
        }else{
          $code = mb_strtoupper($this->language->data['code']);
        }

        $data['lang'] = "Description".$code;

        $data['countries'] = $this->model_extension_shipping_meest->getRegions();

        $data['shipping_cost'] = $this->config->get('meest_delivery_price');

        $data['free_total_coast'] = $this->config->get('meest_min_total_for_free_delivery');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
          $this->response->setOutput($this->load->view('extension/shipping/meest', $data));
        }else{
          return $this->load->view('extension/shipping/meest', $data);
        }
    }

    public function state() {
        $this->load->model('extension/shipping/meest');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            if ($this->language->data['code'] === 'uk'){
              $code = 'UA';
            }else{
              $code = mb_strtoupper($this->language->data['code']);
            }
    
            $lang = "Description".$code;

            $cities = $this->model_extension_shipping_meest->getCitiesByRegion($this->request->post['country_id']);

            $response = sprintf('<option value="%s" >%s</option>', '-1', 'Выберите город...');

            foreach($cities as $city) {
                $response .= sprintf('<option value="%s" >%s</option>', $city['uuid'], $city[$lang]);
            }

            $this->response->setOutput($response);
        }
    }

    public function city() {
        $this->load->model('extension/shipping/meest');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {

            if ($this->language->data['code'] === 'uk'){
              $code = 'UA';
            }else{
              $code = mb_strtoupper($this->language->data['code']);
            }
    
            $lang = "Description".$code;

            $offices = $this->model_extension_shipping_meest->getBranchByCity($this->request->post['zone_id']);

            $response = sprintf('<option value="%s" >%s</option>', '-1', 'Выберите отделение...');

            foreach($offices as $office) {
                $branch = '№'.$office['BranchCode'].' - '.$office[$lang];
                $response .= sprintf('<option value="%s" >%s</option>', $branch, $branch);
            }

            $this->response->setOutput($response);
        }
    }

}
