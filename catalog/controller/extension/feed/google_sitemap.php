<?php

class ControllerExtensionFeedGoogleSitemap extends Controller
{
    public $in_file = false;
    public $map_file = 'sitemap_{store_id}.xml';

    public function index()
    {
        $map_file = str_replace('{store_id}', $this->config->get('config_store_id'), $this->map_file);

        if (!$this->in_file || ($this->config->get('feed_google_sitemap_status') && !is_file($map_file))) {
            $output = $this->generate();
            if ($this->in_file) {
                $file = fopen($map_file, "w");
                fwrite($file, $output);
                fclose($file);
                echo "FILE GENERATE OK";
            } else {
                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            }
        } else {
            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput(file_get_contents($map_file));
        }
    }

    public function view_xml()
    {
        header('Content-type: text/xml');
        return $this->generate();
    }

    public function generate()
    {
        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">';

        $this->load->model('catalog/category');

        $output .= $this->getCategories(0, '', $this->config->get('config_store_id'),'');

        $this->load->model('catalog/manufacturer');

        $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($manufacturers as $manufacturer) {
            //if ($this->config->get('config_store_id') === '2') {
                $output .= '<url>';

                $output .= '  <loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']) . '</loc>';
                if ($this->config->get('config_store_id') === '1') {
                //    $output .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']) . '&lng=ua-uk" />';
                }
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <priority>0.7</priority>';
                $output .= '</url>';
           // }
        }

        $this->load->model('catalog/information');

        $informations = $this->model_catalog_information->getInformations();

        foreach ($informations as $information) {
            //if ($this->config->get('config_store_id') === '2') {
                $output .= '<url>';
                $output .= '  <loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id']) . '</loc>';
                if ($this->config->get('config_store_id') === '4') {
              //      $output .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $this->url->link('information/information', 'information_id=' . $information['information_id']) . '&lng=ua-uk" />';
                }
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <priority>0.5</priority>';
                $output .= '</url>';
            //}
        }

        $output .= '</urlset>';
        $output = str_replace('&', '&amp;', $output);

        return $output;
    }

    protected function getCategories($parent_id, $current_path = '', $store_id, $store_url)
    {
        $output = '';

        $results = $this->model_catalog_category->getCategories($parent_id);

        foreach ($results as $result) {
            if (!$current_path) {
                $new_path = $result['category_id'];
            } else {
                $new_path = $current_path . '_' . $result['category_id'];
            }




            $paths=[];

            if ($result['parent_id']>0)
            {
                $query_key='category_id='.$result['parent_id'];
                $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                $query = $this->db->query($sql);

                $row = $query->row;
                $path_url_ua='';
                if ($row){

                    $paths[]=$row['keyword'];
                    $query_key='category_id='.$result['category_id'];
                    $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                    $query = $this->db->query($sql);

                    $row = $query->row;
                    $path_url_ua='';
                    if ($row){

                        $paths[]=$row['keyword'];


                    }

                }
            }else{
                $query_key='category_id='.$result['category_id'];
                $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                $query = $this->db->query($sql);

                $row = $query->row;
                $path_url_ua='';
                if ($row){

                    $paths[]=$row['keyword'];


                }
            }
            if (count($paths)>0)
            {
                $path_url=$store_url.'/'.implode('/',$paths);//$row['keyword'];
                $path_url_ua=$store_url.'/ua/'.implode('/',$paths);
            }
/*
            $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
            $query = $this->db->query($sql);

            $row = $query->row;
            $path_url_ua='';
            if ($row){
                $path_url=$store_url.'/'.$row['keyword'];
                $path_url_ua=$store_url.'/ua/'.$row['keyword'];
            }*/

            /*if ($result['parent_id']>0)
                $new_path=$result['parent_id'].'_'.$result['category_id'];
            else
                $new_path=$result['category_id'];
            $path_url=$this->url->link('product/category', 'path=' . $new_path);*/
            if ($path_url!='') {
                $output .= '<url>';

                $output .= '  <loc>' . $path_url . '</loc>';//$this->url->link('product/category', 'path=' . $new_path)
                // if ($store_id=== '1') {
                //$output .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $path_url_ua . '" />';
                // }
                $output .= '  <changefreq>weekly</changefreq>';
                $output .= '  <priority>0.7</priority>';
                $output .= '</url>';
            }

            $output .= $this->getCategories($result['category_id'], $new_path, $store_id,$store_url);
        }

        return $output;
    }

    protected function getProducts($store_id, $store_url)
    {
        $sql = "SELECT * FROM oc_product WHERE status=1";//_index_" . $store_id;
        $query = $this->db->query($sql);
        $rows = $query->rows;

        //$collection_products = Mage::getModel('catalog/product')->getCollection();
        //$resource = Mage::getSingleton('core/resource');
        //$readConnection = $resource->getConnection('core_read');

        $size = count($rows);
        if ($size>10000)
            $count_maps = ceil($size / 10000);
        else $count_maps=1;

        $maps_products = [];
        for ($i = 1; $i <= $count_maps; $i++) {
            $limit = 10000;
            $offset = $i;

            $sql = "SELECT * FROM oc_product LIMIT $offset,$limit";
            //" . $store_id."
            $query = $this->db->query($sql);

            //$rows = $query->rows;
            $new_sitemap = '';

            $rows = $query->rows;
            $output = '';
            if (count($rows) > 0) {
                $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">';
                foreach ($rows as $row) {
                    $product_id = $row['product_id'];

                    $query_key='product_id='.$product_id;

                    $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                    $query = $this->db->query($sql);

                    $row = $query->row;
                    $path_url_ua='';
                    if ($row){
                        $path_url=$store_url.'/'.$row['keyword'];
                        $path_url_ua=$store_url.'/ua/'.$row['keyword'];
                    }



                    if ($path_url!='') {
                        $new_sitemap .= '<url>';
                        $new_sitemap .= '  <loc>' . $path_url . '</loc>';//$this->url->link('product/product', 'path=' . $new_path)
                        //if ($store_id === '1') {
                        //$new_sitemap .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $path_url_ua . '" />';
                        //}
                        $new_sitemap .= '  <changefreq>weekly</changefreq>';
                        $new_sitemap .= '  <priority>0.7</priority>';
                        $new_sitemap .= '</url>';
                    }

                }
            }
            $new_sitemap .= '</urlset>';
            $name = 'sitemap_product_'. $i;

            //$this->save_file_xml($new_sitemap, 'static_sitemap', $store_id,$store_url);
            if ($this->save_file_xml($new_sitemap,$name, $store_id,$store_url)) {
                $maps_products[] = $name;
            }
            @ob_flush();
            flush();
        }



        return $maps_products;


        $sitemap_product_common = '<?xml version="1.0" encoding="UTF-8"?>
        <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($maps_products as $map) {
            $date = date('Y-m-d');
            $sitemap_product_common .= ' <sitemap>
            <loc>' . $store_url . '/sitemaps/' . $map . '.xml</loc>
            <lastmod>' . $date . '</lastmod>
           </sitemap>';
        }
        $sitemap_product_common .= '</sitemapindex>';
        //if (save_file_xml($sitemap_product_common, $name_map)) {
        if ($this->save_file_xml($sitemap_product_common,$name_map, $store_id,$store_url)) {
            return true;
        }

        return false;



        /*

                $sql = 'SELECT * FROM oc_product_index_' . $store_id;//$this->config->get('config_store_id');
                $query = $this->db->query($sql);


                $rows = $query->rows;
                $output = '';
                if (count($rows) > 0) {
                    foreach ($rows as $row) {
                        $product_id = $row['product_id'];
                        //$new_path = $product_url['keyword'];
                        $query_key='product_id='.$product_id;

                        $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                        $query = $this->db->query($sql);

                        $row = $query->row;
                        if ($row){
                            $path_url=$store_url.'/'.$row['keyword'];
                        }



                        $output .= '<url>';
                        $output .= '  <loc>' . $path_url . '</loc>';//$this->url->link('product/product', 'path=' . $new_path)
                        if ($store_id === '1') {
                            $output .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $path_url . '&lng=ua-uk" />';
                        }
                        $output .= '  <changefreq>weekly</changefreq>';
                        $output .= '  <priority>0.7</priority>';
                        $output .= '</url>';

                    }
                }

                return $output;
        */

    }



    public function generateXml()
    {
        $stores_ids=[];
        $stores_ids[0] = 'https://shlepki.com.ua';
        foreach ($stores_ids as $store_id=>$store_url) {
            $array_files_sitemaps = [
                'static_sitemap' => 'static_sitemap.xml',
                'category_sitemap' => 'category_sitemap.xml',
                'manufacturer_sitemap' => 'manufacturer_sitemap.xml',
                //'filters_sitemap' => 'filters_sitemap.xml',
                'product_sitemap' => 'product_sitemap.xml',
            ];
            $date = date('Y-m-d');

            $url_domen = $store_url;//$this->config->get('config_url');
            $sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            foreach ($array_files_sitemaps as $key => $map) {

                $date = date('Y-m-d');
                if ($generate_sotemap = $this->generate_map($key, $store_id,$store_url)) {
                    $sitemap .= ' <sitemap>
  	<loc>' . $url_domen . '/sitemaps/' . $key . '.xml</loc>
  	<lastmod>' . $date . '</lastmod>
   </sitemap>';
                }
            }


            $maps_products = $this->getProducts($store_id,$store_url);




            foreach ($maps_products as $map) {
                $date = date('Y-m-d');
                $sitemap .= ' <sitemap>
            <loc>' . $store_url . '/sitemaps/' . $map . '.xml</loc>
            <lastmod>' . $date . '</lastmod>
           </sitemap>';
            }



            $sitemap .= '</sitemapindex>';
            $this->save_file_xml($sitemap, 'sitemap', $store_id, $store_url);
        }


    }


    public function generate_map($key, $store_id,$store_url)
    {

        $date = date('Y-m-d');
        switch ($key) {
            case 'static_sitemap':
                $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">';


                $this->load->model('catalog/information');

                $informations = $this->model_catalog_information->getInformations();

                $url = $store_url;
                $new_sitemap .= '<url>
                        <loc>' . $url . '</loc>
                        <lastmod>' . $date . '</lastmod>
                        <changefreq>always</changefreq>
                       </url>';

                foreach ($informations as $information) {

                    $query_key='information_id=' . $information['information_id'];
                    $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND language_id=2 AND query='".$query_key."'";//$this->config->get('config_store_id');
                    $query = $this->db->query($sql);

                    $path_url='';
                    $row = $query->row;
                    if ($row){
                        $path_url=$store_url.'/'.$row['keyword'];
                        $path_url_ua=$store_url.'/ua/'.$row['keyword'];

                    }


                    if ($path_url!='') {
                        $new_sitemap .= '<url>';
                        $new_sitemap .= '  <loc>' . $path_url . '</loc>';//$this->url->link('information/information', 'information_id=' . $information['information_id'])
                        //if ($store_id === '1') {
                        //$new_sitemap .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $path_url_ua . '" />';
                        //}
                        $new_sitemap .= '  <changefreq>weekly</changefreq>';
                        $new_sitemap .= '  <priority>0.5</priority>';
                        $new_sitemap .= '</url>';
                    }
                }


                $new_sitemap .= '</urlset>';

                $this->save_file_xml($new_sitemap, 'static_sitemap', $store_id,$store_url);
                break;
            case 'category_sitemap':


                $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">';

                $this->load->model('catalog/category');

                $new_sitemap .= $this->getCategories(0, '', $store_id,$store_url);

                $new_sitemap .= '</urlset>';
                $this->save_file_xml($new_sitemap, 'category_sitemap', $store_id,$store_url);


                break;

            case 'manufacturer_sitemap':

                $this->load->model('catalog/manufacturer');


                $sql_man = "SELECT * FROM " . DB_PREFIX . "manufacturer m
                       LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id)
                       WHERE m2s.store_id = " . $store_id;// . "                       ";

                $query_man = $this->db->query($sql_man);
                //echo $sql_man."\r\n\r\n";

                $manufacturers = $query_man->rows;
                //$this->model_catalog_manufacturer->getManufacturers();
                $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
    http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml">';
                foreach ($manufacturers as $manufacturer) {


                    $query_key='manufacturer_id=' . $manufacturer['manufacturer_id'];
                    $sql = "SELECT * FROM oc_seo_url WHERE store_id='" . $store_id."' AND query='".$query_key."' LIMIT 1";//$this->config->get('config_store_id');

                    $query = $this->db->query($sql);

                    $row = $query->row;
                    $path_url=$path_url_ua='';
                    if ($row){
                        $path_url=$store_url.'/'.$row['keyword'];
                        $path_url_ua=$store_url.'/ua/'.$row['keyword'];
                    }


                    if ($path_url!='') {
                        $new_sitemap .= '<url>';
                        $new_sitemap .= '  <loc>' . $path_url . '</loc>';
                        //if ($store_id === '1') {
                        //$new_sitemap .= '  <xhtml:link rel="alternate" hreflang="uk-ua" href="' . $path_url_ua . '" />';
                        //}
                        $new_sitemap .= '  <changefreq>weekly</changefreq>';
                        $new_sitemap .= '  <priority>0.7</priority>';
                        $new_sitemap .= '</url>';
                    }
                }
                $new_sitemap .= '</urlset>';
                $this->save_file_xml($new_sitemap, 'manufacturer_sitemap', $store_id,$store_url);
                break;

            /* case 'product_sitemap':
                 $new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
 <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

                 $new_sitemap .= $this->getProducts($store_id,$store_url);
                 $new_sitemap .= '</urlset>';
                 $this->save_file_xml($new_sitemap, 'product_sitemap', $store_id,$store_url);

                 break;*/
        }
        return true;
    }


    protected function save_file_xml($data, $name, $store_id,$store_url)
    {


        //DIR_PATH
        $config_url = $store_url;//$this->config->get('config_url');
        $file = $name . '.xml';
        $folder = 'sitemaps';// $this->config->get('config_store_id');
        // $name = $name . $store_id;//$this->config->get('config_store_id');
        if ($name != 'sitemap') {
            if (!file_exists($folder)) {
                umask(0);
                mkdir($folder, 775);
            }
            $file = $folder . '/' . $file;

        }
        echo $name.' $file='.$file."\r\n";
        $handle = fopen($file, "w+");

        fwrite($handle, header('Content-Type: text/html; charset=utf8'));
//        echo '<pre>$data';
//        print_r($data);
//        echo '</pre>';
        if (fwrite($handle, $data)) {
            fclose($handle);
            return true;
        }
        fclose($handle);
        return false;

    }
}
