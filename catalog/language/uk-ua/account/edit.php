<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']      = 'Настройки профілю';

// Text
$_['text_account']       = 'Обліковий запис';
$_['text_settings']      = 'Настройки профілю';
$_['text_edit']          = 'Змінити інформацію';
$_['text_your_details']  = 'Ваші особисті дані';
$_['text_success']       = 'Ваш обліковий запис була успішно оновлена!';
$_['text_change_pass']   = 'Змінити пароль';

// Entry
$_['entry_firstname']    = 'Ім\'я';
$_['entry_lastname']     = 'Прізвище';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_fax']          = 'Факс';
$_['entry_old_password'] = 'Поточний пароль';
$_['entry_password']     = 'Новий пароль';
$_['entry_confirm']      = 'Підтвердження паролю';
$_['entry_birthday']     = 'Дата народження';
$_['entry_newsletter']   = 'Підписатися на розсилку новин';

// Error
$_['error_exists']       = 'Така E-Mail адреса вже зареєстрована!';
$_['error_firstname']    = 'Ім\'я повинно містити від 1 до 32 символів!';
$_['error_lastname']     = 'Прізвище повинно містити від 1 до 32 символів!';
$_['error_email']        = 'Неправильний E-Mail!';
$_['error_telephone']    = 'Телефон повинен містити від 3 до 32 символів!';
$_['error_custom_field'] = '%s необхідно!';
$_['error_password']     = 'Пароль повинен містити від 4 до 20 символів!';
$_['error_confirm']      = 'Підтвердження паролю не співпадає!';
