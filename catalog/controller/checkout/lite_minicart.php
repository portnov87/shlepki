<?php
class ControllerCheckoutLiteMinicart extends Controller
{
    private $error = array();

    public function index()
    {
        $this->language->load('checkout/lite_cart');

        if (!isset($this->session->data['vouchers'])) {
            $this->session->data['vouchers'] = array();
        }

        // Update
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        // Remove
        if (isset($this->request->get['remove'])) {
            $this->cart->remove($this->request->get['remove']);

            unset($this->session->data['vouchers'][$this->request->get['remove']]);

            $this->session->data['success'] = $this->language->get('text_remove');

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        // Coupon
        if (isset($this->request->post['coupon']) && $this->validateCoupon()) {
            $this->session->data['coupon'] = $this->request->post['coupon'];

            $this->session->data['success'] = $this->language->get('text_coupon');

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        // Voucher
        if (isset($this->request->post['voucher']) && $this->validateVoucher()) {
            $this->session->data['voucher'] = $this->request->post['voucher'];

            $this->session->data['success'] = $this->language->get('text_voucher');

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        // Reward
        if (isset($this->request->post['reward']) && $this->validateReward()) {
            $this->session->data['reward'] = abs($this->request->post['reward']);

            $this->session->data['success'] = $this->language->get('text_reward');

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        // Shipping
        if (isset($this->request->post['shipping_method']) && $this->validateShipping()) {
            $shipping = explode('.', $this->request->post['shipping_method']);

            $this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];

            $this->session->data['success'] = $this->language->get('text_shipping');

            $this->response->redirect($this->url->link('checkout/lite_cart'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => false
          );

        $data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/lite_cart'),
            'text'      => $this->language->get('heading_title'),
            'separator' => $this->language->get('text_separator')
          );

        if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
            $points = $this->customer->getRewardPoints();

            $points_total = 0;

            foreach ($this->cart->getProducts() as $product) {
                if ($product['points']) {
                    $points_total += $product['points'];
                }
            }

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_next'] = $this->language->get('text_next');
            $data['text_next_choice'] = $this->language->get('text_next_choice');
            $data['text_use_coupon'] = $this->language->get('text_use_coupon');
            $data['text_use_voucher'] = $this->language->get('text_use_voucher');
            $data['text_use_reward'] = sprintf($this->language->get('text_use_reward'), $points);
            $data['text_shipping_estimate'] = $this->language->get('text_shipping_estimate');
            $data['text_shipping_detail'] = $this->language->get('text_shipping_detail');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_select'] = $this->language->get('text_select');
            $data['text_none'] = $this->language->get('text_none');
            $data['text_until_cancelled'] = $this->language->get('text_until_cancelled');
            $data['text_freq_day'] = $this->language->get('text_freq_day');
            $data['text_freq_week'] = $this->language->get('text_freq_week');
            $data['text_freq_month'] = $this->language->get('text_freq_month');
            $data['text_freq_bi_month'] = $this->language->get('text_freq_bi_month');
            $data['text_freq_year'] = $this->language->get('text_freq_year');

            $data['column_image'] = $this->language->get('column_image');
            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['entry_coupon'] = $this->language->get('entry_coupon');
            $data['entry_voucher'] = $this->language->get('entry_voucher');
            $data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);
            $data['entry_country'] = $this->language->get('entry_country');
            $data['entry_zone'] = $this->language->get('entry_zone');
            $data['entry_postcode'] = $this->language->get('entry_postcode');

            $data['button_update'] = $this->language->get('button_update');
            $data['button_remove'] = $this->language->get('button_remove');
            $data['button_coupon'] = $this->language->get('button_coupon');
            $data['button_voucher'] = $this->language->get('button_voucher');
            $data['button_reward'] = $this->language->get('button_reward');
            $data['button_quote'] = $this->language->get('button_quote');
            $data['button_shipping'] = $this->language->get('button_shipping');
            $data['button_shopping'] = $this->language->get('button_shopping');
            $data['button_checkout'] = $this->language->get('button_checkout');

            $data['text_trial'] = $this->language->get('text_trial');
            $data['text_recurring'] = $this->language->get('text_recurring');
            $data['text_length'] = $this->language->get('text_length');
            $data['text_recurring_item'] = $this->language->get('text_recurring_item');
            $data['text_payment_profile'] = $this->language->get('text_payment_profile');

            $data['text_mini_thank_you'] = $this->language->get('text_mini_thank_you');
            $data['text_mini_your_order'] = $this->language->get('text_mini_your_order');
            $data['text_mini_product'] = $this->language->get('text_mini_product');
            $data['text_mini_number'] = $this->language->get('text_mini_number');
            $data['text_mini_price'] = $this->language->get('text_mini_price');
            $data['text_mini_shipping'] = $this->language->get('text_mini_shipping');
            $data['text_mini_savings'] = $this->language->get('text_mini_savings');
            $data['text_mini_to_pay'] = $this->language->get('text_mini_to_pay');
            $data['text_mini_if_problems'] = $this->language->get('text_mini_if_problems');
            $data['text_mini_phone1'] = $this->language->get('text_mini_phone1');
            $data['text_mini_phone2'] = $this->language->get('text_mini_phone2');

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } elseif (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $data['error_warning'] = $this->language->get('error_stock');
            } else {
                $data['error_warning'] = '';
            }

            if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
                $data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
            } else {
                $data['attention'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            $data['action'] = $this->url->link('checkout/lite_cart');

            if ($this->config->get('config_cart_weight')) {
                $data['weight'] = $this->cart->getWeight();
            } else {
                $data['weight'] = 0;
            }

            $this->load->model('tool/image');
            $this->load->model('tool/upload');
            $this->load->model('catalog/product');

            $data['products'] = array();

            $data['stock_status_error'] = [];

            $products = $this->cart->getProducts();

            $data['discount'] = 0;
            $total_amount=0;

            foreach ($products as $product) {
                $product_total = 0;

                $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                }

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'), $product_info['watermark']);
                } else {
                    $image = '';
                }

                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

            //    $filters = $this->model_catalog_product->getProductFilters($product['product_id']);
                $data['product_filters'] = array();

                $in_box = 1;
                $size = false;
                $in_box_text = '';
                $number_of_pockets_text = '';
                $dimension = false;
                $number_of_pockets = 0;



                if ($dimension && !empty($dimension)){
                  ksort($dimension);
                  $dimension = implode('x', $dimension);
                }

                // Display prices
                $exchange = (int)(CURRENCY_CODE === $product['currency_code']);
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price']*$in_box, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, true, true);
                    $price_item = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, true, true);
                } else {
                    $price = false;
                    $price_item = false;
                }

                // $data['discount'] += ($product_info['price'] - $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))) * $product['quantity'];
                // if ($data['discount']>0) {
                //     $data['discount'] = $this->currency->format($data['discount'], $this->session->data['currency']);
                //     $data['discount_present'] = true;
                // }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $total = $this->currency->format($this->tax->calculate($product['price']*$in_box, $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency'], $exchange, true, true);
                } else {
                    $total = false;
                }

                $profile_description = '';

                if ($product['recurring']) {
                    $frequencies = array(
                        'day' => $this->language->get('text_day'),
                        'week' => $this->language->get('text_week'),
                        'semi_month' => $this->language->get('text_semi_month'),
                        'month' => $this->language->get('text_month'),
                        'year' => $this->language->get('text_year'),
                    );

                    if ($product['recurring_trial']) {
                        $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        $profile_description = sprintf($this->language->get('text_trial_description'), $recurring_price, $product['recurring_trial_cycle'], $frequencies[$product['recurring_trial_frequency']], $product['recurring_trial_duration']) . ' ';
                    }

                    $recurring_price = $this->currency->format($this->tax->calculate($product['recurring_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                    if ($product['recurring_duration']) {
                        $profile_description .= sprintf($this->language->get('text_payment_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                    } else {
                        $profile_description .= sprintf($this->language->get('text_payment_until_canceled_description'), $recurring_price, $product['recurring_cycle'], $frequencies[$product['recurring_frequency']], $product['recurring_duration']);
                    }
                }

                if ($product['stock_status']){
                  $data['stock_status_error'][] = $product['product_id'];
                }



                $in_box = $this->model_catalog_product->getParInBox($product['product_id']);
                $exchange=false;

//                $exchange_price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
//                $price_item = $this->currency->format($this->tax->calculate($exchange_price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);
//
//                $price = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
//                //$price_number'] = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, false, false);
//
//                if ((float)$result['special']) {
//                    $exchange_special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
//                } else {
//                    $exchange_special = false;
//                }
//
//                if ((float)$result['special']) {
//                    $special = $this->currency->format($this->tax->calculate($exchange_special * $in_box, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
//                } else {
//                    $special = false;
//                }
//
//                if ($exchange_special)
//                {
//                    $price_item = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);
//
//                }else{
//                    $price_item = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);
//
//                }

                $quantity_text=$product['quantity'].' '.$this->plural_form($product['quantity'], ['ящик', 'ящика', 'ящиков']);

                $data['products'][] = array(
                    'cart_id'                 => $product['cart_id'],

                    'thumb'                   => $image,
                    'name'                    => $product['name'],
                    'par_in_box'=>$in_box,
                    'quantity_text'=>$quantity_text,
                    'model'                   => $product['model'],
                    'stock_status'            => $product['stock_status'],
                    'option'                  => $option_data,
                    'quantity'                => $product['quantity'],
                    'stock'                   => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                    'reward'                  => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                    'price'                   => $product['price'],//$price,
                    //'price_item'              => $price_item,
                    //'in_box'                  => $in_box,
                    //'in_box_text'             => $in_box_text,
                    'size'                    => $size,
                    //'is_bag'                  => $product['is_bag'],
                    //'number_of_pockets'       => $number_of_pockets,
                    //'number_of_pockets_text'  => $number_of_pockets_text,
                   // 'dimension'               => $dimension,
                    'total'                   => $total,
                    'href'                    => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'remove'                  => $this->url->link('checkout/lite_cart', 'remove=' . $product['product_id']),
                    'recurring'               => $product['recurring'],
                    //'profile_name'          => $product['profile_name'],
                    'profile_description'     => $profile_description,
                    'product_id'              => $product['product_id'],
                );
                $total_amount=$total_amount+$product['price'];
            }

            $total_amount = $this->currency->format($this->tax->calculate($total_amount, 1, $this->config->get('config_tax')), $this->session->data['currency'], $exchange, true, true);


//            echo '<pre>products';
//            print_r($data['products']);
//            echo '</pre>';
            $countProducts=count($data['products']);
            $data['economy'] = false;

            $data['total_amount'] = $total_amount;

            $data['products_recurring'] = array();

            $data['stock_status_error'] = (!empty($data['stock_status_error']) && count($data['stock_status_error']) !== count($products)) ? true : false;

            // Gift Voucher
            $data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $key => $voucher) {
                    $data['vouchers'][] = array(
                        'key'         => $key,
                        'description' => $voucher['description'],
                        'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
                        'remove'      => $this->url->link('checkout/lite_cart', 'remove=' . $key)
                    );
                }
            }

            if (isset($this->request->post['next'])) {
                $data['next'] = $this->request->post['next'];
            } else {
                $data['next'] = '';
            }

            $data['coupon_status'] = $this->config->get('coupon_status');

            if (isset($this->request->post['coupon'])) {
                $data['coupon'] = $this->request->post['coupon'];
            } elseif (isset($this->session->data['coupon'])) {
                $data['coupon'] = $this->session->data['coupon'];
            } else {
                $data['coupon'] = '';
            }

            $data['voucher_status'] = $this->config->get('voucher_status');

            if (isset($this->request->post['voucher'])) {
                $data['voucher'] = $this->request->post['voucher'];
            } elseif (isset($this->session->data['voucher'])) {
                $data['voucher'] = $this->session->data['voucher'];
            } else {
                $data['voucher'] = '';
            }

            $data['reward_status'] = ($points && $points_total && $this->config->get('reward_status'));

            if (isset($this->request->post['reward'])) {
                $data['reward'] = $this->request->post['reward'];
            } elseif (isset($this->session->data['reward'])) {
                $data['reward'] = $this->session->data['reward'];
            } else {
                $data['reward'] = '';
            }

            $data['shipping_status'] = $this->config->get('shipping_status') && $this->config->get('shipping_estimator') && $this->cart->hasShipping();

            if (isset($this->request->post['country_id'])) {
                $data['country_id'] = $this->request->post['country_id'];
            } elseif (isset($this->session->data['shipping_country_id'])) {
                $data['country_id'] = $this->session->data['shipping_country_id'];
            } else {
                $data['country_id'] = $this->config->get('config_country_id');
            }

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();

            if (isset($this->request->post['zone_id'])) {
                $data['zone_id'] = $this->request->post['zone_id'];
            } elseif (isset($this->session->data['shipping_zone_id'])) {
                $data['zone_id'] = $this->session->data['shipping_zone_id'];
            } else {
                $data['zone_id'] = '';
            }

            if (isset($this->request->post['postcode'])) {
                $data['postcode'] = $this->request->post['postcode'];
            } elseif (isset($this->session->data['shipping_postcode'])) {
                $data['postcode'] = $this->session->data['shipping_postcode'];
            } else {
                $data['postcode'] = '';
            }

            if (isset($this->request->post['shipping_method'])) {
                $data['shipping_method'] = $this->request->post['shipping_method'];
            } elseif (isset($this->session->data['shipping_method'])) {
                $data['shipping_method'] = $this->session->data['shipping_method']['code'];
            } else {
                $data['shipping_method'] = '';
            }

            // Totals
      			$this->load->model('setting/extension');

      			$totals = array();
      			$taxes = $this->cart->getTaxes();
      			$total = 0;

      			// Because __call can not keep var references so we put them into an array.
      			$total_data = array(
      				'totals' => &$totals,
      				'taxes'  => &$taxes,
      				'total'  => &$total
      			);

      			// Display prices
      			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
      				$sort_order = array();

      				$results = $this->model_setting_extension->getExtensions('total');

      				foreach ($results as $key => $value) {
      					$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
      				}
//      				echo '<pre>';
//      				print_r($sort_order);
//      				echo '</pre>';
//
//      				die();
      				array_multisort($sort_order, SORT_ASC, $results);

      				foreach ($results as $result) {
      					if ($this->config->get('total_' . $result['code'] . '_status')) {
      						$this->load->model('extension/total/' . $result['code']);



      						// We have to put the totals in an array so that they pass by reference.
      						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
      					}
      				}

//                    echo '<pre>'.$result['code'];
//                    print_r($result);
//                    echo '</pre>';

//                    echo '<pre>'.$result['code'];
//                    print_r($total_data);
//                    echo '</pre>';
//      				die();

      				$sort_order = array();

      				foreach ($totals as $key => $value) {
      					$sort_order[$key] = $value['sort_order'];
      				}

      				array_multisort($sort_order, SORT_ASC, $totals);
      			}

      			$data['totals'] = array();


      			foreach ($totals as $total) {
      				$data['totals'][] = array(
      					'title' => $total['title'],
      					'text'  => $this->currency->format($total['value'], $this->session->data['currency'], 1, true, true)
      				);
      			}


            $data['text_order_total'] ='Итого к оплате';//$countProducts.' '.$this->plural_form($countProducts, ['товар', 'товара', 'товаров']);
            $data['order_total'] =$countProducts.' '.$this->plural_form($countProducts, ['товар', 'товара', 'товаров']);
      			//$this->url->link('common/home');


            $data['continue'] = $this->url->link('common/home');

            $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

            $data['cart'] = $this->url->link('checkout/cart');

            $data['checkout_buttons'] = array();

            return $this->load->view('checkout/lite_minicart', $data);
        }
    }

    public function plural_form($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }



    protected function validateCoupon()
    {
        $this->load->model('checkout/coupon');

        $coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);

        if (!$coupon_info) {
            $this->error['warning'] = $this->language->get('error_coupon');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateVoucher()
    {
        $this->load->model('checkout/voucher');

        $voucher_info = $this->model_checkout_voucher->getVoucher($this->request->post['voucher']);

        if (!$voucher_info) {
            $this->error['warning'] = $this->language->get('error_voucher');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateReward()
    {
        $points = $this->customer->getRewardPoints();

        $points_total = 0;

        foreach ($this->cart->getProducts() as $product) {
            if ($product['points']) {
                $points_total += $product['points'];
            }
        }

        if (empty($this->request->post['reward'])) {
            $this->error['warning'] = $this->language->get('error_reward');
        }

        if ($this->request->post['reward'] > $points) {
            $this->error['warning'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
        }

        if ($this->request->post['reward'] > $points_total) {
            $this->error['warning'] = sprintf($this->language->get('error_maximum'), $points_total);
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateShipping()
    {
        if (!empty($this->request->post['shipping_method'])) {
            $shipping = explode('.', $this->request->post['shipping_method']);

            if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
                $this->error['warning'] = $this->language->get('error_shipping');
            }
        } else {
            $this->error['warning'] = $this->language->get('error_shipping');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function add()
    {
        $this->language->load('checkout/lite_cart');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if (isset($this->request->post['quantity'])) {
                $quantity = $this->request->post['quantity'];
            } else {
                $quantity = 1;
            }

            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            } else {
                $option = array();
            }

            if (isset($this->request->post['profile_id'])) {
                $profile_id = $this->request->post['profile_id'];
            } else {
                $profile_id = 0;
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            $profiles = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($profiles) {
                $profile_ids = array();

                foreach ($profiles as $profile) {
                    $profile_ids[] = $profile['profile_id'];
                }

                if (!in_array($profile_id, $profile_ids)) {
                    $json['error']['profile'] = $this->language->get('error_profile_required');
                }
            }

            if (!$json) {
                $this->cart->add($this->request->post['product_id'], $quantity, $option, $profile_id);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/lite_cart'));

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                // Totals
                $this->load->model('setting/extension');

                $totals = array();
                $taxes = $this->cart->getTaxes();
                $total = 0;

                // Because __call can not keep var references so we put them into an array.
                $total_data = array(
                  'totals' => &$totals,
                  'taxes'  => &$taxes,
                  'total'  => &$total
                );

                // Display prices
                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                  $sort_order = array();

                  $results = $this->model_setting_extension->getExtensions('total');

                  foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
                  }

                  array_multisort($sort_order, SORT_ASC, $results);

                  foreach ($results as $result) {
                    if ($this->config->get('total_' . $result['code'] . '_status')) {
                      $this->load->model('extension/total/' . $result['code']);

                      // We have to put the totals in an array so that they pass by reference.
                      $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
                    }
                  }

                  $sort_order = array();

                  foreach ($totals as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                  }

                  array_multisort($sort_order, SORT_ASC, $totals);
                }

                $data['totals'] = array();

                foreach ($totals as $total) {
                  $data['totals'][] = array(
                    'title' => $total['title'],
                    'text'  => $this->currency->format($total['value'], $this->session->data['currency'], '', true, true)
                  );
                }

                $json['total'] = $this->cart->countProducts() . ':' .  $this->currency->format($total, $this->session->data['currency']);
            } else {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function quote()
    {
        $this->language->load('checkout/lite_cart');

        $json = array();

        if (!$this->cart->hasProducts()) {
            $json['error']['warning'] = $this->language->get('error_product');
        }

        if (!$this->cart->hasShipping()) {
            $json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
        }

        if ($this->request->post['country_id'] == '') {
            $json['error']['country'] = $this->language->get('error_country');
        }

        if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '') {
            $json['error']['zone'] = $this->language->get('error_zone');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
            $json['error']['postcode'] = $this->language->get('error_postcode');
        }

        if (!$json) {
            $this->tax->setShippingAddress($this->request->post['country_id'], $this->request->post['zone_id']);

            // Default Shipping Address
            $this->session->data['shipping_country_id'] = $this->request->post['country_id'];
            $this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
            $this->session->data['shipping_postcode'] = $this->request->post['postcode'];

            if ($country_info) {
                $country = $country_info['name'];
                $iso_code_2 = $country_info['iso_code_2'];
                $iso_code_3 = $country_info['iso_code_3'];
                $address_format = $country_info['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $this->load->model('localisation/zone');

            $zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);

            if ($zone_info) {
                $zone = $zone_info['name'];
                $zone_code = $zone_info['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            $address_data = array(
                'firstname'      => '',
                'lastname'       => '',
                'company'        => '',
                'address_1'      => '',
                'address_2'      => '',
                'postcode'       => $this->request->post['postcode'],
                'city'           => '',
                'zone_id'        => $this->request->post['zone_id'],
                'zone'           => $zone,
                'zone_code'      => $zone_code,
                'country_id'     => $this->request->post['country_id'],
                'country'        => $country,
                'iso_code_2'     => $iso_code_2,
                'iso_code_3'     => $iso_code_3,
                'address_format' => $address_format
            );

            $quote_data = array();

            $this->load->model('setting/extension');

            $results = $this->model_setting_extension->getExtensions('shipping');

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('shipping/' . $result['code']);

                    $quote = $this->{'model_shipping_' . $result['code']}->getQuote($address_data);

                    if ($quote) {
                        $quote_data[$result['code']] = array(
                            'title'      => $quote['title'],
                            'quote'      => $quote['quote'],
                            'sort_order' => $quote['sort_order'],
                            'error'      => $quote['error']
                        );
                    }
                }
            }

            $sort_order = array();

            foreach ($quote_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $quote_data);

            $this->session->data['shipping_methods'] = $quote_data;

            if ($this->session->data['shipping_methods']) {
                $json['shipping_method'] = $this->session->data['shipping_methods'];
            } else {
                $json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function country()
    {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id'        => $country_info['country_id'],
                'name'              => $country_info['name'],
                'iso_code_2'        => $country_info['iso_code_2'],
                'iso_code_3'        => $country_info['iso_code_3'],
                'address_format'    => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status'            => $country_info['status']
            );
        }

        $this->response->setOutput(json_encode($json));
    }
}
