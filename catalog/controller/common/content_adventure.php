<?php
class ControllerCommonContentAdventure extends Controller {
    public function index($setting)
    {
        $this->load->language('common/content_adventure');

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['categories'] = array();

        $results = $this->model_catalog_category->getNewCategories();
        //$countSpecialProducts=$this->model_catalog_product->getTotalProductSpecialsPropose();
        //$data['countSpecialProducts'] = $countSpecialProducts;


//        $data['categories'][] = array(
//            'name' => $this->language->get('text_ukraine'),//$result['name'],
//            'href' =>$this->url->link('product/category', 'path=256')
//        );


        $data['categories'][] = array(
            'name' => $this->language->get('text_special'),//$result['name'],
            'href' =>$this->url->link('product/special')
        );

//        $data['categories'][] = array(
//            'name' => $this->language->get('text_latest'),//$result['name'],
//            'href' =>$this->url->link('product/category', 'path=' . $result['category_id'])
//        );
        if ($results) {

            foreach ($results as $index=>$result) {

//                if ($result['image']) {
//                    $image = $this->model_tool_image->resize($result['image'], 354, 477);
//                } else {
//                    $image = $this->model_tool_image->resize('no_image.png', 354, 477);
//                }

//                switch ($index)
//                {
//                    case 0:
//                        $width=589;
//                        $height=462;
//                        break;
//                    case 1:
//                        $width=282;
//                        $height=462;
//                        break;
//                    case 2:
//                    case 3:
//                    case 4:
//                        $width=286;
//                        $height=139;
//                        break;
//
//
//                }
               // $image = $this->model_tool_image->resize($result['image'], $width, $height);

                if ($result['category_id']=='255')
                    $url=$this->url->link('product/latest');
                else $url=$this->url->link('product/category', 'path=' . $result['category_id']);

                $data['categories'][] = array(
                    //'thumb' => $image,
                    'name' => $result['name'],
                    //'icon' => $result['icon'],
                    //'icon_class' => $result['icon_class'],
                    'href' =>$url
                );
            }
        }

        $data['categories'][] = array(
            'name' => $this->language->get('text_brand'),//$result['name'],
            'href' =>$this->url->link('product/manufacturer')
        );

//        echo '<pre>category';
//        print_r($data['categories']);
//        echo '</pre>';

        return $this->load->view('common/content_adventure', $data);
    }
}
