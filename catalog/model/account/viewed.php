<?php
class ModelAccountViewed extends Model {
  public function addProductView($product_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "customer_product_view WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");
    $this->db->query("INSERT INTO " . DB_PREFIX . "customer_product_view SET customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', date_view = NOW()");
  }

  public function deleteProductView($product_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "customer_product_view WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");
  }

  public function getProductView($data = array()) {
    $sql = "SELECT * FROM " . DB_PREFIX . "customer_product_view WHERE customer_id = '" . (int)$this->customer->getId() . "'";


      if (isset($data['sort'])) {
          $sql .= " ORDER BY " . $data['sort'] . " ";
      }

      if (isset($data['order'])) {
          $sql .= $data['order'];
      }

      if (isset($data['start']) || isset($data['limit'])) {
          if ($data['start'] < 0) {
              $data['start'] = 0;
          }

          if ($data['limit'] < 1) {
              $data['limit'] = 20;
          }

      }

     if (isset($data['start']) || isset($data['limit'])) {

        $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

     }

     $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getTotalProductView() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_product_view WHERE customer_id = '" . (int)$this->customer->getId() . "'");

    return $query->row['total'];
  }
}
