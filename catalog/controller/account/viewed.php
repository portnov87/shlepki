<?php
class ControllerAccountViewed extends Controller {
  public function index() {
    if (!$this->customer->isLogged()) {
      $this->session->data['redirect'] = $this->url->link('account/viewed', '', true);

      $this->response->redirect($this->url->link('common/home', '', true));
    }

    $this->load->language('account/viewed');

    $this->load->model('account/viewed');

    $this->load->model('account/wishlist');

    $this->load->model('catalog/product');

    //$this->load->model('catalog/filter');

    $this->load->model('tool/image');

    $this->document->setTitle($this->language->get('heading_title'));


      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/home')
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->language->get('heading_title')
      );


      $url = '';

    if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
    } else {
        $order = 'DESC';
    }

    if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
    } else {
        $page = 1;
    }

    if (isset($this->request->get['limit'])) {
        $limit = (int)$this->request->get['limit'];
    } else {
        $limit = 38;
    }

    $data['products'] = array();

    $filter_data = [
        'sort'                    => 'date_view',
        'order'                   => $order,
        'start'                   => ($page - 1) * $limit,
        'limit'                   => $limit
    ];

    $viewed_total = $this->model_account_viewed->getTotalProductView();

    $results = $this->model_account_viewed->getProductView($filter_data);

    foreach ($results as $result) {
      $product_info = $this->model_catalog_product->getProduct($result['product_id']);

      if ($product_info) {
        if ($product_info['image']) {
          $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height'), true);
        } else {
          $image = false;
        }

        if ($product_info['quantity'] <= 0) {
          $stock = $product_info['stock_status'];
        } elseif ($this->config->get('config_stock_display')) {
          $stock = $product_info['quantity'];
        } else {
          $stock = $this->language->get('text_instock');
        }

        //$filters = $this->model_catalog_product->getProductFilters($product_info['product_id']);
        $data['product_filters'] = array();

        $in_box = 1;
        $size = false;
        $dimension = false;
        $number_of_pockets = 0;
        $color = null;
        $color_code = null;

        $exchange = (int)(CURRENCY_CODE === $product_info['currency_code']);
        $exchange_price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);

        if ((float)$product_info['special']) {
          $exchange_special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
        }else{
          $exchange_special = false;
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $price = $this->currency->format($this->tax->calculate($exchange_price*$in_box, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
        } else {
          $price = false;
        }

        if ($exchange_special) {
          $special = $this->currency->format($this->tax->calculate($exchange_special*$in_box, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
          $special_percent = '-'.round((($exchange_price - $exchange_special) / $exchange_price) * 100).'%';
        } else {
          $special = false;
          $special_percent = false;
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $price_item = $this->currency->format($this->tax->calculate($exchange_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
        } else {
          $price_item = false;
        }

        if ($exchange_special) {
          $special_item = $this->currency->format($this->tax->calculate($exchange_special, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
        } else {
          $special_item = false;
        }

        $wishlist = [];
        foreach ($this->model_account_wishlist->getWishlist() as $w) {
          $wishlist[] = $w['product_id'];
        }

          $in_box = $this->model_catalog_product->getParInBox($product_info['product_id']);
          $exchange=false;

          $exchange_price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
          $price_item = $this->currency->format($this->tax->calculate($exchange_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);

          $price = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
          //$price_number'] = $this->currency->format($this->tax->calculate($exchange_price * $in_box, $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, false, false);

          if ((float)$result['special']) {
              $exchange_special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], $exchange, false, false);
          } else {
              $exchange_special = false;
          }

          if ((float)$result['special']) {
              $special = $this->currency->format($this->tax->calculate($exchange_special * $in_box, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, true);
          } else {
              $special = false;
          }

          if ($exchange_special)
          {
              $price_item = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);

          }else{
              $price_item = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'], 1, true, false);

          }




          $data['products'][] = array(
          'product_id'       => $product_info['product_id'],
          'thumb'            => $image,
          'name'             => $product_info['name'],
          'model'            => $product_info['model'],

            'par_in_box'=>$in_box,
          'stock_status'     => $product_info['stock_status'],
          'stock'            => $stock,
          'price'            => $price,
          'price_item'       => $price_item,
          'special'          => $special,

          'special_percent'  => $special_percent,
          'special_item'     => $special_item,
          'in_box'           => $in_box,
          'size'             => $size,
          'number_of_pockets'=> $number_of_pockets,
          'dimension'        => $dimension,
          'color'            => $color,
          'color_code'       => $color_code,
          'is_new'           => $product_info['is_new'],
          'wishlist'         => in_array($result['product_id'], $wishlist),
          'href'             => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
        );
      } else {
        $this->model_account_wishlist->deleteWishlist($result['product_id']);
      }
    }

      $pagination = new Pagination();
      $pagination->total = $viewed_total;
      $pagination->page = $page;
      $pagination->limit = $limit;
      $pagination->url = $this->url->link('account/viewed', $url . '&page={page}');

      $data['last_page'] = (int)ceil($viewed_total / $limit) === (int)$page;

      $data['pagination'] = $pagination->render();

      $data['next_page'] = $this->url->link('account/viewed', $url.'&page='.($page + 1));

      $data['results'] = sprintf($this->language->get('text_pagination'), ($viewed_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($viewed_total - $limit)) ? $viewed_total : ((($page - 1) * $limit) + $limit), $viewed_total, ceil($viewed_total / $limit));

      if ($limit && ceil($viewed_total / $limit) > $page) {
          $this->document->addLink($this->url->link('account/viewed', '&page='. ($page + 1)), 'next');
      }

      if ($limit && (ceil($viewed_total / $limit) >= $page) && ($page !== 1)) {
          $this->document->addLink($this->url->link('account/viewed', '&page='. ($page - 1)), 'prev');
      }

      $data['order'] = $order;
      $data['limit'] = $limit;

    $data['text_empty'] = $this->language->get('text_empty');

      $data['heading_title']=$this->language->get('heading_title');


    $data['account_nav'] = $this->load->controller('account/account_nav', ['active' => 'viewed']);

    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');

    $this->response->setOutput($this->load->view('account/viewed', $data));
  }

}
