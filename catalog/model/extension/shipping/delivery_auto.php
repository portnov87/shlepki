<?php

class ModelExtensionShippingDeliveryAuto extends Model {

    public function getCountries() {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_auto_country WHERE status = '1' ORDER BY name ASC")->rows;
    }

    public function getZonesByCountryId($country_id) {
        return  $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_auto_zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name")->rows;
    }

    public function getCitiesByZoneId($zone_id) {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_auto_city WHERE zone_id = '" . (int)$zone_id . "' AND status = '1' ORDER BY city_id")->rows;
    }

    public function getZone($zone_id) {
      $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "delivery_auto_zone WHERE zone_id = '" . (int)$zone_id . "'");

      return $query->row;
    }

    public function getCountry($country_id) {
      $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "delivery_auto_country WHERE country_id = '" . (int)$country_id . "'");

      return $query->row;
    }

    public function getQuote($address) {
        $this->load->language('extension/shipping/delivery_auto');

        if ($this->config->get('shipping_delivery_auto_status')) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('shipping_delivery_auto_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            if (!$this->config->get('shipping_delivery_auto_geo_zone_id')) {
                $status = true;
            } elseif ($query->num_rows) {
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $quote_data = array();

            $cost = 0.00;

            if ($this->config->get('shipping_delivery_auto_min_total_for_free_delivery') > $this->cart->getSubTotal()) {
                $cost = (($this->cart->getWeight() * $this->config->get('shipping_delivery_auto_delivery_price')) + $this->config->get('shipping_delivery_auto_delivery_order') + ($this->cart->getSubTotal() * $this->config->get('shipping_delivery_auto_delivery_insurance') / 100));

            }
            if (!empty($_POST['payment_method'])) {
                if ($_POST['payment_method'] == 'cash_delivery') {
                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_delivery_auto_delivery_nal') / 100) + $this->config->get('shipping_delivery_auto_delivery_nal_fixed');
                }

            } else {
                if (isset($this->session->data['payment_method']['code']) && $this->session->data['payment_method']['code'] == 'cash_delivery') {
                    $cost += ($this->cart->getSubTotal() * $this->config->get('shipping_delivery_auto_delivery_nal') / 100) + $this->config->get('shipping_delivery_auto_delivery_nal_fixed');
                }
            }

            $quote_data['delivery_auto'] = array(
                'code'          => 'delivery_auto.delivery_auto',
                'title'         => $this->language->get('text_description'),
                'cost'          => $cost,
                'tax_class_id'  => 0,
                'text'          => $this->currency->format($cost, $this->session->data['currency'])
            );

            $method_data = array(
                'code'          => 'delivery_auto',
                'title'         => $this->language->get('text_title'),
                'quote'         => $quote_data,
                'sort_order'    => $this->config->get('shipping_delivery_auto_sort_order'),
                'error'         => false
            );
        }

        return $method_data;
    }

}

?>
