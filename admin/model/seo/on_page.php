<?php
class ModelSeoOnPage extends Model {
  public function addOnPage($data) {
    $this->db->query(
      "INSERT INTO
        ".DB_PREFIX."seo_on_page
      SET
        store_id='".(int)$data['store_id']."',
        seo_url_id='".(int)$data['seo_url_id']."',
        category_id='".(int)$data['category_id']."',
        head='".$this->db->escape($data['head'])."'
    ");

    $seo_on_page_id = $this->db->getLastId();

    foreach ($data['on_page_text'] as $language_id => $value) {
      $this->db->query(
        "INSERT INTO
          ".DB_PREFIX."seo_on_page_description
        SET
          seo_on_page_id='".(int)$seo_on_page_id."',
          language_id='".(int)$language_id."',
          heading='".$this->db->escape($value['heading'])."',
          seo_text='".$this->db->escape($value['seo_text'])."',
          short_description='".$this->db->escape($value['short_description'])."',
          meta_title='".$this->db->escape($value['meta_title'])."',
          meta_description='".$this->db->escape($value['meta_description'])."'
      ");
    }
  }

  public function editOnPage($seo_on_page_id, $data) {
    $this->db->query(
      "UPDATE
        ".DB_PREFIX."seo_on_page
      SET
        store_id='".(int)$data['store_id']."',
        seo_url_id='".(int)$data['seo_url_id']."',
        category_id='".(int)$data['category_id']."',
        head='".$this->db->escape($data['head'])."'
      WHERE
        seo_on_page_id='".(int)$seo_on_page_id."'
    ");

    $this->db->query("DELETE FROM ".DB_PREFIX."seo_on_page_description WHERE seo_on_page_id='".(int)$seo_on_page_id."'");

    foreach ($data['on_page_text'] as $language_id => $value) {
      $this->db->query(
        "INSERT INTO
          ".DB_PREFIX."seo_on_page_description
        SET
          seo_on_page_id='".(int)$seo_on_page_id."',
          language_id='".(int)$language_id."',
          heading='".$this->db->escape($value['heading'])."',
          seo_text='".$this->db->escape($value['seo_text'])."',
          short_description='".$this->db->escape($value['short_description'])."',
          meta_title='".$this->db->escape($value['meta_title'])."',
          meta_description='".$this->db->escape($value['meta_description'])."'
      ");
    }
  }

  public function deleteOnPage($seo_on_page_id) {
    $this->db->query("DELETE FROM ".DB_PREFIX."seo_on_page WHERE seo_on_page_id='".(int)$seo_on_page_id."'");
  }

  public function getOnPage($seo_on_page_id) {
    $result = [];

    $query = $this->db->query(
      "SELECT * FROM
        ".DB_PREFIX."seo_on_page sp
       LEFT JOIN
        ".DB_PREFIX."seo_on_page_description spd ON(sp.seo_on_page_id = spd.seo_on_page_id)
       WHERE sp.seo_on_page_id='".(int)$seo_on_page_id."'"
    );

    foreach ($query->rows as $row) {
      $result['seo_url_id'] = $row['seo_url_id'];
      $result['category_id'] = $row['category_id'];
      $result['store_id'] = $row['store_id'];
      $result['head'] = $row['head'];
      $result['on_page_text'][$row['language_id']] = [
        'heading'          => $row['heading'],
        'seo_text'         => $row['seo_text'],
          'short_description'         => $row['short_description'],
        'meta_title'       => $row['meta_title'],
        'meta_description' => $row['meta_description'],
      ];
    }

    return $result;
  }

  public function getOnPages($data = array()) {
    $sql = "SELECT * FROM ".DB_PREFIX."seo_on_page";

    if (!is_null($data['filter_seo_url_id'])) {
      $sql .= " WHERE seo_url_id IN('".implode(',',$data['filter_seo_url_id'])."')";
    }

    $sort_data = array(
      'seo_on_page_id',
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY seo_on_page_id";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getTotal() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM ".DB_PREFIX."seo_on_page");

    return $query->row['total'];
  }
}
