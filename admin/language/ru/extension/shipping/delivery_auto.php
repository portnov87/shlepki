<?php
// Heading
$_['heading_title']    = 'Отправка Деливери';

// Text
$_['text_shipping']    = 'Доставка';
$_['text_success']     = 'Настройки модуля обновлены!';

// Entry
$_['entry_geo_zone']   = 'Географическая зона:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Порядок сортировки:';
$_['entry_api_key'] = 'Api 2 key:';

$_['button_update'] = 'Обновить базу отделений';
$_['button_check_update'] = 'Проверить статус обновления';
$_['text_update_success'] = 'Обновление прошло успешно!';
$_['text_update_error'] = 'Во время обновления что то пошло не так, повторите попытку поже.';
$_['text_api_key_error'] = 'Нужно указать APi key в настройках плагина доставки "Отправка Новой Почтой"';

$_['entry_min_total_for_free_delivery'] = 'Минимальная сумма заказа для бесплатной доставки:<br /><span class="help">Если сумма заказа больше или равна указаной сумме, тогда стоимость доставки для клиента будет равна нулю.</span>';
$_['entry_delivery_order'] = 'Стоимость оформления посылки:<br /><span class="help">Обязательный фиксированый платеж при оформлении.</span>';
$_['entry_delivery_price'] = 'Тариф доставки:<br /><span class="help">Стоимость за 1 кг. Например, 1.1</span>';
$_['entry_delivery_insurance'] = 'Стоимость страховки:<br /><span class="help">Процент от суммы заказа.</span>';
$_['entry_delivery_nal'] = 'Комиссия за наложенный платеж:<br /><span class="help">Процент от суммы заказа.</span>';
$_['entry_delivery_nal_fixed'] = 'Комиссия за наложенный платеж:<br /><span class="help">Фиксированная сумма.</span>';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
?>
