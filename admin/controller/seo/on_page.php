<?php
class ControllerSeoOnPage extends Controller {
  private $error = array();
  private $stores = [];

  public function __construct($params) {
    parent::__construct($params);
    $this->load->language('seo/on_page');
    $this->load->model('setting/store');
    $this->load->model('design/seo_url');
    $this->stores[0] = $this->config->get('config_name');

    $stores = $this->model_setting_store->getStores();

    foreach ($stores as $store) {
      $this->stores[$store['store_id']] = $store['name'];
    }
  }

  public function index() {
    $this->load->language('seo/on_page');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/on_page');
    $this->load->model('catalog/category');

    $this->getList();
  }

  public function add() {
    $this->load->language('seo/on_page');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/on_page');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      $this->model_seo_on_page->addOnPage($this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function edit() {
    $this->load->language('seo/on_page');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/on_page');
    $this->load->model('catalog/category');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      $this->model_seo_on_page->editOnPage($this->request->get['seo_on_page_id'], $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }

  public function delete() {
    $this->load->language('seo/on_page');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('seo/on_page');

    if (isset($this->request->post['selected']) && $this->validateDelete()) {
      foreach ($this->request->post['selected'] as $seo_on_page_id) {
        $this->model_seo_on_page->deleteOnPage($seo_on_page_id);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getList();
  }

  protected function getList() {
    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'seo_url_id';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    if (isset($this->request->get['filter_keyword'])) {
      $filter_keyword = $this->request->get['filter_keyword'];
      $get_seo_url = $this->model_design_seo_url->getSeoUrls([
        'filter_keyword' => '%'.$filter_keyword.'%',
        'filter_language_id' => $this->config->get('config_language_id')
      ]);
      $filter_seo_url_id = [];
      foreach ($get_seo_url as $seo_url) {
        $filter_seo_url_id[] = $seo_url['seo_url_id'];
      }
    } else {
      $filter_keyword = '';
      $filter_seo_url_id = null;
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    if (isset($this->request->get['filter_keyword'])) {
      $url .= '&filter_keyword=' . $this->request->get['filter_keyword'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . $url, true)
    );

    $data['add'] = $this->url->link('seo/on_page/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
    $data['delete'] = $this->url->link('seo/on_page/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

    $data['filter_keyword'] = $filter_keyword;

    $data['on_pages'] = array();

    $filter_data = array(
      'sort'                => $sort,
      'order'               => $order,
      'filter_seo_url_id'   => $filter_seo_url_id,
      'start'               => ($page - 1) * $this->config->get('config_limit_admin'),
      'limit'               => $this->config->get('config_limit_admin')
    );

    $on_page_total = $this->model_seo_on_page->getTotal();

    $results = $this->model_seo_on_page->getOnPages($filter_data);

    foreach ($results as $result) {
      $category_info = $this->model_catalog_category->getCategory($result['category_id']); 
      
      $seo_url_info = $this->model_design_seo_url->getSeoUrl($result['seo_url_id']);

      $seo_url_text = isset($seo_url_info['keyword']) 
        ? (!empty($category_info)) 
          ? $category_info['name'].' => '.$seo_url_info['keyword'] 
          : $seo_url_info['keyword']
        : '-';

      $data['on_pages'][] = array(
        'seo_on_page_id' => $result['seo_on_page_id'],
        'seo_url'        => $seo_url_text,
        'store'          => $this->stores[$result['store_id']],
        'edit'           => $this->url->link('seo/on_page/edit', 'user_token='.$this->session->data['user_token'].'&seo_on_page_id='.$result['seo_on_page_id'].$url, true)
      );
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    if (isset($this->request->post['selected'])) {
      $data['selected'] = (array)$this->request->post['selected'];
    } else {
      $data['selected'] = array();
    }

    $url = '';

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_store'] = $this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . '&sort=store' . $url, true);
    $data['sort_seo_url_id'] = $this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . '&sort=seo_url_id' . $url, true);

    $data['user_token'] = $this->session->data['user_token'];

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $on_page_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('seo/on_page', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

    $data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($on_page_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($on_page_total - $this->config->get('config_limit_admin'))) ? $on_page_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $on_page_total, ceil($on_page_total / $this->config->get('config_limit_admin')));

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('seo/on_page_list', $data));
  }

  protected function getForm() {
    $data['text_form'] = !isset($this->request->get['seo_url_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['seo_url'])) {
      $data['error_seo_url'] = $this->error['seo_url'];
    } else {
      $data['error_seo_url'] = '';
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['user_token'] = $this->session->data['user_token'];

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], true)
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('seo/on_page', 'user_token='.$this->session->data['user_token'].$url, true)
    );

    if (!isset($this->request->get['seo_on_page_id'])) {
      $data['action'] = $this->url->link('seo/on_page/add', 'user_token='.$this->session->data['user_token'].$url, true);
    } else {
      $data['action'] = $this->url->link('seo/on_page/edit', 'user_token='.$this->session->data['user_token'].'&seo_on_page_id='.$this->request->get['seo_on_page_id'].$url, true);
    }

    $data['cancel'] = $this->url->link('seo/on_page', 'user_token='.$this->session->data['user_token'].$url, true);

    if (isset($this->request->get['seo_on_page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $on_page_info = $this->model_seo_on_page->getOnPage($this->request->get['seo_on_page_id']);
    }else{
      $on_page_info = [];
    }

    if (isset($this->request->post['seo_url'])) {
      $data['seo_url'] = $this->request->post['seo_url'];
    } elseif (!empty($on_page_info)) {
      $seo_url_info = $this->model_design_seo_url->getSeoUrl($on_page_info['seo_url_id']);
      $category_info = $this->model_catalog_category->getCategory($on_page_info['category_id']);
      if (!empty($seo_url_info)){
        $category_text = (!empty($category_info)) ? $category_info['name'].' => ' : '';
        $data['seo_url'] = $category_text.$seo_url_info['keyword']." [{$this->stores[$seo_url_info['store_id']]}]";
      }else{
        $data['seo_url'] = '';
      }
    } else {
      $data['seo_url'] = '';
    }

    if (isset($this->request->post['seo_url_id'])) {
      $data['seo_url_id'] = $this->request->post['seo_url_id'];
    } elseif (!empty($on_page_info)) {
      $data['seo_url_id'] = $on_page_info['seo_url_id'];
    } else {
      $data['seo_url_id'] = '';
    }

    if (isset($this->request->post['category_id'])) {
      $data['category_id'] = $this->request->post['category_id'];
    } elseif (!empty($on_page_info)) {
      $data['category_id'] = $on_page_info['category_id'];
    } else {
      $data['category_id'] = '0';
    }

    if (isset($this->request->post['store_id'])) {
      $data['store_id'] = $this->request->post['store_id'];
    } elseif (!empty($on_page_info)) {
      $data['store_id'] = $on_page_info['store_id'];
    } else {
      $data['store_id'] = '0';
    }

    if (isset($this->request->post['on_page_text'])) {
      $data['on_page_text'] = $this->request->post['on_page_text'];
    } elseif (!empty($on_page_info)) {
      $data['on_page_text'] = $on_page_info['on_page_text'];
    } else {
      $data['on_page_text'] = [];
    }

    if (isset($this->request->post['head'])) {
      $data['head'] = $this->request->post['head'];
    } elseif (!empty($on_page_info)) {
      $data['head'] = $on_page_info['head'];
    } else {
      $data['head'] = '';
    }

    $this->load->model('localisation/language');

    $data['languages'] = $this->model_localisation_language->getLanguages();

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('seo/on_page_form', $data));
  }

  protected function validateForm() {
    if (!$this->user->hasPermission('modify', 'seo/on_page')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if (empty(trim($this->request->post['seo_url']))) {
      $this->error['seo_url'] = $this->language->get('error_seo_url');
    }

    return !$this->error;
  }

  protected function validateDelete() {
    if (!$this->user->hasPermission('modify', 'seo/on_page')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }
}
