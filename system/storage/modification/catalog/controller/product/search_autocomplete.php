<?php
class ControllerProductSearchAutocomplete extends Controller {
    public function index() {
        $this->load->language('product/search');

        $this->load->model('catalog/product');
	
				$this->load->model('tool/image');

        if (isset($this->request->get['search'])) {
    			$search = $this->request->get['search'];
    		} else {
    			$search = '';
    		}

    		if (isset($this->request->get['tag'])) {
    			$tag = $this->request->get['tag'];
    		} elseif (isset($this->request->get['search'])) {
    			$tag = $this->request->get['search'];
    		} else {
    			$tag = '';
    		}

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } elseif(isset($this->request->post['page'])) {
            $page = $this->request->post['page'];
        } else {
            $page = 1;
        }

//        if (isset($this->request->get['limit'])) {
//            $limit = (int)$this->request->get['limit'];
//        } else {
//            $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
//        }

        $limit=6;

        $url = '';

        $data['products'] = array();

        if ($search) {
            $filter_data = array(
              'filter_name'         => $search,
              'filter_tag'          => $tag,
              'sort'                => $sort,
              'order'               => $order,
              'start'               => ($page - 1) * $limit,
              'limit'               => $limit
            );

            $results = $this->model_catalog_product->getProductsSphinx($filter_data,true);


            foreach ($results as $result) {

                if (($result['product_id']!='')&&($result['visibility']=='1')) {
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    $data['products'][] = array(
                        'product_id' => $result['product_id'],
                        'name' => $result['name'],
                        'image' => $this->model_tool_image->resize($result['image'], 80, 80, $result['watermark']),
                        'model' => $result['model'],
                        'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'tax' => $tax,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }
            }

        }

        $data['showMore']='/search?search='.$search;
        $this->response->setOutput($this->load->view('product/search_autocomplete', $data));
    }

}
