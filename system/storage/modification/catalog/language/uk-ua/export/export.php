<?php

$_['column_order_id']         = '№ заказа';
$_['column_count']            = '№ п/п';
$_['column_date_added']       = 'Дата';
$_['column_customer']         = 'Покупець';
$_['column_telephone']        = 'Телефон';
$_['column_total']            = 'Разом';
$_['column_goods']            = 'Товар';
$_['column_quantity']         = 'Kіл. ящ. (од.)';
$_['column_in_box']           = 'Пар в ящику';
$_['column_size']             = 'Розмір';
$_['column_item_price']       = 'Ціна за пару';
$_['column_price']            = 'Ціна за ящ.';
$_['column_cost_price']       = 'Ціна закупівлі';
$_['column_name']             = 'Найменування';
$_['column_sum']              = 'Сума';
$_['column_image']            = 'Модель';
$_['column_brand']            = 'Бренд';
$_['column_category']         = 'Тип';
$_['column_product_id']       = '№';
$_['column_product_name']     = 'Назва товару';
$_['column_product_model']    = 'Артикул';
$_['column_product_price']    = 'Ціна продажу';
$_['column_filter_id']        = '№';
$_['column_name']             = 'Назва';
$_['column_firstname']        = 'Клієнт';
$_['column_email']            = 'Email';
$_['column_payment_method']   = 'Спосіб оплати';
$_['column_shipping_method']  = 'Спосіб доставки';
$_['column_order_date_added'] = 'Дата створення';
$_['column_order_status']     = 'Статус замовлення';
$_['column_comment']          = 'Коментар';
$_['column_reduction']        = 'Знижка';

// Error
$_['error_permission']        = 'У Вас немає прав для управління даними модулем!';
$_['error_order']             = 'Помилка експорту!';

// Texts
$_['text_order']              = 'Деталі замовлення';
$_['text_empty_manufacturer'] = 'Без імені';
