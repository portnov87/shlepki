<?php
// Heading
$_['heading_title']       = 'Evercart';

// Text
$_['text_module']         = 'Модулі';
$_['text_success']        = 'Налаштування модуля оновлені!';

// Entry
$_['entry_input_name']    = 'Им\'я поля (латиниця):';
$_['entry_input_caption'] = 'Назва поля:';
$_['entry_layout']        = 'Макет:';
$_['entry_position']      = 'Расположение:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортцвання:';
$_['entry_additional_fields'] = 'Додаткові поля';

$_['button_add']          = 'Додати';

// Error
$_['error_permission']    = 'У Вас немає прав для керування цим модулем!';
?>
