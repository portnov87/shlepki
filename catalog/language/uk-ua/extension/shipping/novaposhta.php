<?php
// Text
$_['text_title']       = 'Нова Пошта';
$_['text_description'] = 'Нова Пошта';
$_['text_district'] = 'Регіон:';
$_['text_select_district'] = 'Виберіть область ...';
$_['text_city'] = 'Місто:';
$_['text_delivery_type'] = 'Тип доставки:';
$_['text_delivery_department'] = 'Доставка на відділення';
$_['text_delivery_address'] = 'Доставка на адресу';
$_['text_office'] = 'Відділення:';
$_['text_address'] = 'Адреса:';
$_['text_order_comment'] = 'Коментар до замовлення:';
?>