<?php
// Text
$_['text_items']     = 'Товаров %s (%s)';
$_['text_empty']     = 'Ваша корзина пуста!';
$_['text_cart']      = 'Перейти в корзину';
$_['text_checkout']  = 'Оформить заказ';
$_['text_recurring'] = 'Платежный профиль';

$_['text_wishlist'] = 'Избранное';

$_['text_shopping_cart'] = 'Корзина';
$_['text_empty_desc']    = 'Но это легко исправить :)';