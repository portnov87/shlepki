<?php

class ControllerStartupSeoUrl extends Controller
{
    public function index()
    {
        // Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);

            // OCFilter start
            if ($this->registry->has('ocfilter')) {
                $this->url->addRewrite($this->ocfilter);
            }
            // OCFilter end

        }


//        $this->request->get['route'] = 'product/manufacturer/info';
//        $this->request->get['manufacturer_id'] = 217;

        //$this->request->get['route'] = 'product/manu';


        // Decode URL
        if (isset($this->request->get['_route_'])) {
            $parts = explode('/', $this->request->get['_route_']);

            if (isset($parts[0]) && $parts[0] === 'sitemap.xml') {
                die($this->load->controller('extension/feed/google_sitemap/view_xml'));
            }
            // remove any empty arrays from trailing
            if (utf8_strlen(end($parts)) == 0) {
                array_pop($parts);
            }

            $product_page = false;
            if (isset($parts[0]) && $parts[0] === 'product') {
                $parts = [$parts[1]];
                $product_page = true;
            }
            $redirect_seo_url_id = false;

            $_paths = [];

            if (isset($parts[0]))
            {

            }
            foreach ($parts as $part) {

                $redirect_seo_url_id = false;
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                keyword = '" . $this->db->escape($part) . "' 
                AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

                $redirect_seo_url_id = $query->row['seo_url_id'];

                $this->request->data['seo_url_id'] = $redirect_seo_url_id;
                if ($product_page) {
                    $query_p = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $this->db->escape($part) . "' 
                    AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                    AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'");

                    $rows = $query_p->rows;
                    // $redirect_seo_url_id=false;
                    foreach ($rows as $row) {
                        $prod_id = false;
                        $url = explode('=', $row['query']);

                        if ($url[0] == 'product_id') {
                            $prod_id = $url[1];
                        }


                        if ($prod_id) {
                            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE status='1' 
                            AND product_id = '" . $prod_id . "'");
                            if ($query->row) {
                                //if ($query->row['status']==1){
                                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query='product_id=" . $prod_id . "' 
                                AND keyword = '" . $this->db->escape($part) . "' 
                                AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'");

                                $redirect_seo_url_id = $query->row['seo_url_id'];
                                break;
                                //}
                            }
                        }

                    }
                } else {

                    $sql="SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                    keyword = '" . $this->db->escape($part) . "' 
                    AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                    AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'";
                    $query = $this->db->query($sql);

                    //echo $sql;die();

                    $redirect_seo_url_id = $query->row['seo_url_id'];
                    $url = explode('=', $query->row['query']);

                    if (count($parts)==1) {
                        if ($url[0] == 'category_id') {
                            $catId = $url[1];

                            $_sql = "SELECT * FROM " . DB_PREFIX . "category WHERE 
                            category_id = '" . $catId . "' 
                            ";
                            $_query = $this->db->query($_sql);
                            $parent_id = $_query->row['parent_id'];
                            if ($parent_id > 0){
                                $this->request->get['route'] = 'error/not_found';
                                break;
                            }
                        }
                    }
                }


                if ($query->num_rows) {
                    $rows = $query->rows;

                    $url = explode('=', $query->row['query']);

                    if ($url[0] == 'product_id') {
                        $this->request->get['product_id'] = $url[1];
                    }


                    if ($url[0] == 'simple_blog_article_id') {
                        $this->request->get['simple_blog_article_id'] = $url[1];
                    }
                    if ($url[0] == 'simple_blog_author_id') {
                        $this->request->get['simple_blog_author_id'] = $url[1];
                    }
                    if ($url[0] == 'simple_blog_category_id') {
                        if (!isset($this->request->get['simple_blog_category_id'])) {
                            $this->request->get['simple_blog_category_id'] = $url[1];
                        } else {
                            $this->request->get['simple_blog_category_id'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'category_id') {
                        if (!isset($this->request->get['path'])) {
                            $this->request->get['path'] = $url[1];
                            $_paths[] = $url[1];
                        } else {

                            if (count($rows) > 1) {

                                $_par_category_id = $_paths[0];
                                foreach ($rows as $_r) {
                                    $_q = $_r['query'];
                                    $ex_cat = explode('=', $_q);
                                    $_category_id = $ex_cat[1];

                                    $_cat_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE 
                                    category_id = '" . (int)$_category_id . "' AND parent_id='" . (int)$_par_category_id . "' ");

                                    if (count($_cat_query->rows) > 0) {
                                        $_paths[] = $url[1];
                                        $this->request->get['path'] .= '_' . $_category_id;
                                    }
                                }

                                if (count($_paths)==1)
                                {
                                    $_paths[] = $url[1];
                                    $this->request->get['path'] .= '_' . $url[1];
                                }

                            } else {
                                $_paths[] = $url[1];
                                $this->request->get['path'] .= '_' . $url[1];
                            }
                        }
                    }


                    if ($url[0] == 'manufacturer_id') {
                        $this->request->get['manufacturer_id'] = $url[1];
                        $this->request->get['route'] = 'product/manufacturer/info';
                    }

                    if ($url[0] == 'information_id') {
                        $this->request->get['information_id'] = $url[1];
                    }

                    if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id' && $url[0] != 'simple_blog_article_id' && $url[0] != 'simple_blog_author_id' && $url[0] != 'simple_blog_category_id') {
                        $this->request->get['route'] = $query->row['query'];
                    }
                    if ($query->row['query'] &&
                        $url[0] != 'information_id' &&
                        $url[0] != 'manufacturer_id' &&
                        $url[0] != 'category_id' &&
                        $url[0] != 'product_id'
                        //$url[0] != '_size_' &&
                        //$url[0] != '_stock_' &&
                        //$url[0] != 'selection_size' &&
                        //$url[0] != 'filter_id'
                    ) {

                        $this->request->get['route'] = $query->row['query'];
                    }elseif($url[0] == 'manufacturer_id'){
//                        echo 'route = '.$this->request->get['route'];
//                        die();

                        //echo $this->request->get['route'];die();
                            //=
                        //unset($this->request->get['route']);
                        break;
                    }
                } else {

                    if (($this->config->has('simple_blog_seo_keyword')) && ($this->db->escape($part) == $this->config->get('theme_' . $this->config->get('config_theme') . '_simple_blog_seo_keyword'))) {
                    } else if ($this->db->escape($part) == 'simple-blog') {
                    } else {
                        if ($parts[0]!='search')
                        {
                            $this->request->get['route'] = 'error/not_found';
                            //echo 'test';die();
                            break;
                        }
                        //
                    }

                }
            }


            if (!isset($this->request->get['route'])) {
                if (isset($this->request->get['product_id'])) {
                    $this->request->get['route'] = 'product/product';
                } elseif (isset($this->request->get['path'])) {
                    $this->request->get['route'] = 'product/category';
                } elseif (isset($this->request->get['manufacturer_id'])){//&&(!isset($this->request->get['type']))) {
                    $this->request->get['route'] = 'product/manufacturer/info';
                } elseif (isset($this->request->get['information_id'])) {

                    $this->request->get['route'] = 'information/information';
                } else if (isset($this->request->get['simple_blog_article_id'])) {
                    $this->request->get['route'] = 'simple_blog/article/view';
                } else if (isset($this->request->get['simple_blog_author_id'])) {
                    $this->request->get['route'] = 'simple_blog/author';
                } else if (isset($this->request->get['simple_blog_category_id'])) {
                    $this->request->get['route'] = 'simple_blog/category';
                } else {
                    if (($this->config->has('simple_blog_seo_keyword'))) {
                        if ($this->request->get['_route_'] == $this->config->get('theme_' . $this->config->get('config_theme') . '_simple_blog_seo_keyword')) {
                            $this->request->get['route'] = 'simple_blog/article';
                        }
                    }
                    if ($this->request->get['_route_'] == 'simple-blog') {
                        $this->request->get['route'] = 'simple_blog/article';
                    }

                }
            }
        } elseif (isset($this->request->get['route'])) {
            //if ((isset($this->request->get['manufacturer_id']))&&(!isset($this->request->get['type']))) {
//            echo 'route = '.$this->request->get['route'];
//            die();
            if (isset($this->request->get['manufacturer_id'])){
                $manufacturer_id = $this->request->get['manufacturer_id'];
//                echo '$manufacturer_id'.$manufacturer_id;
//                die();

                $query_m = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'manufacturer_id=" . $manufacturer_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'");

                if ($query_m->num_rows) {
                    $row_man = $query_m->row;
                    $url_key = $query_m->row['keyword'];
                    if ($url_key != '') {
                        if (!isset($this->request->get['type'])) {

                            if (!isset($this->request->get['page'])) {


                                header('Location: /' . $url_key, true, 301);
                            } elseif ($this->request->get['page'] == 1) {
                                header('Location: /' . $url_key, true, 301);
                            }
                        }
                    }

                }
            }
            elseif(isset($this->request->get['information_id'])){

                $information_id=$this->request->get['information_id'];
                $query_m = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'information_id=" . $information_id . "' 
                AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'");

                if ($query_m->num_rows) {
                    $row_man = $query_m->row;
                    $url_key = $query_m->row['keyword'];
                    if ($url_key != '') {
                        if (!isset($this->request->get['type'])) {
                            if (!isset($this->request->get['page'])) {
                                header('Location: /' . $url_key, true, 301);
                            } elseif ($this->request->get['page'] == 1) {
                                header('Location: /' . $url_key, true, 301);
                            }
                        }
                    }

                }
            }

            if ($this->request->get['route']=='information/contact')
            {

                $query_m = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'information/contact' 
                AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                AND language_id = '" . (int)$this->config->get('config_default_language_id') . "'");

                if ($query_m->num_rows) {
                    $row_man = $query_m->row;
                    $url_key = $query_m->row['keyword'];
                    if ($url_key != '') {
                        if (!isset($this->request->get['type'])) {
                            if (!isset($this->request->get['page'])) {
                                header('Location: /' . $url_key, true, 301);
                            } elseif ($this->request->get['page'] == 1) {
                                header('Location: /' . $url_key, true, 301);
                            }
                        }
                    }

                }
                //header('Location: /contact', true, 301);
            }

        }
        //else {echo 'tests';die();}

    }

    public function rewrite($link)
    {
        $this->load->model('catalog/category');
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';

        $data = array();
//        if (isset($data['lng'])) {
//            $data_lng = $data['lng'];
//            unset($data['lng']);
//        } else {
//            $data_lng = null;
//        }

        parse_str($url_info['query'], $data);

        $route = isset($data['route']) ? $data['route'] : false;

        if (isset($data['lng'])) {
            $data_lng = $data['lng'];
            unset($data['lng']);
        } else {
            $data_lng = null;
        }



        foreach ($data as $key => $value) {
            if (isset($data['route'])) {
                if ((strtolower($data['route']) == 'product/category') && $key == 'path') {
                    $path = $value;
                    $_path = explode('_', $path);

                    foreach ($_path as $_p) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape('category_id=' . (int)$_p) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                       // echo "q = SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape('category_id=' . (int)$_p) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'";



                        if ($query->num_rows == 0) {
                            $category = $this->model_catalog_category->getCategory($_p);
                            if ($category) {
                                $name = $category['name'];
                                $keyword = $this->str2url($name);
                                $keyword = str_replace(' ', '-', $keyword);
                                $keyword = strtolower($keyword);
                                if ($keyword != '') {
                                    $category_id = $_p;
                                    $languages = [2, 4];
                                    foreach ($languages as $language_id) {

                                        $_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                                        `query` = '" . $this->db->escape('category_id=' . (int)$_p) . "' 
                                        AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                        AND language_id = '" . (int)(int)$language_id . "'");

                                        if ($_query->num_rows == 0) {
                                            $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET 
                                            store_id = '" . (int)$this->config->get('config_store_id') . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            query = 'category_id=" . (int)$category_id . "', 
                                            keyword = '" . $this->db->escape($keyword) . "'");
                                        }
                                    }
                                }
                            }
                            $url .= $query->row['keyword'];//'/' .
                            unset($data[$key]);
                        }
                    }


                    //die();

                } elseif ((strtolower($data['route']) == 'product/manufacturer/info') && $key == 'manufacturer_id') {


                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                        `query` = '" . $this->db->escape('manufacturer_id=' . (int)$value) . "' 
                        AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                        AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                    if ($query->num_rows == 0) {
                        $manufacturer = $this->model_catalog_manufacturer->getManufacturer($value);
                        if ($manufacturer) {
                            $name = $manufacturer['name'];
                            $keyword = $this->str2url($name);
                            $keyword = str_replace(' ', '-', $keyword);
                            $keyword = strtolower($keyword);
                            if ($keyword != '') {
                                $category_id = $_p;
                                $languages = [2, 4];
                                foreach ($languages as $language_id) {

                                    $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET 
                                            store_id = '" . (int)$this->config->get('config_store_id') . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            query = 'manufacturer_id=" . (int)$value . "', 
                                            keyword = '" . $this->db->escape($keyword) . "'");
                                }
                            }
                        }
                        $url .=  $query->row['keyword'];//'/' .
                        unset($data[$key]);
                    }

                    $path = $value;
                    $_path = explode('_', $path);
                    foreach ($_path as $_p) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape('category_id=' . (int)$_p) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
                        if ($query->num_rows == 0) {
                            $category = $this->model_catalog_category->getCategory($_p);
                            if ($category) {
                                $name = $category['name'];
                                $keyword = $this->str2url($name);
                                $keyword = str_replace(' ', '-', $keyword);
                                $keyword = strtolower($keyword);
                                if ($keyword != '') {
                                    $category_id = $_p;
                                    $languages = [2, 4];
                                    foreach ($languages as $language_id) {

                                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET 
                                            store_id = '" . (int)$this->config->get('config_store_id') . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            query = 'category_id=" . (int)$category_id . "', 
                                            keyword = '" . $this->db->escape($keyword) . "'");
                                    }
                                }
                            }
                            $url .= $query->row['keyword'];//'/' .
                            unset($data[$key]);
                        }
                    }


                }
                elseif((strtolower($data['route']) == 'product/product') && $key == 'product_id')
                {
                        $productId=$data[$key];
                        $q="SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                        `query` = '" . $this->db->escape('product_id=' . (int)$productId) . "' 
                        AND store_id = '" . (int)$this->config->get('config_store_id') . "'
                        AND language_id = '" . (int)$this->config->get('config_language_id') . "' 
                        ";
                        //
//echo $productId.' '.$q;
                        $query = $this->db->query($q);

                        //
                            $product = $this->model_catalog_product->getProduct($productId);
                            if ($product) {
                                $name = $product['model'];
                                $keyword = $this->str2url($name);
                                $keyword = str_replace(' ', '-', $keyword);
                                $keyword = strtolower($keyword);
                                //echo '$keyword $productId = '.$productId.' '.$keyword;
                                if ($keyword != '') {

                                    $languages = [2, 4];
                                    foreach ($languages as $language_id) {

                                        $sqlSel="SELECT * FROM " . DB_PREFIX . "seo_url WHERE 
                                        `query` = '" . $this->db->escape('product_id=' . (int)$productId) . "' 
                                        AND store_id = '" . (int)$this->config->get('config_store_id') . "' 
                                        AND language_id = '" . (int)$language_id . "'";
                                        $_query = $this->db->query($sqlSel);
                                        //echo '$productId '.$productId. ' $sqlSel= '.$sqlSel."<br/><br/>\r\n\r\n";


                                        $sqlUpdate="UPDATE " . DB_PREFIX . "seo_url SET                                                                                            
                                            keyword = '" . $this->db->escape($keyword) . "' 
                                            WHERE 
                                            query = 'product_id=" . (int)$productId . "' 
                                            AND store_id = '" . (int)$this->config->get('config_store_id') . "'
                                            AND language_id = '" . (int)$language_id . "'";

                                        $this->db->query($sqlUpdate);


                                        if ($_query->num_rows == 0) {
                                            $sqlInsert="INSERT INTO " . DB_PREFIX . "seo_url SET 
                                            store_id = '" . (int)$this->config->get('config_store_id') . "', 
                                            language_id = '" . (int)$language_id . "', 
                                            query = 'product_id=" . (int)$productId . "', 
                                            keyword = '" . $this->db->escape($keyword) . "'";
                                            //echo '$productId '.$productId. ' '.$sqlInsert."<br/>\r\n\r\n";
                                            $this->db->query($sqlInsert);
                                        }
                                    }
                                }
                            }
                        if ($query->num_rows == 0) {
                            $url .= $query->row['keyword'];//'/' .
                            unset($data[$key]);
                        }
                }


                if ((strtolower($data['route']) == 'product/product' && $key == 'product_id') || ((strtolower($data['route']) == 'product/manufacturer/info' || strtolower($data['route']) == 'product/manufacturer' || strtolower($data['route']) == 'product/latest' || strtolower($data['route']) == 'product/special' || strtolower($data['route']) == 'product/product') && $key == 'manufacturer_id') || (strtolower($data['route']) == 'information/information' && $key == 'information_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . $query->row['keyword'];

                        unset($data[$key]);
                    }

                } else if (strtolower($data['route']) == 'simple_blog/article/view' && $key == 'simple_blog_article_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
                    if ($query->num_rows) {
                        $url .= '/' . $query->row['keyword'];
                        unset($data[$key]);
                    } else {
                        $url .= '/simple-blog/' . (int)$value;
                        unset($data[$key]);
                    }
                } else if (strtolower($data['route']) == 'simple_blog/author' && $key == 'simple_blog_author_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
                    if ($query->num_rows) {
                        $url .= '/' . $query->row['keyword'];
                        unset($data[$key]);
                    } else {
                        $url .= '/simple-blog/' . (int)$value;
                        unset($data[$key]);
                    }
                } else if (strtolower($data['route']) == 'simple_blog/category' && $key == 'simple_blog_category_id') {
                    $blog_categories = explode("_", $value);
                    foreach ($blog_categories as $blog_category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'simple_blog_category_id=" . (int)$blog_category . "'");
                        if ($query->num_rows) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url .= '/simple-category' . $blog_category;
                        }
                    }
                    unset($data[$key]);
                } else if (strtolower($data['route']) == 'simple_blog/search') {
                    //echo $data['route'];
                    if (isset($key) && ($key == 'blog_search')) {
                        $url .= '/search&blog_search=' . $value;
                    } else {
                        $url .= '/search';
                    }
                    //echo $url;
                } else if (isset($data['route']) && strtolower($data['route']) == 'simple_blog/article' && $key != 'simple_blog_article_id' && $key != 'simple_blog_author_id' && $key != 'simple_blog_category_id' && $key != 'page') {
                    if ($this->config->has('simple_blog_seo_keyword')) {
                        $url .= '/' . $this->config->get('theme_' . $this->config->get('config_theme') . '_simple_blog_seo_keyword');
                    } else {
                        $url .= '/simple-blog';
                    }
                } elseif ($key == 'path') {

                    $categories = explode('_', $value);

                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'category_id=" . (int)$category . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';

                            break;
                        }
                    }

                    unset($data[$key]);
                } else {
                    $sql = "SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($value) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'";
                    $query = $this->db->query($sql);
                    //echo '$sql='.$sql."\r\n";

                    if (($query->num_rows && $query->row['keyword']) || $value == 'common/home') {

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';
                        }

                        unset($data[$key]);
                    }
                }
            }
        }

        // Seo Lang Url
        $lang_pref = '';
        if (isset($this->request->get['lng']) && !$data_lng) {
            $expl = explode('-', $this->request->get['lng']);
            if (isset($expl[1]) && $expl[1] !== 'ru') {
                $lang_pref = '/' . $expl[1];
            }
        } elseif ($data_lng) {
            $expl = explode('-', $data_lng);
            if (isset($expl[1]) && $expl[1] !== 'ru') {
                $lang_pref = '/' . $expl[1];
            }
        }

        //echo '$lang_pref'.$lang_pref.$url."<br/>";
        //die();

        if ($url) {
            unset($data['route']);

            $query = '';

            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
                }

                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $lang_pref . $url . $query;
            //return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
        } else {
            $link = str_replace('index.php?route=common/home', '', $link);
            $link = str_replace('&amp;', '&', $link);

            //echo '$route'.$route.'<br/>';
            if ($this->config->get('config_store_id') && isset($this->request->get['lng'])) {
                if ($route && $route == 'common/home') {
                    $link = $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $lang_pref . $url;
                } else {
                    $link .= '&lng=' . $this->request->get['lng'];
                }
            } elseif ((!$route) || ($this->config->get('config_store_id') && $route == 'common/home')) {
                $link = $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $lang_pref . $url;
            }
            return $link;
        }
//        else {
//            return $link;
//        }
    }


    public function rus2translit($string)
    {
        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public function str2url($str)
    {
        // переводим в транслит
        $str = $this->rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }


    /*function transliterate($textcyr = null, $textlat = null) {
        $cyr = array(
            'ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я','ы',
            'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я','Ы');
        $lat = array(
            'zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', '', 'ya','i',
            'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', '', 'Ya','i');

        if($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }*/


}
