<?php
class ControllerExtensionShippingCourier extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/shipping/courier');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('shipping_courier', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_cost'] = $this->language->get('entry_cost');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_time'] = $this->language->get('entry_time');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/shipping/courier', 'user_token=' . $this->session->data['user_token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/shipping/courier', 'user_token=' . $this->session->data['user_token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/shipping', 'user_token=' . $this->session->data['user_token'], 'SSL');


		if (isset($this->request->post['shipping_shipping_courier_status'])) {
			$data['shipping_shipping_courier_status'] = $this->request->post['shipping_shipping_courier_status'];
		} else {
			$data['shipping_shipping_courier_status'] = $this->config->get('shipping_shipping_courier_status');
		}

		if (isset($this->request->post['shipping_courier_cost'])) {
			$data['shipping_courier_cost'] = $this->request->post['shipping_courier_cost'];
		} else {
			$data['shipping_courier_cost'] = $this->config->get('shipping_courier_cost');
		}
		if (isset($this->request->post['shipping_courier_time'])) {
			$data['shipping_courier_time'] = $this->request->post['shipping_courier_time'];
		} else {
			$data['shipping_courier_time'] = $this->config->get('shipping_courier_time');
		}

		if (isset($this->request->post['free_total_coast'])) {
			$data['free_total_coast'] = $this->request->post['free_total'];
		} else {
			$data['free_total_coast'] = $this->config->get('free_total');
		}


		if (isset($this->request->post['shipping_courier_tax_class_id'])) {
			$data['shipping_courier_tax_class_id'] = $this->request->post['shipping_courier_tax_class_id'];
		} else {
			$data['shipping_courier_tax_class_id'] = $this->config->get('shipping_courier_tax_class_id');
		}

		$this->load->model('localisation/tax_class');

		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['shipping_courier_geo_zone_id'])) {
			$data['shipping_courier_geo_zone_id'] = $this->request->post['shipping_courier_geo_zone_id'];
		} else {
			$data['shipping_courier_geo_zone_id'] = $this->config->get('shipping_courier_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['shipping_courier_status'])) {
			$data['shipping_courier_status'] = $this->request->post['shipping_courier_status'];
		} else {
			$data['shipping_courier_status'] = $this->config->get('shipping_courier_status');
		}

		if (isset($this->request->post['shipping_courier_sort_order'])) {
			$data['shipping_courier_sort_order'] = $this->request->post['shipping_courier_sort_order'];
		} else {
			$data['shipping_courier_sort_order'] = $this->config->get('shipping_courier_sort_order');
		}

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('extension/shipping/courier', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/courier')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
