<?php
// Heading
$_['heading_title']          = '301 Redirect';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_filter']            = 'Фильтр';
$_['text_list']              = 'Список';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';

// Column
$_['column_seo_url']         = 'seo-url';
$_['column_store']           = 'Магазин';
$_['column_to']              = 'Ссылка куда';
$_['column_action']          = 'Действие';

// Entry
$_['entry_seo_url']          = 'seo-url';
$_['entry_store']            = 'Магазин';
$_['entry_to']               = 'Ссылка куда';


// Help
$_['help_status']            = 'Показывать/Скрыть переключатели языков (витрина магазина).';

// Error
$_['error_permission']       = 'У Вас нет прав для изменения on page параметров!';
$_['error_seo_url']          = 'seo-url обязателное поле!';
$_['error_to']               = 'Ссылка куда обязателное поле!';
