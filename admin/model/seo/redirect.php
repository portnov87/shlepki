<?php
class ModelSeoRedirect extends Model {
  public function addRedirect($data) {
    $this->db->query(
      "INSERT INTO
        ".DB_PREFIX."seo_redirect
      SET
        store_id='".(int)$data['store_id']."',
        seo_url_id='".(int)$data['seo_url_id']."',
        `to`='".$this->db->escape($data['to'])."',
        `code`='".$this->db->escape($data['code'])."'
    ");
  }

  public function editRedirect($seo_redirect_id, $data) {
    $this->db->query(
      "UPDATE
        ".DB_PREFIX."seo_redirect
      SET
        store_id='".(int)$data['store_id']."',
        seo_url_id='".(int)$data['seo_url_id']."',
        `to`='".$this->db->escape($data['to'])."',
        `code`='".$this->db->escape($data['code'])."'
      WHERE
        seo_redirect_id='".(int)$seo_redirect_id."'
    ");
  }

  public function deleteRedirect($seo_redirect_id) {
    $this->db->query("DELETE FROM ".DB_PREFIX."seo_redirect WHERE seo_redirect_id='".(int)$seo_redirect_id."'");
  }

  public function getRedirect($seo_redirect_id) {
    $query = $this->db->query("SELECT * FROM ".DB_PREFIX."seo_redirect WHERE seo_redirect_id='".(int)$seo_redirect_id."'");
    return $query->row;
  }

  public function getRedirects($data = array()) {
    $sql = "SELECT * FROM ".DB_PREFIX."seo_redirect";

    if (!is_null($data['filter_seo_url_id'])) {
      $sql .= " WHERE seo_url_id IN('".implode(',',$data['filter_seo_url_id'])."')";
    }

    if (!is_null($data['filter_seo_url_id']) && !is_null($data['filter_to'])) {
      $sql .= " AND `to` LIKE '%".$data['filter_to']."%'";
    }elseif (!is_null($data['filter_to'])){
      $sql .= " WHERE `to` LIKE '%".$data['filter_to']."%'";
    }

    $sort_data = array(
      'seo_redirect_id',
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY seo_redirect_id";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getTotal() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM ".DB_PREFIX."seo_redirect");

    return $query->row['total'];
  }
}
